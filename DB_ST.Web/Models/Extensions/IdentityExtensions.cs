﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;

namespace DB_ST.Web.Models.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetUserEmpleadoId(this IIdentity identity)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst("EmpleadoId");
            // Test for null to avoid issues during local testing
            return (claim != null) ? claim.Value : string.Empty;
        }
        //public static string GetUserApodo(this IIdentity identity)
        //{
        //    var claim = ((ClaimsIdentity)identity).FindFirst("Apodo");
        //    // Test for null to avoid issues during local testing
        //    return (claim != null) ? claim.Value : string.Empty;
        //}
    }
}