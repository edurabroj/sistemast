﻿using DB_ST.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DB_ST.Web.Models.EntitiesVM
{
    public class __NotasClaseVM
    {
        public Clase Clase { get; set; }
        public IEnumerable<Periodo> Periodos { get; set; }
        public IEnumerable<__NotasEstudianteVM> NotasEstudiante { get; set; }
    }

    public class __NotasEstudianteVM
    {
        public Estudiante Estudiante { get; set; }
        public IEnumerable<Nota> Notas { get; set; }
    }
}