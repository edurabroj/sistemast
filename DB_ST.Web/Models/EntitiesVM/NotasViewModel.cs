﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DB_ST.Web.Models
{
    public class NotasViewModel
    {
        public Int32 AsignaturaId { get; set; }
        
        public int GradoId { get; set; }

        public int SeccionId { get; set; }

        public List<EstudiantesModel> Estudiantes { get; set; }
    }

    public class EstudiantesModel
    {
        public Int32 EstudianteId { get; set; }
        public String Nombre { get; set; }
        public String Trimestre { get; set; }
        public String Cualitativa { get; set; }
        public Int32 Vigecimal { get; set; }

        public Int32 ValorNota1 { get; set; }
        public Int32 ValorNota2 { get; set; }
        public Int32 ValorNota3 { get; set; }
        public Int32 ValorNota4 { get; set; }
        public Int32 Promedio { get; set; }
    }
}