﻿using DB_ST.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DB_ST.Web.Models.EntitiesVM
{
    public class HorarioVM
    {
        public int AnioEscolarId { get; set; }
        public int GradoId { get; set; }
        public int SeccionId { get; set; }
        public List<ClaseVM> Clases { get; set; }
    }

    public class ClaseVM
    {
        public int AsignaturaId { get; set; }
        public int DocenteId { get; set; }
        public int Capacidad { get; set; }
        public List<SesionVM> Sesiones { get; set; }
    }

    public class SesionVM
    {
        public Dia Dia { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFin { get; set; }
    }
}