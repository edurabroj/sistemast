﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DB_ST.Web.Models
{
    public class HorariosViewModel
    {
        public int GradoId { get; set; }

        public int SeccionId { get; set; }

        public List<HorariosModel> Horarios { get; set; }
    }

    public class HorariosModel
    {
        public Int32 AsignaturaId { get; set; }
        public String Nombre { get; set; }

        public String Dia { get; set; }
        public DateTime HoraInicio { get; set; }
        public DateTime HoraFin { get; set; }
    }
}