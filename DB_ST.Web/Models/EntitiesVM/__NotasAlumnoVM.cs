﻿using DB_ST.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DB_ST.Web.Models.EntitiesVM
{
    public class __LibretaVM
    {
        public IEnumerable<Periodo> Periodos { get; set; }
        public List<__NotasCursoVM> NotasCursos { get; set; }
    }

    public class __NotasCursoVM
    {
        public Clase Clase { get; set; }
        public IEnumerable<Nota> Notas { get; set; }
    }
}