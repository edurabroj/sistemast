﻿using DB_ST.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DB_ST.Web.Models
{
    public class AsistenciaAlumnoVM
    {
        [Required]
        //[DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}"
        //    , ApplyFormatInEditMode = true)
        //]
        public DateTime Fecha { get; set; }

        [Required]
        public Int32 GradoId { get; set; }

        [Required]
        public Int32 SeccionId { get; set; }

        //[Required]
        //public Int32 NivelId { get; set; }

        //[Required]
        //public int CursoId { get; set; }

        public List<EstudianteVM> Estudiantes { get; set; }
    }

    public class EstudianteVM
    {
        public Int32 EstudianteId { get; set; }
        public string Estudiante { get; set; }
        public ValorAsistencia ValorAsistencia { get; set; }
    }
}