﻿using DB_ST.Domain;
using DB_ST.Service;
using DB_ST.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DB_ST.Web.Controllers
{
    public class HomeController : Controller
    {
        IMasterService<Empleado> _serviceEmpleado;
        IMasterService<Apoderado> _serviceApoderado;
        private ApplicationDbContext _context = new ApplicationDbContext();

        public HomeController
        (
            IMasterService<Empleado> _serviceEmpleado,
            IMasterService<Apoderado> _serviceApoderado
        )
        {
            this._serviceEmpleado = _serviceEmpleado;
            this._serviceApoderado = _serviceApoderado;
        }

        public ActionResult Index()
        {
            CreateAdminUser();
            CreateSecretarioUser();
            CreateDirectorUser();
            CreateEmpleadosUsers();
            CreateApoderadosUsers();
            return View();
        }

        public void CreateDirectorUser()
        {
            var correo = "director" + Configuraciones.emailDomain;
            var rol = Rols.Director;
            CreateUsuario(correo,rol);
        }

        public void CreateSecretarioUser()
        {
            var correo = "secretario" + Configuraciones.emailDomain;
            var rol = Rols.Secretario;
            CreateUsuario(correo, rol);
        }
        public void CreateAdminUser()
        {
            var correo = "admin" + Configuraciones.emailDomain;
            var rol = Rols.Administrador;
            CreateUsuario(correo, rol);
        }

        public void CreateEmpleadosUsers()
        {
            var empleados = _serviceEmpleado.GetAll(x=>x.TipoEmpleado);
            foreach (var empleado in empleados)
            {
                CreateUsuario(empleado.Email, empleado.TipoEmpleado.NombreTipoEmpleado.ToLower(),empleado.EmpleadoId);
            }
        }

        public void CreateApoderadosUsers()
        {
            var apoderados = _serviceApoderado.GetAll();
            foreach (var apoderado in apoderados)
            {
                CreateUsuario(apoderado.Email, Rols.Apoderado,apoderado.ApoderadoId);
            }
        }

        public void CreateUsuario(string correo, string rol, int usuarioStID=0,string password = Configuraciones.defaultPassword)
        {
            if (!_context.Users.Any(u => u.UserName.Equals(correo)) && !correo.Contains("-"))
            {
                var userStore = new UserStore<ApplicationUser>(_context);
                var userManager = new UserManager<ApplicationUser>(userStore);

                var user = new ApplicationUser
                {
                    UserName = correo,
                    Email = correo,
                    EmpleadoId = usuarioStID
                };

                var result = userManager.Create(user, password);
                _context.Roles.AddOrUpdate(r => r.Name, new IdentityRole() { Name = rol });
                _context.SaveChanges();
                userManager.AddToRole(user.Id, rol);
            }
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}