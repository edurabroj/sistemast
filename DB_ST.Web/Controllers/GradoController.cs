﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Director)]
    public class GradoController : Controller
    {
        IMasterService<Grado> _service;

        public GradoController(IMasterService<Grado> service)
        {
            _service = service;
        }

        public ActionResult Index(string SearchCriteria)
        {
            if (string.IsNullOrEmpty(SearchCriteria))
                SearchCriteria = "";

            var lista = _service.Find(
                        m => m.Nombre.Contains(SearchCriteria)
                        //, x => x.Estudiante, x => x.Seccion, x => x.Grado, x => x.AnioEscolar
                    );

            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Index", lista) : View(lista);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Grado model)
        {
            Validar(model);

            model.UlltimaModificacion = DateTime.Now;
            if (ModelState.IsValid)
            {
                _service.Add(model);
                return RedirectToAction("Index");
            }
            return null;
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Grado model)
        {
            Validar(model);
            model.UlltimaModificacion = DateTime.Now;
            if (ModelState.IsValid)
            {
                _service.Edit(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(Grado model)
        {
            if (ModelState.IsValid)
            {
                _service.Remove(model.GradoId);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        //Validaciones///////////////////////////////////////////////////////////////
        private void Validar(Grado grado)
        {
            ModelState.Clear();
            
            if (string.IsNullOrEmpty(grado.Nombre))
                ModelState.AddModelError("Nombre", "Campo requerido");
            if (_service.Find(x => x.Nombre == grado.Nombre && x.GradoId!=grado.GradoId).Any())
                ModelState.AddModelError("Nombre", "Ya Existe");
        }
    }
}