﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;

namespace DB_ST.Web.Controllers
{
    public class UbicacionGeograficaController : Controller
    {
        IMasterService<Departamento> _serviceDep;
        IMasterService<Provincia> _servicePro;
        IMasterService<Distrito> _serviceDis;

        public UbicacionGeograficaController(IMasterService<Departamento> serviceDep,
            IMasterService<Provincia> servicePro,
            IMasterService<Distrito> serviceDis)
        {
            _serviceDep = serviceDep;
            _servicePro = servicePro;
            _serviceDis = serviceDis;
        }
        // GET: UbicacionGeografica
        public ActionResult ObtenerUbicacion()
        {
            var deps = _serviceDep.GetAll();
            var pros = _servicePro.GetAll();
            var diss = _serviceDis.GetAll();
            ViewBag.Deps = new SelectList(deps, "DepartamentoId", "Nombre");
            ViewBag.Pros = new SelectList(pros, "ProvinciaId", "Nombre");
            ViewBag.Diss = new SelectList(diss, "DistritoId", "Nombre");
            return View();
        }
    }
}