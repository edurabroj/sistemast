﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Director)]
    public class AsignaturaController : Controller
    {
        IMasterService<Asignatura> _service;

        public AsignaturaController(IMasterService<Asignatura> service)
        {
            _service = service;
        }

        public ActionResult Index(string SearchCriteria)
        {
            if (string.IsNullOrEmpty(SearchCriteria))
                SearchCriteria = "";

            var lista = _service.Find(
                        m => m.Nombre.Contains(SearchCriteria)
                        //, x => x.Estudiante, x => x.Seccion, x => x.Grado, x => x.AnioEscolar
                    );

            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Index", lista) : View(lista);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Asignatura model)
        {
            Validar(model);
            model.UlltimaModificacion = DateTime.Now;
            if (ModelState.IsValid)
            {
                _service.Add(model);
                return RedirectToAction("Index");
            }
            return null;
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Asignatura model)
        {
            model.UlltimaModificacion = DateTime.Now;
            Validar(model);
            if (ModelState.IsValid)
            {
                _service.Edit(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(Asignatura model)
        {
            if (ModelState.IsValid)
            {
                _service.Remove(model.AsignaturaId);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        //Validaciones///////////////////////////////////////////////////////////////
        private void Validar(Asignatura asignatura)
        {
            ModelState.Clear();
            if (_service.Find(x => x.Nombre == asignatura.Nombre
                                && x.AsignaturaId!=asignatura.AsignaturaId).Any())
                ModelState.AddModelError("Nombre", "Ya Existe");

            if (string.IsNullOrEmpty(asignatura.Nombre))
                ModelState.AddModelError("Nombre", "Campo requerido");
            
        }
    }
}