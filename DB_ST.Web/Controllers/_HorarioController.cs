﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;

namespace DB_ST.Web.Controllers
{
    public class _HorarioController : Controller
    {

        IMasterService<Sesion> _serviceSesion;
        IMasterService<Clase> _serviceClase;
        IMasterService<EstudianteClase> _serviceEstudianteClase;
        IMasterService<Asignatura> _serviceAsignatura;


        public _HorarioController
        (
            IMasterService<Sesion> Sesion,
            IMasterService<Clase> Clase,
            IMasterService<EstudianteClase> EstudianteClase,
            IMasterService<Asignatura> Asignatura
        )
        {
            _serviceSesion = Sesion;
            _serviceClase = Clase;
            _serviceEstudianteClase = EstudianteClase;
            _serviceAsignatura = Asignatura;

        }
        // GET: Horario
        public ActionResult Index(String SearchCriteria)
        {
            
            return View();

        }

        public ActionResult GetHorario()
        {

            return View();
        }

    }
}