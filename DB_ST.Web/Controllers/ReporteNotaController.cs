﻿using DB_ST.Domain;
using DB_ST.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using DB_ST.Web.Models.Extensions;
using DB_ST.Web.Models.EntitiesVM;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Apoderado + "," + Rols.Director + "," +
                       Rols.Administrador + "," + Rols.Docente + "," +
                       Rols.Secretario)]
    public class ReporteNotaController : Controller
    {
        IMasterService<Asignatura> _serviceAsignatura;
        IMasterService<Nota> _serviceNota;
        IMasterService<EstudianteClase> _serviceEstudianteClase;
        IMasterService<Clase> _serviceClase;
        IMasterService<Estudiante> _serviceEstudiante;

        string userId;
        SelectList estudiantesSL;

        public ReporteNotaController
            (
                IMasterService<Asignatura> _serviceAsignatura,
                IMasterService<Nota> _serviceNota,
                IMasterService<Clase> _serviceClase,
                IMasterService<EstudianteClase> _serviceEstudianteClase,
                IMasterService<Estudiante> _serviceEstudiante
            )
        {
            this._serviceAsignatura = _serviceAsignatura;
            this._serviceNota = _serviceNota;
            this._serviceClase = _serviceClase;
            this._serviceEstudianteClase = _serviceEstudianteClase;
            this._serviceEstudiante = _serviceEstudiante;
        }

        // GET: ReporteNota
        public ActionResult Index()
        {
            //userId = User.Identity.GetUserEmpleadoId();

            //var students = DependencyResolver.Current.
            //                            GetService<ReporteAsistenciaController>().
            //                            GetEstudiantesByApoderado(int.Parse(userId));

            ////var students = _serviceEstudiante.GetAll();

            List<Estudiante> estudiantesSelect;
            if (User.IsInRole(Rols.Apoderado))
            {
                userId = User.Identity.GetUserEmpleadoId();
                estudiantesSelect = DependencyResolver.Current.
                                        GetService<ReporteAsistenciaController>().
                                        GetEstudiantesByApoderado(int.Parse(userId));
            }
            else
            {
                estudiantesSelect = _serviceEstudiante.GetAll().ToList();
            }

            estudiantesSL = new SelectList(estudiantesSelect,
                                        "EstudianteId", "DniFullName");
            ViewBag.Estudiantes = estudiantesSL;
            return View();
        }

        public ActionResult _GetLibreta(int estudianteId, int? anioEscolarId)
        {
            var libreta = new __LibretaVM();
            if (anioEscolarId == null)
                anioEscolarId = DependencyResolver.Current.
                                        GetService<MatriculaController>().
                                        GetAnioActual().AnioEscolarId;

            libreta.Periodos = DependencyResolver.Current.
                                        GetService<NotaController>().
                                        GetPeriodosByAnio(anioEscolarId.Value);

            var clases = GetClasesByEstudiante(estudianteId);

            libreta.NotasCursos = new List<__NotasCursoVM>();
            foreach (var clase in clases)
            {
                libreta.NotasCursos.Add
                (
                    new __NotasCursoVM
                    {
                        Clase=clase,
                        Notas = GetNotasEstudianteByClase(estudianteId, clase.ClaseId)
                    }
                );
            }
            return PartialView(libreta);
        }

        public List<Clase> GetClasesByEstudiante(int estudianteId)
        {
            var clasesIDs = _serviceEstudianteClase.Find(
                                        x => x.EstudianteId == estudianteId
                                        && x.Clase.IsHabilitado ).ToList()
                                        .Select(x => x.ClaseId);
            return (from c in _serviceClase.GetAll().ToList()
                         from cId in clasesIDs
                         where c.ClaseId == cId
                         select c).ToList();
        }

        public List<Nota> GetNotasEstudianteByClase(int estudianteId, int claseId)
        {
            return _serviceNota.Find(x => x.EstudianteClase.EstudianteId == estudianteId &&
                                     x.EstudianteClase.ClaseId == claseId).ToList();
        }
    }
}