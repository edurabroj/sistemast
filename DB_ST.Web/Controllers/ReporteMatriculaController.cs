﻿using DB_ST.Domain;
using DB_ST.Service;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DB_ST.Web.Controllers
{
    public class ReporteMatriculaController : Controller
    {
        IMasterService<Matricula> _serviceMatricula;
        IMasterService<AnioEscolar> _serviceAnioEscolar;
        IMasterService<Seccion> _serviceSeccion;
        IMasterService<Grado> _serviceGrado;

        SelectList anioEscolars;
        SelectList grados;
        SelectList secciones;

        public ReporteMatriculaController
        (
            IMasterService<Matricula> _serviceMatricula,
            IMasterService<AnioEscolar> _serviceAnioEscolar,
            IMasterService<Seccion> _serviceSeccion,
            IMasterService<Grado> _serviceGrado
        )
        {
            this._serviceMatricula = _serviceMatricula;
            this._serviceAnioEscolar = _serviceAnioEscolar;
            this._serviceSeccion = _serviceSeccion;
            this._serviceGrado = _serviceGrado;

            anioEscolars = new SelectList(_serviceAnioEscolar.GetAll(), "AnioEscolarId", "Anio");
            grados = new SelectList(_serviceGrado.GetAll(), "GradoId", "Nombre");
            secciones = new SelectList(new List<Seccion>(), "SeccionId", "Nombre");
        }


        // GET: Reporte
        public ActionResult Index(string SearchCriteria, int? AnioEscolarId, int? GradoId, int? SeccionId, int? page)
        {
            if (string.IsNullOrEmpty(SearchCriteria))
                SearchCriteria = "";

            SearchCriteria = SearchCriteria.ToLower().Trim();

            var lista = _serviceMatricula.Find(
                        m => m.Estudiante.DNI.Contains(SearchCriteria) ||
                        m.Estudiante.Nombres.ToLower().Trim().Contains(SearchCriteria) ||
                        m.Estudiante.Apellidos.ToLower().Trim().Contains(SearchCriteria) ||
                        (m.Estudiante.Nombres + " " + m.Estudiante.Apellidos).ToLower().Trim().Contains(SearchCriteria)
                        , x => x.Estudiante, x => x.Seccion, x => x.Grado, x => x.AnioEscolar
                    );

            if (AnioEscolarId != null) lista = lista.Where(x => x.AnioEscolarId == AnioEscolarId);
            if (GradoId != null) lista = lista.Where(x => x.GradoId == GradoId);
            if (SeccionId != null) lista = lista.Where(x => x.SeccionId == SeccionId);

            ViewBag.AnioEscolars = anioEscolars;
            ViewBag.Grados = grados;
            ViewBag.Secciones = secciones;

            ViewBag.SearchCriteria = SearchCriteria;
            ViewBag.AnioEscolarId = AnioEscolarId;
            ViewBag.GradoId = GradoId;
            ViewBag.SeccionId = SeccionId;

            var pageIndex = page ?? 1;
            lista = lista.OrderByDescending(x=>x.FechaMatricula).ToPagedList(pageIndex, Configuraciones.defaultPageSize);

            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Matriculados", lista) : View("Matriculados",lista);
        }
    }
}