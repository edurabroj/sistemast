﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Director)]
    public class AnioEscolarController : Controller
    {
        IMasterService<AnioEscolar> _service;

        public AnioEscolarController(IMasterService<AnioEscolar> service)
        {
            _service = service;
        }

        // GET: AnioEscolar
        public ActionResult Index(string SearchCriteria)
        {
            if (!String.IsNullOrEmpty(SearchCriteria))
                return View(_service.SearchByCriteria(a => a.Anio.Contains(SearchCriteria)));
            else
                return View(_service.GetAll());
        }

        //[HttpPost]
        //public ActionResult Index(string SearchCriteria)
        //{
        //    var datos = _service.SearchByCriteria(a=>a.Anio.Contains(SearchCriteria));
        //    return View(datos);
        //}
        
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Create(AnioEscolar anioEscolar)
        {
            Validar(anioEscolar);
            if (ModelState.IsValid)
            {
                _service.Add(anioEscolar);
                return RedirectToAction("Index");
            }
            return View(anioEscolar);
        }

        public ActionResult Edit(int id)
        {
            var model = _service.GetById(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(AnioEscolar anioEscolar)
        {
            Validar(anioEscolar);

            if (ModelState.IsValid)
            {
                _service.Edit(anioEscolar);
                return RedirectToAction("Index");
            }
            return View(anioEscolar);
        }

        public ActionResult Delete(int id)
        {
            var model = _service.GetById(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(AnioEscolar anioEscolar)
        {
            if (ModelState.IsValid)
            {
                _service.Remove(anioEscolar.AnioEscolarId);
                return RedirectToAction("Index");
            }
            return View();
        }

        public ActionResult Details(int id)
        {
            var model = _service.GetById(id);
            return View(model);
        }

        //Validaciones///////////////////////////////////////////////////////////////
        private void Validar(AnioEscolar anioEscolar)
        {
            ModelState.Clear();
            
            if (_service.Find(x => x.Anio == anioEscolar.Anio && x.AnioEscolarId != anioEscolar.AnioEscolarId).Any())
                ModelState.AddModelError("Anio", "Ya Existe");
            if (string.IsNullOrEmpty(anioEscolar.Anio))
                ModelState.AddModelError("Anio", "Campo requerido");
            if (string.IsNullOrEmpty(anioEscolar.FechaInicio.ToString()))
                ModelState.AddModelError("FechaInicio", "Campo requerido");
            if (string.IsNullOrEmpty(anioEscolar.FechaFin.ToString()))
                ModelState.AddModelError("FechaFin", "Campo requerido");
            if (string.IsNullOrEmpty(anioEscolar.FechaInicioMatricula.ToString()))
                ModelState.AddModelError("FechaInicioMatricula", "Campo requerido");
            if (string.IsNullOrEmpty(anioEscolar.FechaFinMatricula.ToString()))
                ModelState.AddModelError("FechaFinMatricula", "Campo requerido");
            if (string.IsNullOrEmpty(anioEscolar.FechaMatriculaContemporanea.ToString()))
                ModelState.AddModelError("FechaMatriculaContemporanea", "Campo requerido");
            if (string.IsNullOrEmpty(anioEscolar.MontoMatriculaRegular.ToString()))
                ModelState.AddModelError("MontoMatriculaRegular", "Campo requerido");
            if (string.IsNullOrEmpty(anioEscolar.MontoMatriculaExtemporanea.ToString()))
                ModelState.AddModelError("MontoMatriculaExtemporanea", "Campo requerido");

        }
    }
}
