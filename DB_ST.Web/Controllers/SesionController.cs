﻿using DB_ST.Domain;
using DB_ST.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Director)]
    public class SesionController : Controller
    {
        private IMasterService<Sesion> _serviceSesion;
        private IMasterService<Clase> _serviceClase;
        private IMasterService<Grado> _serviceGrado;
        //private IMasterService<Asignatura> _serviceAsignatura;
        private IMasterService<Seccion> _serviceSeccion;
        private IMasterService<Horario> _serviceHorario;
        //private IMasterService<Estudiante> _serviceEstudiante;

        public SesionController
            (
            IMasterService<Sesion> serviceSesion,
            IMasterService<Clase> serviceClase,
            IMasterService<Grado> serviceGrado,
            //IMasterService<Asignatura> serviceAsignatura,
            IMasterService<Seccion> serviceSeccion,
            IMasterService<Horario> serviceHorario
            //IMasterService<Estudiante> serviceEstudiante
            )
        {
            _serviceSesion = serviceSesion;
            _serviceClase = serviceClase;
            _serviceGrado = serviceGrado;
            //_serviceAsignatura = serviceAsignatura;
            _serviceSeccion = serviceSeccion;
            _serviceHorario = serviceHorario;
            //_serviceEstudiante = serviceEstudiante;
        }
        // GET: HorarioEstudiante
        public ActionResult Index()
        {
            ViewBag.Titulo = "Clases";

            ViewBag.Secciones = _serviceSeccion.GetAll();
            ViewBag.Grados = _serviceGrado.GetAll();

            return View("Index");
        }

        //A C-Sesion
        public ActionResult GetByClaseId(int claseId)
        {
            var sesiones = _serviceSesion.Find(s => s.ClaseId == claseId, x => x.Clase, x=>x.Clase.Seccion,x=>x.Horario);
            var clase = _serviceClase.GetById(claseId);
            var seccion = _serviceSeccion.GetById(clase.SeccionId);
            var grado = _serviceGrado.GetById(seccion.GradoId);
            @ViewBag.Titulo = "Sesiones para '" + clase.Nombre + "' (" + grado.Nombre + "-" + seccion.Nombre + ")";
            @ViewBag.ClaseId = clase.ClaseId;
            return View(sesiones);
        }

        public void llenarCombos()
        {
            ViewBag.Horarios = new SelectList(_serviceHorario.GetAll(),"HorarioId", "Full");
        }
        //A C-Sesion
        public ActionResult Create(int claseId)
        {
            llenarCombos();
            ViewBag.Titulo = "Registrar";
            return View(new Sesion
            {
                ClaseId = claseId 
                //HoraInicio =DateTime.Now.Date,
                //HoraFin = DateTime.Now.Date
            });

            //if (claseId != null)
            //    return View(new Sesion { ClaseId = claseId.Value });
            //else
            //    return null;
        }

        private Clase GetClaseCompleta(int claseId)
        {
            return _serviceClase.Find(x => x.ClaseId == claseId, x => x.Asignatura).FirstOrDefault();
        }

        //A C-Sesion
        [HttpPost]
        public ActionResult Create(Sesion model)
        {
            Validar(model);

            if (ModelState.IsValid)
            {
                _serviceSesion.Add(model);
                return RedirectToAction("GetByClaseId", new { claseId=model.ClaseId});
            }
            llenarCombos();
            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _serviceSesion.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            llenarCombos();
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Sesion model)
        {
            Validar(model);
            if (ModelState.IsValid)
            {
                _serviceSesion.Edit(model);
                return RedirectToAction("GetByClaseId", new { claseId = model.ClaseId });
            }
            llenarCombos();
            return View(model);
        }

        public ActionResult Delete(int id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            var model = GetSesionCompleta(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        private Sesion GetSesionCompleta(int id)
        {
            return _serviceSesion.Find(x => x.SesionId == id, x => x.Clase, x => x.Horario).FirstOrDefault();
        }

        [HttpPost]
        public ActionResult Delete(Sesion model)
        {
            if (ModelState.IsValid)
            {
                var claId= model.ClaseId;
                _serviceSesion.Remove(model.SesionId);
                return RedirectToAction("GetByClaseId", new { claseId = claId });
            }
            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = GetSesionCompleta(id.Value);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        //validaciones/////////////////
        private void Validar(Sesion sesion)
        {
            ModelState.Clear();
            var clase = GetClaseCompleta(sesion.ClaseId);

            var sesionesSolapadas = _serviceSesion.Find(x => x.Dia == sesion.Dia
                                    && x.HorarioId == sesion.HorarioId
                                    && x.Clase.SeccionId == clase.SeccionId
                                    && x.SesionId != sesion.SesionId,
                                    x=>x.Clase.Asignatura);
            if (sesionesSolapadas.Any())
                ModelState.AddModelError("HorarioId", "Sesión solapada con " + sesionesSolapadas.FirstOrDefault().Clase.FullAsignaturaClase);
            if (string.IsNullOrEmpty(sesion.Dia.ToString()))
                ModelState.AddModelError("Dia", "Campo requerido");
            if (sesion.HorarioId<1)
                ModelState.AddModelError("HoraInicio", "Campo requerido");
        }


    }
}