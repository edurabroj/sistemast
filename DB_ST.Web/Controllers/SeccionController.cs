﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Director)]
    public class SeccionController : Controller
    {
        IMasterService<Seccion> _service;
        IMasterService<Grado> _serviceGrado;
        IMasterService<AnioEscolar> _serviceAnioEscolar;
        IMasterService<Empleado> _serviceEmpleado;

        private SelectList grados;
        private SelectList anios;
        private SelectList docentes;

        public SeccionController
        (
            IMasterService<Seccion> service,
            IMasterService<Grado> serviceGrado,
            IMasterService<AnioEscolar> serviceAnioEscolar,
            IMasterService<Empleado> serviceEmpleado
        )
        {
            _service = service;
            _serviceGrado = serviceGrado;
            _serviceAnioEscolar = serviceAnioEscolar;
            _serviceEmpleado = serviceEmpleado;

            grados = new SelectList(_serviceGrado.GetAll(), "GradoId", "Nombre");
            anios = new SelectList(_serviceAnioEscolar.GetAll(), "AnioEscolarId", "Anio");
            docentes = new SelectList(_serviceEmpleado.Find(x => x.TipoEmpleado.NombreTipoEmpleado.ToLower() == "docente"), "EmpleadoId", "FullName");
        }

        public ActionResult Index(string SearchCriteria)
        {
            if (string.IsNullOrEmpty(SearchCriteria))
                SearchCriteria = "";

            var lista = _service.Find(
                        m => m.Nombre.Contains(SearchCriteria)
                        , x => x.Grado, x => x.AnioEscolar//, x => x.Grado, x => x.AnioEscolar
                    );

            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Index", lista) : View(lista);
        }

        public ActionResult Create()
        {
            ViewBag.Grados = grados;
            ViewBag.AnioEscolars = anios;
            ViewBag.Docentes = docentes;

            return View();           
        }

        [HttpPost]
        public ActionResult Create(Seccion model)
        {
            Validar(model);
            //model.UlltimaModificacion = DateTime.Now;
            if (ModelState.IsValid)
            {
                //model.VacantesDisp = model.Capacidad;
                _service.Add(model);
                return RedirectToAction("Index");
            }

            ViewBag.Grados = grados;
            ViewBag.AnioEscolars = anios;
            ViewBag.Docentes = docentes;

            //var rpta = "";
            //foreach (var key in ModelState.Keys)
            //{
            //    rpta += key + " ";
            //}
            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            ViewBag.Grados = grados;
            ViewBag.AnioEscolars = anios;
            ViewBag.Docentes = docentes;

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Seccion model)
        {
            Validar(model);
            //model.UlltimaModificacion = DateTime.Now;
            if (ModelState.IsValid)
            {
                _service.Edit(model);
                return RedirectToAction("Index");
            }
            ViewBag.Grados = grados;
            return View(model);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(Seccion model)
        {
            if (ModelState.IsValid)
            {
                _service.Remove(model.SeccionId);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }


        //Validaciones///////////////////////////////////////////////////////////////
        private void Validar(Seccion seccion, bool editar=false)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(seccion.Nombre))
                ModelState.AddModelError("Nombre", "Campo requerido");
            if (!editar && _service.Find(x => x.Nombre == seccion.Nombre).Any())
                ModelState.AddModelError("Nombre", "Ya Existe");
            if (seccion.Capacidad < 15)
                ModelState.AddModelError("Capacidad", "Valor no válido");
            //if (seccion.VacantesDisp < 5)
            //    ModelState.AddModelError("VacantesDisp", "Valor no válido");
        }
    }
}