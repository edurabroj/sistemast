﻿using DB_ST.Domain;
using DB_ST.Service;
using DB_ST.Web.Models;
using DB_ST.Web.Validators;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Secretario)]
    public class ApoderadoController : Controller
    {
        IMasterService<Apoderado> _service;
        private ApplicationDbContext _context = new ApplicationDbContext();

        public ApoderadoController(IMasterService<Apoderado> service)
        {
            _service = service;
        }

        //GET: Apoderado
        public ActionResult Index(string SearchCriteria, int? page)
        {
            if (string.IsNullOrEmpty(SearchCriteria))
                SearchCriteria = "";

            var lista = _service.Find(
                        m => m.DNI.Contains(SearchCriteria)
                        //, x => x.Estudiante, x => x.Seccion, x => x.Grado, x => x.AnioEscolar
                    );

            ViewBag.SearchCriteria = SearchCriteria;

            var pageIndex = page ?? 1;
            lista = lista.OrderBy(p => p.Apellidos).ToPagedList(pageIndex, Configuraciones.defaultPageSize);

            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Index", lista) : View(lista);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Apoderado apoderado)
        {
            Validar(apoderado);

            if (ModelState.IsValid)
            {
                _service.Add(apoderado);
                try
                {

                    var userStore = new UserStore<ApplicationUser>(_context);
                    var userManager = new UserManager<ApplicationUser>(userStore);

                    //var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();//primero se migra la base de datos y se copias esta parte de codigo
                    var user = new ApplicationUser
                    {
                        UserName = apoderado.Email,
                        Email = apoderado.Email,
                        EmpleadoId = apoderado.ApoderadoId
                    };
                    var result = userManager.Create(user, "St2017*");
                    _context.Roles.AddOrUpdate(r => r.Name, new IdentityRole() { Name=Rols.Apoderado });
                    _context.SaveChanges();
                    userManager.AddToRole(user.Id, Rols.Apoderado);

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("ERROR: " + ex.Message);
                }
                return RedirectToAction("Index");
            }
            return View(apoderado);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Apoderado apoderado)
        {
            Validar(apoderado);
            if (ModelState.IsValid)
            {
                _service.Edit(apoderado);
                return RedirectToAction("Index");
            }
            return View(apoderado);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(Apoderado apoderado)
        {
            if (ModelState.IsValid)
            {
                _service.Remove(apoderado.ApoderadoId);
                return RedirectToAction("Index");
            }
            return View(apoderado);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        //Validaciones///////////////////////////////////////////////////////////////
        private void Validar(Apoderado apoderado)
        {
            ModelState.Clear();
            if (!string.IsNullOrEmpty(apoderado.DNI) && _service.Find(x => x.DNI == apoderado.DNI 
                                                                        && x.ApoderadoId!=apoderado.ApoderadoId).Any())
                ModelState.AddModelError("DNI", "Campo repetido");
            

            if (string.IsNullOrEmpty(apoderado.Nombres))
                ModelState.AddModelError("Nombres", "Campo requerido");
            if (string.IsNullOrEmpty(apoderado.Apellidos))
                ModelState.AddModelError("Apellidos", "Campo requerido");
            if (!string.IsNullOrEmpty(apoderado.DNI) && !Validator.IsDNI(apoderado.DNI))
                ModelState.AddModelError("DNI", "Inválido.");
            if (String.IsNullOrEmpty(apoderado.FechaNacimiento.ToString()))
                ModelState.AddModelError("FechaNacimiento", "Campo requerido");
            if (string.IsNullOrEmpty(apoderado.Celular))
                ModelState.AddModelError("Celular", "Campo requerido");
        }
    }
}