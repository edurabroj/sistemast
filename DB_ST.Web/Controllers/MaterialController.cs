﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Docente)]
    public class MaterialController : Controller
    {
        IMasterService<Material> _service;
        IMasterService<Seccion> _serviceSeccion;

        public MaterialController(IMasterService<Material> service, IMasterService<Seccion> serviceSeccion)
        {
            _service = service;
            _serviceSeccion = serviceSeccion;

        }

        public ActionResult Index(string SearchCriteria)
        {
            if (string.IsNullOrEmpty(SearchCriteria))
                SearchCriteria = "";

            var lista = _service.Find(
                        m => m.Nombre.Contains(SearchCriteria)
                        , x => x.Seccion
                        //, x => x.Seccion, x => x.Grado, x => x.AnioEscolar
                    );

            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Index", lista) : View(lista);
        }

        public ActionResult Create()
        {
            var secciones = _serviceSeccion.GetAll();
            ViewBag.Secciones= new SelectList(secciones,"SeccionId","Nombre");
            return View();
        }

        [HttpPost]
        public ActionResult Create(Material model)
        {
            Validar(model);
            //model.UlltimaModificacion = DateTime.Now;
            if (ModelState.IsValid)
            {
                _service.Add(model);
                return RedirectToAction("Index");
            }
            return null;
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }

            var secciones = _serviceSeccion.GetAll();
            ViewBag.Secciones = new SelectList(secciones, "SeccionId", "Nombre");

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Material model)
        {
            //model.UlltimaModificacion = DateTime.Now;
            Validar(model);
            if (ModelState.IsValid)
            {
                _service.Edit(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                _service.Remove(id);
                return RedirectToAction("Index");
            }
            return View(id);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        //Validaciones///////////////////////////////////////////////////////////////
        private void Validar(Material material)
        {
            ModelState.Clear();
            
            if (string.IsNullOrEmpty(material.Nombre))
                ModelState.AddModelError("Nombre", "Campo requerido");
            if (string.IsNullOrEmpty(material.Descripcion))
                ModelState.AddModelError("Descripcion", "Campo requerido");
            if (_service.Find(x => x.Nombre == material.Nombre && x.MaterialId!=material.MaterialId).Any())
                ModelState.AddModelError("Nombre", "Ya Existe");
            if (material.Disponibles < 1)
                ModelState.AddModelError("Disponibles", "Valor no válido");
            if (material.Usados < 1)
                ModelState.AddModelError("Usados", "Valor no válido");
        }
    }
}