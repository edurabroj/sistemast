﻿using DB_ST.Domain;
using DB_ST.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using DB_ST.Web.Models.Extensions;
using DB_ST.Domain.Enum;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Docente+","+Rols.Director)]
    public class NotaSolicitudCambioController : Controller
    {
        IMasterService<NotaSolicitudCambio> _serviceNotaSolicitudCambio;
        IMasterService<Nota> _serviceNota;
        IMasterService<Empleado> _serviceEmpleado;

        public NotaSolicitudCambioController
            (
                IMasterService<NotaSolicitudCambio> _serviceNotaSolicitudCambio,
                IMasterService<Nota> _serviceNota,
                IMasterService<Empleado> _serviceEmpleado
            )
        {
            this._serviceNotaSolicitudCambio = _serviceNotaSolicitudCambio;
            this._serviceNota = _serviceNota;
            this._serviceEmpleado = _serviceEmpleado;
        }

        [Authorize(Roles = Rols.Director)]
        // GET: NotaSolicitudCambio
        public ActionResult Index(int estadoCod=0, string info="", string success = "", string error = "")
        {
            ViewBag.info = info;
            ViewBag.success = success;
            ViewBag.error = error;

            var userEmpleadoId = int.Parse(User.Identity.GetUserEmpleadoId());
            var solicitudes = _serviceNotaSolicitudCambio.Find
                (
                    x => x.DestinatarioId == userEmpleadoId,
                    x=>x.Nota.EstudianteClase.Estudiante,
                    x => x.Nota.EstudianteClase.Clase.Asignatura,
                    x => x.Nota.EstudianteClase.Clase.Empleado
                );

            EstadoSolicitud estado = new EstadoSolicitud();

            switch (estadoCod)
            {
                case 0:
                    estado = EstadoSolicitud.Pendiente;
                    break;
                case 1:
                    estado = EstadoSolicitud.Aprobada;
                    break;
                case 2:
                    estado = EstadoSolicitud.Rechazada;
                    break;
            }
            solicitudes = solicitudes.ToList().FindAll(x => x.EstadoSolicitud==estado);
            //_serviceNotaSolicitudCambio.Find
            return View(solicitudes);
            //return View();
        }

        [Authorize(Roles = Rols.Docente)]
        // GET: NotaSolicitudCambio
        public ActionResult IndexDocente(int estadoCod = 0, string info = "", string success = "", string error = "")
        {
            ViewBag.info = info;
            ViewBag.success = success;
            ViewBag.error = error;

            var userEmpleadoId = int.Parse(User.Identity.GetUserEmpleadoId());
            var solicitudes = _serviceNotaSolicitudCambio.Find
                (
                    x => x.Nota.EstudianteClase.Clase.EmpleadoId == userEmpleadoId,
                    x => x.Nota.EstudianteClase.Estudiante,
                    x => x.Nota.EstudianteClase.Clase.Asignatura,
                    x => x.Nota.EstudianteClase.Clase.Empleado
                );

            //EstadoSolicitud estado = new EstadoSolicitud();

            //switch (estadoCod)
            //{
            //    case 0:
            //        estado = EstadoSolicitud.Pendiente;
            //        break;
            //    case 1:
            //        estado = EstadoSolicitud.Aprobada;
            //        break;
            //    case 2:
            //        estado = EstadoSolicitud.Rechazada;
            //        break;
            //}
            solicitudes = solicitudes.ToList();
            //_serviceNotaSolicitudCambio.Find
            return View(solicitudes.OrderByDescending(x=>x.NotaSolicitudCambioId));
            //return View();
        }

        [Authorize(Roles = Rols.Docente)]
        public ActionResult Create(int notaId)
        {
            if (_serviceNotaSolicitudCambio.Find(x => x.NotaId == notaId && x.EstadoSolicitud==EstadoSolicitud.Pendiente).Any())
                return RedirectToAction("Index","Nota", new
                {
                    error = "Su solicitud de modificación ya ha sido enviada anteriormente, la nota se cambiará si se aprueba."
                }
                );
            
            ViewBag.NotaActual = _serviceNota.GetById(notaId).Vigesimal;

            var destinatarios = _serviceEmpleado.Find(x => x.TipoEmpleado.NombreTipoEmpleado.ToLower() == "director", x => x.TipoEmpleado);
            ViewBag.Destinatarios = new SelectList(destinatarios, "EmpleadoId", "FullName");
            return View(new NotaSolicitudCambio
            {
                NotaId = notaId,
                EstadoSolicitud = Domain.Enum.EstadoSolicitud.Pendiente
            });
        }

        [Authorize(Roles = Rols.Docente)]
        [HttpPost]
        public ActionResult Create(NotaSolicitudCambio model)
        {
            Validar(model);
            if (ModelState.IsValid)
            {
                _serviceNotaSolicitudCambio.Add(model);
                return RedirectToAction("Index","Nota", new { success = "Su solicitud se ha enviado correctamente, la nota se cambiará si se aprueba." });
            }
            ViewBag.NotaActual = _serviceNota.GetById(model.NotaId).Vigesimal;
            var destinatarios = _serviceEmpleado.Find(x => x.TipoEmpleado.NombreTipoEmpleado.ToLower() == "director", x => x.TipoEmpleado);
            ViewBag.Destinatarios = new SelectList(destinatarios, "EmpleadoId", "FullName");
            return View(model);
        }

        [Authorize(Roles = Rols.Director)]
        public ActionResult Responder(int id)
        {
            NotaSolicitudCambio solicitud = _serviceNotaSolicitudCambio.Find
                (x=>x.NotaSolicitudCambioId==id,
                    x=>x.Nota,
                    x => x.Nota.EstudianteClase.Estudiante,
                    x => x.Nota.EstudianteClase.Clase.Asignatura,
                    x => x.Nota.EstudianteClase.Clase.Empleado,
                    x => x.Nota.EstudianteClase.Clase.Seccion,
                    x => x.Nota.EstudianteClase.Clase.Seccion.Grado
                ).FirstOrDefault();
            return View(solicitud);
        }

        [Authorize(Roles = Rols.Director)]
        [HttpPost]
        public ActionResult Responder(int id, bool aprobar)
        {
            NotaSolicitudCambio model = _serviceNotaSolicitudCambio.GetById(id);

            object attrs;

            if (aprobar)
            {
                try
                {
                    //cambiar la nota
                    var nota = _serviceNota.GetById(model.NotaId);
                    nota.Vigesimal = model.NotaNueva;
                    _serviceNota.Edit(nota);

                    //cambiar estado de solicitud
                    model.EstadoSolicitud = EstadoSolicitud.Aprobada;

                    attrs = new { success = "Solicitud aprobada correctamente, la nota se ha actualizado." };
                }
                catch (Exception e)
                {
                    attrs = new { success = "Error al modificar la nota." };
                }
            }
            else
            {
                model.EstadoSolicitud = EstadoSolicitud.Rechazada;
                attrs = new object();
            }
            _serviceNotaSolicitudCambio.Edit(model);
            return RedirectToAction("Index", attrs);
        }

        private void Validar(NotaSolicitudCambio model)
        {
            ModelState.Clear();
            if (model.NotaNueva < 1 || model.NotaNueva > 20)
                ModelState.AddModelError("NotaNueva", "Valor no válido");

            if (string.IsNullOrEmpty(model.Motivo))
                ModelState.AddModelError("Motivo", "Campo requerido");

            if (string.IsNullOrEmpty(model.NotaId.ToString()))
                ModelState.AddModelError("Nota", "Se Requiere una nota");

        }
    }
}