﻿using DB_ST.Domain;
using DB_ST.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using DB_ST.Web.Models.Extensions;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Director)]
    public class ClaseController : Controller
    {
        IMasterService<Clase> _service;
        IMasterService<Asignatura> _serviceAsignatura;
        IMasterService<AnioEscolar> _serviceAnio;
        private IMasterService<Seccion> _serviceSeccion;
        private IMasterService<Grado> _serviceGrado;
        private IMasterService<Empleado> _sEmpleado;
        // GET: Clase

        public ClaseController(
            IMasterService<Clase> service,
            IMasterService<Asignatura> serviceAsignatura,
            IMasterService<AnioEscolar> serviceAnio,
            IMasterService<Seccion> serviceSeccion,
            IMasterService<Grado> serviceGrado,
            IMasterService<Empleado> sEmpleado)
        {
            _service = service;
            _serviceAsignatura = serviceAsignatura;
            _serviceAnio = serviceAnio;
            _serviceSeccion = serviceSeccion;
            _serviceGrado = serviceGrado;
            _sEmpleado = sEmpleado;
        }

        public ActionResult Index(string SearchCriteria,int? seccionId, bool verDisabled = false)
        {
            ViewBag.VerDisabled = verDisabled/*.ToString().ToLower()*/;
            ViewBag.VerDisabledString = verDisabled.ToString().ToLower();

            ViewBag.Titulo = "Clases";

            SelectList anioEscolars = new SelectList(_serviceAnio.GetAll(), "AnioEscolarId", "Anio");
            var anioEscolarSelectedId = _serviceAnio.Find(x => x.Anio.Equals(DateTime.Now.Year.ToString())).FirstOrDefault().AnioEscolarId;
            SelectListItem anioEscolarSelected = anioEscolars.FirstOrDefault(x => x.Value == anioEscolarSelectedId.ToString());
            anioEscolars = new SelectList(_serviceAnio.GetAll(), "AnioEscolarId", "Anio", anioEscolarSelected);

            SelectList grados = new SelectList(_serviceGrado.GetAll(), "GradoId", "Nombre");

            var seccionesList = _serviceSeccion.Find(x => x.AnioEscolar.Anio.Equals(DateTime.Now.Year.ToString()));
            SelectList secciones = new SelectList(seccionesList, "SeccionId", "Nombre");

            if (seccionId != null)
            {
                var gradoSelectedId = _serviceSeccion.GetById(seccionId).GradoId;
                SelectListItem gradoSelected = grados.FirstOrDefault(x => x.Value == gradoSelectedId.ToString());
                grados = new SelectList(_serviceGrado.GetAll(), "GradoId", "Nombre", gradoSelected);

                SelectListItem seccionSelected = secciones.FirstOrDefault(
                    x => x.Value == seccionId.ToString());
                secciones = new SelectList(seccionesList.ToList().FindAll(x=>x.GradoId==gradoSelectedId), "SeccionId", "Nombre",seccionSelected);
            }

            ViewBag.AnioEscolars = anioEscolars;
            ViewBag.Grados = grados;
            ViewBag.Secciones = secciones;

            if (string.IsNullOrEmpty(SearchCriteria))
                SearchCriteria = "";

            var lista = _service.Find(
                        m => m.Nombre.Contains(SearchCriteria) && m.IsHabilitado == true,
                        m => m.Asignatura
                    );

            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Index", lista) : View(lista);
        }

        public ActionResult GetBySeccionId(int? seccionId, bool verDisabled=false)
        {
            IEnumerable<Clase> clases;

            clases = _service.Find(s => s.SeccionId == seccionId, x => x.Empleado, x => x.Asignatura);

            IEnumerable<Clase> clasesHabilitadas = clases.ToList().FindAll(x => x.IsHabilitado);

            //if (!verDisabled)
            //    clases = clasesHabilitadas;

            return PartialView("_GetBySeccionId", verDisabled? clases:clasesHabilitadas);
        }

        [AllowAnonymous]
        public JsonResult GetByAsignaturaId(int? asignaturaId, int? anioEscolarId)
        {
            List<Clase> lista = new List<Clase>();
            if (asignaturaId != null)
            {
                lista = _service.Find
                (
                    s => s.AsignaturaId == asignaturaId.Value,
                    x => x.Seccion
                ).ToList();

                if (User.IsInRole("docente"))
                {
                    var userEmpleadoId = int.Parse(User.Identity.GetUserEmpleadoId());
                    lista = lista.FindAll(x => x.EmpleadoId == userEmpleadoId);
                }
            }

            if (anioEscolarId != null)
                lista = lista.FindAll(x => x.Seccion.AnioEscolarId == anioEscolarId);
            else
                lista = lista.FindAll(x => x.Seccion.AnioEscolar.Anio.Equals(DateTime.Now.Year.ToString()));

            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Create(int? seccionId)
        {
            ViewBag.Titulo = "Registrar";

            ViewBag.Asignaturas = GetAsignaturasList();
            ViewBag.Anios = GetAniosList();
            ViewBag.Docentes = GetDocentesList();

            if (seccionId != null)
                return View(new Clase { SeccionId = seccionId.Value });
            else
                return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult Create(Clase model)
        {
            Validar(model);
            if (ModelState.IsValid)
            {
                model.IsHabilitado = true;
                _service.Add(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Titulo = "Modificar";

            ViewBag.Asignaturas = GetAsignaturasList();
            ViewBag.Anios = GetAniosList();
            ViewBag.Docentes = GetDocentesList();

            var model = _service.GetById(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Clase model)
        {
            Validar(model);
            if (ModelState.IsValid)
            {
                _service.Edit(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Details(int id)
        {
            ViewBag.Titulo = "Detalles";
            var model = _service.GetById(id);
            return View(model);
        }

        public ActionResult Disable(int? id)
        {
            ViewBag.Titulo = "Deshabilitar/Habilitar Clase";
            var model = _service.GetById(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Disable(int id)
        {
            Clase model = _service.GetById(id);
            if (ModelState.IsValid)
            {
                if (model.IsHabilitado == true)
                    model.IsHabilitado = false;
                else
                    model.IsHabilitado = true;
                _service.Edit(model);
                return RedirectToAction("Index");
            }
            return View();
        }

        public SelectList GetAsignaturasList()
        {
            return new SelectList(_serviceAsignatura.GetAll(), "AsignaturaId", "Nombre");
        }
        public SelectList GetDocentesList()
        {
            return new SelectList(_sEmpleado.Find(x=>x.TipoEmpleado.NombreTipoEmpleado.ToLower()=="docente"), "EmpleadoId", "FullName");
        }

        public SelectList GetAniosList()
        {
            return new SelectList(_serviceAnio.GetAll(), "AnioEscolarId", "Anio");
        }
        
        public ActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                _service.Remove(id);
            }
            return RedirectToAction("Index");
        }


        //Validaciones///////////////////////////////////////////////////////////////
        private void Validar(Clase clase)
        {
            ModelState.Clear();
            if (_service.Find(x => x.Nombre == clase.Nombre
                                    && x.ClaseId!=clase.ClaseId).Any())
                    ModelState.AddModelError("Nombre", "Ya Existe");
            if (string.IsNullOrEmpty(clase.Nombre))
                ModelState.AddModelError("Nombre", "Campo requerido");
        }
    }
}