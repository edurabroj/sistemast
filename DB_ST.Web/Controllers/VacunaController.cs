﻿using DB_ST.Domain;
using DB_ST.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Administrador)]
    public class VacunaController : Controller
    {
        IMasterService<Vacuna> _service;

        public VacunaController(IMasterService<Vacuna> service)
        {
            _service = service;
        }
        
        public ActionResult Index(string SearchCriteria)
        {
            if (string.IsNullOrEmpty(SearchCriteria))
                SearchCriteria = "";

            var lista = _service.Find(
                        m => m.Descripcion.Contains(SearchCriteria)
                        //, x => x.Estudiante, x => x.Seccion, x => x.Grado, x => x.AnioEscolar
                    );

            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Index", lista) : View(lista);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Vacuna model)
        {
            Validar(model);
            model.UlltimaModificacion = DateTime.Now;
            if (ModelState.IsValid)
            {
                _service.Add(model);
                return RedirectToAction("Index");
            }
            return null;
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Vacuna model)
        {
            model.UlltimaModificacion = DateTime.Now;
            Validar(model);
            if (ModelState.IsValid)
            {
                _service.Edit(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(Vacuna model)
        {
            if (ModelState.IsValid)
            {
                _service.Remove(model.VacunaId);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }
        //Validaciones///////////////////////////////////////////////////////////////
        private void Validar(Vacuna vacuna)
        {
            ModelState.Clear();

            if (_service.Find(x => x.TipoVacuna == vacuna.TipoVacuna
                                && x.VacunaId!=vacuna.VacunaId).Any())
                ModelState.AddModelError("TipoVacuna", "Ya Existe");
            if (string.IsNullOrEmpty(vacuna.TipoVacuna))
                ModelState.AddModelError("TipoVacuna", "Campo requerido");
            if (string.IsNullOrEmpty(vacuna.Descripcion))
                ModelState.AddModelError("Descripcion", "Campo requerido");
        }
    }
}