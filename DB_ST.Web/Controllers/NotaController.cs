﻿using DB_ST.Domain;
using DB_ST.Service;
using DB_ST.Web.Models.EntitiesVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using DB_ST.Web.Models.Extensions;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Director+","+Rols.Docente)]
    public class NotaController : Controller
    {
        IMasterService<AnioEscolar> _serviceAnioEscolar;
        IMasterService<Asignatura> _serviceAsignatura;
        IMasterService<Clase> _serviceClase;
        IMasterService<Estudiante> _serviceEstudiante;
        IMasterService<EstudianteClase> _serviceEstudianteClase;
        IMasterService<Nota> _serviceNota;
        IMasterService<NotaSolicitudCambio> _serviceNotaSolicitudCambio;
        IMasterService<Periodo> _servicePeriodo;
        IMasterService<Empleado> _serviceEmpleado;

        public NotaController
        (
            IMasterService<AnioEscolar> _serviceAnioEscolar,
            IMasterService<Asignatura> _serviceAsignatura,
            IMasterService<Clase> _serviceClase,
            IMasterService<Estudiante> _serviceEstudiante,
            IMasterService<EstudianteClase> _serviceEstudianteClase,
            IMasterService<Nota> _serviceNota,
            IMasterService<NotaSolicitudCambio> _serviceNotaSolicitudCambio,
            IMasterService<Periodo> _servicePeriodo,
            IMasterService<Empleado> _serviceEmpleado
        )
        {
            this._serviceAnioEscolar = _serviceAnioEscolar;
            this._serviceAsignatura = _serviceAsignatura;
            this._serviceClase = _serviceClase;
            this._serviceEstudiante = _serviceEstudiante;
            this._serviceEstudianteClase = _serviceEstudianteClase;
            this._serviceNota = _serviceNota;
            this._servicePeriodo = _servicePeriodo;
            this._serviceNotaSolicitudCambio = _serviceNotaSolicitudCambio;
            this._serviceEmpleado = _serviceEmpleado;
        }

        // GET: Nota
        public ActionResult Index(string error="", string success = "")
        {
            ViewBag.Titulo = "Notas";
            ViewBag.error = error;
            ViewBag.success = success;
            ViewBag.AnioEscolars = new SelectList( _serviceAnioEscolar.GetAll(),"AnioEscolarId","Anio");
            ViewBag.Asignaturas = new SelectList(_serviceAsignatura.GetAll(), "AsignaturaId", "Nombre");
            ViewBag.Clases = new SelectList(new List<Clase>(), "AsignaturaId", "Nombre");
            return View();
        }
        //----------------------------------------------------------------------------------------------------
        public ActionResult _GetNotasClase(int? claseId)
        {
            var claseVM = new __NotasClaseVM();

            var notasClaseVM = new List<__NotasEstudianteVM>();
            IEnumerable<Periodo> periodos = new List<Periodo>();
            Clase clase = new Clase();

            if (claseId!=null)
            {
                clase = _serviceClase.GetById(claseId);
                var estudiantesClaseIDs = _serviceEstudianteClase.Find(x => x.ClaseId == claseId).ToList().Select(x => x.EstudianteId);
                var estudiantes = _serviceEstudiante.GetAll().ToList();

                var estudiantesClase = from e in estudiantes
                                       from id in estudiantesClaseIDs
                                       where e.EstudianteId == id
                                       select e;

                foreach (var estudiante in estudiantesClase)
                {
                    var studentNota = new __NotasEstudianteVM();
                    notasClaseVM.Add
                        (
                            new __NotasEstudianteVM
                            {
                                Estudiante = estudiante,
                                Notas = _serviceNota.Find
                                (
                                    x => x.EstudianteClase.EstudianteId == estudiante.EstudianteId &&
                                    x.EstudianteClase.ClaseId==claseId,
                                    x=>x.EstudianteClase
                                )
                            }
                        );
                }

                var anioId = _serviceClase.Find(x => x.ClaseId == claseId, x => x.Seccion).FirstOrDefault().Seccion.AnioEscolarId;

                periodos = GetPeriodosByAnio(anioId);
            }

            claseVM.NotasEstudiante = notasClaseVM;
            claseVM.Periodos = periodos;
            claseVM.Clase = clase;
            return PartialView(claseVM);
        }

        public List<Periodo> GetPeriodosByAnio(int anioId)
        {
            return  _servicePeriodo.Find
                    (
                        x => x.AnioEscolarId == anioId
                    ).ToList();
        }


        //-------------------------------------------------------------------------------------------------------------------------
        public ActionResult Create(int estudianteId,int claseId, int periodoId)
        {
            var periodo = _servicePeriodo.GetById(periodoId);
            var hoy = DateTime.Now;

            if((hoy<periodo.IngresoNotasInicio || hoy > periodo.IngresoNotasFin) && !User.IsInRole("director"))
            {
                return RedirectToAction("Index", new {error="El plazo de registro de notas para el periodo ha expirado."});
            }

            var estudianteClase = _serviceEstudianteClase.Find
                (
                    x=>x.EstudianteId==estudianteId &&
                    x.ClaseId==claseId,

                    x => x.Estudiante,
                    x => x.Clase,
                    x => x.Clase.Asignatura
                ).FirstOrDefault();

            ViewBag.Titulo = "Registrar";

            ViewBag.Alumno = estudianteClase.Estudiante.FullName;
            ViewBag.Asignatura = estudianteClase.Clase.Asignatura.Nombre;
            ViewBag.Clase = estudianteClase.Clase.Nombre;
            ViewBag.Periodo = periodo.Nombre;

            return View(new Nota
            {
                EstudianteClaseId=estudianteClase.EstudianteClaseId,
                PeriodoId=periodoId
            });
        }

        [HttpPost]
        public ActionResult Create(Nota model)
        {
            Validar(model);
            if (ModelState.IsValid)
            {
                _serviceNota.Add(model);
                return RedirectToAction("Index");
            }

            var periodo = _servicePeriodo.GetById(model.PeriodoId);
            var estudianteClase = _serviceEstudianteClase.Find
                (
                    x => x.EstudianteClaseId == model.EstudianteClaseId,
                    x => x.Estudiante,
                    x => x.Clase,
                    x => x.Clase.Asignatura
                ).FirstOrDefault();
            
            ViewBag.Alumno = estudianteClase.Estudiante.FullName;
            ViewBag.Asignatura = estudianteClase.Clase.Asignatura.Nombre;
            ViewBag.Clase = estudianteClase.Clase.Nombre;
            ViewBag.Periodo = periodo.Nombre;

            return View(model);
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _serviceNota.Find(x=>x.NotaId==id,
                    x => x.Periodo,
                    x => x.EstudianteClase.Estudiante,
                    x => x.EstudianteClase.Clase,
                    x => x.EstudianteClase.Clase.Asignatura
                ).FirstOrDefault();

            if (model == null)
            {
                return HttpNotFound();
            }

            var periodo = _servicePeriodo.GetById(model.PeriodoId);
            var hoy = DateTime.Now;
            if ((hoy < periodo.IngresoNotasInicio || hoy > periodo.IngresoNotasFin ) && !User.IsInRole("director"))
            {
                return RedirectToAction("Create","NotaSolicitudCambio", new { notaId=model.NotaId });
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Nota model)
        {
            Validar(model);
            if (ModelState.IsValid)
            {
                _serviceNota.Edit(model);
                return RedirectToAction("Index");
            }
            model = _serviceNota.Find(x => x.NotaId == model.NotaId,
                    x => x.Periodo,
                    x => x.EstudianteClase.Estudiante,
                    x => x.EstudianteClase.Clase,
                    x => x.EstudianteClase.Clase.Asignatura
                ).FirstOrDefault();
            return View(model);
        }

        
        private void Validar(Nota nota)
        {
            ModelState.Clear();
            if (nota.Vigesimal<1 || nota.Vigesimal>20)
                ModelState.AddModelError("Vigesimal", "Valor no válido");
            if (string.IsNullOrEmpty(nota.Vigesimal.ToString()))
                ModelState.AddModelError("Nota", "Campo requerido");
        }

    }
}