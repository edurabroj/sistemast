﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;
using System.Linq.Expressions;
using DB_ST.Domain.Enum;
using System.Net;
using PagedList;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Secretario)]
    public class MatriculaController : Controller
    {
        string stringPlazoExpirado = "El plazo de matrícula ha expirado.";

        IMasterService<Matricula> _service;
        IMasterService<Seccion> _serviceSeccion;
        IMasterService<Estudiante> _serviceEstudiante;
        IMasterService<Grado> _serviceGrado;
        IMasterService<AnioEscolar> _serviceAnioEscolar;
        IMasterService<Apoderado> _serviceApoderado;
        IMasterService<EstudianteEstadoAnio> _serviceEstudianteEstadoAnio;
        IMasterService<Clase> _serviceClase;
        IMasterService<EstudianteClase> _serviceEstudianteClase;
        IMasterService<Nota> _serviceNota;
        IMasterService<Periodo> _servicePeriodo;

        SelectList anioEscolars;
        SelectList grados;
        SelectList secciones;

        public MatriculaController
        (
            IMasterService<Matricula> service,
            IMasterService<Seccion> serviceSeccion,
            IMasterService<Estudiante> serviceEstudiante,
            IMasterService<Grado> serviceGrado,
            IMasterService<AnioEscolar> serviceAnioEscolar,
            IMasterService<Apoderado> serviceApoderado,
            IMasterService<EstudianteEstadoAnio> serviceEstudianteEstadoAnio,
            IMasterService<Clase> serviceClase,
            IMasterService<EstudianteClase> serviceEstudianteClase,
            IMasterService<Nota> serviceNota,
            IMasterService<Periodo> servicePeriodo
        )
        {
            _service = service;
            _serviceSeccion = serviceSeccion;
            _serviceEstudiante = serviceEstudiante;
            _serviceGrado = serviceGrado;
            _serviceAnioEscolar = serviceAnioEscolar;
            _serviceApoderado = serviceApoderado;
            _serviceEstudianteEstadoAnio = serviceEstudianteEstadoAnio;
            _serviceClase = serviceClase;
            _serviceEstudianteClase = serviceEstudianteClase;
            _serviceNota = serviceNota;
            _servicePeriodo = servicePeriodo;

            anioEscolars = new SelectList(_serviceAnioEscolar.GetAll(), "AnioEscolarId", "Anio");
            grados = new SelectList(_serviceGrado.GetAll(), "GradoId", "Nombre");
            secciones = new SelectList(new List<Seccion>(), "SeccionId", "Nombre");
        }

        //GET: Matricula
        public ActionResult Index(string SearchCriteria, int? AnioEscolarId, int? GradoId, int? SeccionId, int? page)
        {
            ViewBag.success = TempData["success"];


            if (string.IsNullOrEmpty(SearchCriteria))
                SearchCriteria = "";
            
            SearchCriteria = SearchCriteria.ToLower().Trim();

            var lista = _service.Find(
                        m => m.Estudiante.DNI.Contains(SearchCriteria) ||
                        m.Estudiante.Nombres.ToLower().Trim().Contains(SearchCriteria) ||
                        m.Estudiante.Apellidos.ToLower().Trim().Contains(SearchCriteria) ||
                        (m.Estudiante.Nombres + " " + m.Estudiante.Apellidos).ToLower().Trim().Contains(SearchCriteria)
                        , x => x.Estudiante, x => x.Seccion, x => x.Grado, x => x.AnioEscolar
                    );

            if (AnioEscolarId != null) lista = lista.Where(x => x.AnioEscolarId == AnioEscolarId);
            if (GradoId != null) lista = lista.Where(x => x.GradoId == GradoId);
            if (SeccionId != null) lista = lista.Where(x => x.SeccionId == SeccionId);

            ViewBag.AnioEscolars = anioEscolars;
            ViewBag.Grados = grados;
            ViewBag.Secciones = secciones;

            ViewBag.SearchCriteria = SearchCriteria;
            ViewBag.AnioEscolarId = AnioEscolarId;
            ViewBag.GradoId = GradoId;
            ViewBag.SeccionId = SeccionId;

            var pageIndex = page ?? 1;
            lista = lista.OrderByDescending(x=>x.FechaMatricula).ToPagedList(pageIndex, Configuraciones.defaultPageSize);

            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Index", lista) : View(lista);
        }

        public ActionResult SelectEstudiante(string nroDni)
        {
            var modelo = new Estudiante();
            if (!string.IsNullOrEmpty(nroDni)) {
                var query=_serviceEstudiante.Find(
                            m => m.DNI.Contains(nroDni)
                        ).ToList();
                if (query.Count>0)
                    modelo = query.First();
            }
            return PartialView("_SelectEstudiante", modelo);
        }

        public ActionResult SelectApoderado(string nroDni)
        {
            var modelo = new Apoderado();
            if (!string.IsNullOrEmpty(nroDni))
            {
                var query = _serviceApoderado.Find(
                            m => m.DNI.Contains(nroDni)
                        ).ToList();
                if (query.Count > 0)
                    modelo = query.First();
            }
            return PartialView("_SelectApoderado", modelo);
        }

        public ActionResult Create()
        {
            var hoy = DateTime.Now;
            var anioActual = GetAnioActual();

            if (hoy.Date < anioActual.FechaInicioMatricula || hoy.Date > anioActual.FechaFinMatricula)
                ViewBag.error = stringPlazoExpirado;
            if (hoy.Date == anioActual.FechaMatriculaContemporanea.Date)
            {
                ViewBag.error = null;
                ViewBag.info = "La matrícula es extemporánea.";
            }

            if (TempData["error"] != null) ViewBag.error = TempData["error"];
            if (ViewBag.error != null) ViewBag.info = null;

            //ViewBag.error = TempData["error"] !=null? TempData["error"] : null;
            //ViewBag.info = TempData["info"] != null ? TempData["info"] : null;
            //ViewBag.success = TempData["success"] != null ? TempData["success"] : null;

            //var secciones = _serviceSeccion.Find(x=>SeccionTieneCupos(x));
            //ViewBag.Secciones = new SelectList(secciones, "SeccionId", "Nombre");

            var estudiantes = _serviceEstudiante.GetAll().OrderBy(x => x.FullName)
                .Where(x => EstudianteEstaMatriculado(x.EstudianteId, GetAnioActual().AnioEscolarId) == false);
            ViewBag.Estudiantes = new SelectList(estudiantes, "EstudianteId", "DniFullName");

            var apoderados = _serviceApoderado.GetAll().OrderBy(x => x.FullName);
            ViewBag.Apoderados = new SelectList(apoderados, "ApoderadoId", "DniFullName");

            var grados = _serviceGrado.GetAll();
            ViewBag.Grados = new SelectList(grados, "GradoId", "Nombre");

            var anios = _serviceAnioEscolar.GetAll();
            ViewBag.Anios = new SelectList(anios, "AnioEscolarId", "Anio");

            Matricula model = new Matricula { FechaMatricula=hoy};
            EnFechaValida(model);
            //model.TipoDeMatricula = "Regular";
            model.NumeroVoucher = "01021415";
            //model.AnioEscolar.Anio = DateTime.Now.Year.ToString();
            return View(model);
        }

        public JsonResult GetGradosByEstudiante(int estudianteId)
        {
            var notas=_serviceNota.Find(x => x.EstudianteClase.EstudianteId == estudianteId,
                x=>x.EstudianteClase.Clase.Seccion.AnioEscolar);
            var grados = new List<Grado>();

            if (notas.Any())
            {
                var ultAnio = notas.OrderByDescending(x => int.Parse(x.EstudianteClase.Clase.Seccion.AnioEscolar.Anio))
                    .FirstOrDefault().EstudianteClase.Clase.Seccion.AnioEscolar;                

                //clase = _serviceClase.GetById(claseId);
                var clasesIDs = _serviceEstudianteClase
                    .Find(x => x.EstudianteId == estudianteId && 
                               x.Clase.Seccion.AnioEscolarId== ultAnio.AnioEscolarId)
                    .ToList()
                    .Select(x => x.ClaseId);

                var notasTodas = _serviceNota.GetAll(x=>x.EstudianteClase.Clase.Seccion).ToList();

                var notasUltAnio = from e in notasTodas
                                       from id in clasesIDs
                                       where e.EstudianteClase.ClaseId == id
                                       select e;

                if (notasUltAnio.Count() > 0)
                {
                    var utlGrado = _serviceGrado.GetById(notasUltAnio.FirstOrDefault()
                                                         .EstudianteClase.Clase.Seccion.GradoId);
                    if (notasUltAnio.Count() == _servicePeriodo.Find(x => x.AnioEscolarId == ultAnio.AnioEscolarId).Count())
                    {
                        var grado = _serviceGrado.Find(x => x.Orden > utlGrado.Orden).OrderBy(x => x.Orden).FirstOrDefault();
                        grados.Add(grado);
                    }else
                    {
                        grados.Add(utlGrado);
                    }
                }
                    
                else
                    grados.Add
                    (
                        _service.Find(
                            x => x.AnioEscolarId == ultAnio.AnioEscolarId,
                            x => x.AnioEscolar
                            ).FirstOrDefault().Grado
                    );
            }else
            {
                grados = _serviceGrado.GetAll().ToList();
            }

            return Json(grados, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult Create(Matricula model)
        {
            ViewBag.error = TempData["error"];
            AnioEscolar anio = GetAnioActual();
            model.AnioEscolarId = anio.AnioEscolarId;

            Estudiante estudiante = _serviceEstudiante.Find(x => x.EstudianteId == model.EstudianteId).First();
            //Si El estado inicial es trasladado y nunca ha sido matriculado
            if (EsTrasladado(estudiante))
                model.EstadoEstudiante = EstadoEstudiante.Trasladado;
            else
                model.EstadoEstudiante = EstadoEstudiante.Regular;

            Validar(model);
            if (ModelState.IsValid)
            {
                var clases =_serviceClase.Find(x=>x.SeccionId==model.SeccionId);
                if (clases.Count() > 0)
                {
                    foreach (var item in clases)
                    {
                        _serviceEstudianteClase.Add(
                            new EstudianteClase
                            {
                                ClaseId = item.ClaseId,
                                EstudianteId= estudiante.EstudianteId
                            });
                    }
                }

                _service.Add(model);

                //añadir un matriculado
                //var seccion = _serviceSeccion.GetById(model.SeccionId);
                //seccion.Matriculados++;
                //_serviceSeccion.Edit(seccion);

                TempData["success"] = "Matrícula registrada correctamente";
                return RedirectToAction("Index");
            }
            return RedirectToAction("Create");
        }
        
        private void Validar(Matricula model)
        {
            var anio = GetAnioActual();

            ModelState.Clear();
            
            if(!EnFechaValida(model))
            {
                TempData["error"] = stringPlazoExpirado;
                ModelState.AddModelError("EstudianteId", stringPlazoExpirado);
            }
                //ERROR: No se puede matricular en la fecha actual
            

            if (EstudianteEstaMatriculado(model.EstudianteId,model.AnioEscolarId))
            {
                var yaEstaMatriculado="El estudiante ya se encuentra matriculado en el presente año académico.";
                TempData["error"] = yaEstaMatriculado;
                ModelState.AddModelError("EstudianteId", yaEstaMatriculado);
            }        
            
            //quedan cupos
            var seccionMatricula = _serviceSeccion.GetById(model.SeccionId);
            if (!SeccionTieneCupos(seccionMatricula))
            //if (seccionMatricula.Cupos<1)
            {
                var noCupos = "No hay cupos en la sección.";
                TempData["error"] = noCupos;
                ModelState.AddModelError("SeccionId", noCupos);
            }

            if(_service.Find(x=>x.NumeroVoucher==model.NumeroVoucher).Any())
                ModelState.AddModelError("NumeroVoucher", "Campo repetido");

            var estudiante = _serviceEstudiante.GetById(model.EstudianteId);
            if (estudiante.EstadoEstudiante == EstadoEstudiante.Trasladado)
            {
                if(string.IsNullOrEmpty(model.CodigoInsitucionOrigen))
                    ModelState.AddModelError("CodigoInsitucionOrigen", "Campo requerido");
                if (string.IsNullOrEmpty(model.RazonTraslado))
                    ModelState.AddModelError("RazonTraslado", "Campo requerido");
            }
            
            if (model.ApoderadoId <= 0 )
            { 
                    ModelState.AddModelError("ApoderadoId", "Campo requerido");
            }
            if (model.EstudianteId <= 0)
            {
                    ModelState.AddModelError("EstudianteId", "Campo requerido");
            }


        }

        private bool SeccionTieneCupos(Seccion seccionMatricula)
        {
            return _service.Find(x => x.SeccionId == seccionMatricula.SeccionId).Count() < seccionMatricula.Capacidad;
        }


        private bool EnFechaValida(Matricula model)
        {
            var anio = GetAnioActual();
            if (model.FechaMatricula.Date == anio.FechaMatriculaContemporanea.Date)
            {
                model.TipoMatricula = TipoMatricula.Extemporanea;
                model.MontoMatricula = anio.MontoMatriculaExtemporanea;
                return true;
            }
            if (model.FechaMatricula.Date >= anio.FechaInicioMatricula.Date &&
                      model.FechaMatricula.Date <= anio.FechaFinMatricula.Date)
            {
                model.TipoMatricula = TipoMatricula.Regular;
                model.MontoMatricula = anio.MontoMatriculaRegular;
                return true;
            }
            return false;
        }

        private bool EstudianteEstaMatriculado(int estudianteId, int anioEscolarId)
        {
            return _service.Find(x => x.EstudianteId == estudianteId &&
                                 x.AnioEscolarId == anioEscolarId).Any();
        }

        public AnioEscolar GetAnioActual()
        {
            return _serviceAnioEscolar.Find(
                   x => x.Anio.Equals(DateTime.Now.Year.ToString())
                   ).First();
        }

        public ActionResult Edit(int id)
        {
            var model = _service.Find(p => p.MatriculaId == id,
                p => p.Estudiante,
                p => p.Apoderado,
                p => p.Grado,
                p => p.Seccion,
                p => p.AnioEscolar).FirstOrDefault();


            var grados = _serviceGrado.GetAll();
            ViewBag.Grados = new SelectList(grados, "GradoId", "Nombre");

            var secciones = _serviceSeccion.Find(
                x=>x.AnioEscolarId == model.AnioEscolarId &&
                x.GradoId == model.GradoId);

            ViewBag.Secciones = new SelectList(secciones, "SeccionId", "Nombre");
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Matricula model)
        {
            if (ModelState.IsValid)
            {
                _service.Edit(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.Find(p => p.MatriculaId == id,
                p => p.Estudiante,
                p => p.Apoderado,
                p => p.Grado,
                p => p.Seccion,
                p => p.AnioEscolar).First();

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }
        //[HttpPost]
        //public ActionResult DeletePost(int id)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _service.Remove(id);
        //        return RedirectToAction("Index");
        //    }
        //    return View(_service.GetById(id));
        //}
        [HttpPost]
        public ActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                var matricula = _service.GetById(id);
                var anio = _serviceAnioEscolar.GetById(matricula.AnioEscolarId);
                var clases = _serviceClase.Find(x => x.Seccion.AnioEscolarId == anio.AnioEscolarId).ToList();
                if (clases.Count() > 0)
                {
                    foreach (var item in clases)
                    {
                        var estudianteClase = _serviceEstudianteClase.Find(x => x.ClaseId == item.ClaseId &&
                                                                            x.EstudianteId==matricula.EstudianteId)
                                                                            .FirstOrDefault();
                        //var estudianteClaseId = estudianteClase.EstudianteClaseId;


                        var notas = _serviceNota.Find(x => x.EstudianteClaseId == estudianteClase.EstudianteClaseId).ToList();

                        foreach (var nota in notas)
                        {
                            //borrar notas
                            _serviceNota.Remove(nota.NotaId);
                        }
                        ////borrar estClase
                        _serviceEstudianteClase.Remove(estudianteClase.EstudianteClaseId);
                    }
                }
                //borro matricula
                _service.Remove(id);
                return RedirectToAction("Index");
            }
            return View(id);
        }

        public ActionResult Details(int id)
        {
            //var model = _service.GetById(id);

            var model = _service.Find(p => p.MatriculaId == id,
                p => p.Estudiante,
                p => p.Apoderado,
                p => p.Grado,
                p => p.Seccion,
                p => p.AnioEscolar).FirstOrDefault();
            return View(model);
        }

        //public JsonResult GetGradosByAnio(int id)
        //{
        //    return Json(_serviceGrado.Find(x=>x.AnioId==id), JsonRequestBehavior.AllowGet);
        //}
        [AllowAnonymous]
        public JsonResult GetSeccionesByGrado(int? id, int? docenteId, int? anioEscolarId, bool soloConCupos=false)
        {
            List<Seccion> lista;
            lista = new List<Seccion>();
            if (id != null)
                lista = _serviceSeccion.Find(x => x.GradoId == id, x=>x.AnioEscolar).ToList();

            if (anioEscolarId != null)
                lista = lista.FindAll(x => x.AnioEscolarId == anioEscolarId);
            else
                lista = lista.FindAll(x => x.AnioEscolar.Anio.Equals(DateTime.Now.Year.ToString()));

            if (docenteId != null)
                lista = lista.FindAll(x => x.EmpleadoId == docenteId);

            if(soloConCupos)
                lista = lista.FindAll(x => SeccionTieneCupos(x));

            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTraslado(int? id)
        {
            if (id == null || id==0)
                return null;

            var model = _serviceEstudiante.GetById(id);
            if (EsTrasladado(model))
            {
                return PartialView("_TrasladoEntrante", new Matricula());
            }
            return null;
        }

        public bool EsTrasladado(Estudiante estudiante)
        {
            return (estudiante.EstadoEstudiante == EstadoEstudiante.Trasladado &&
                   !_service.Find(x => x.EstudianteId == estudiante.EstudianteId).Any());
        }



    }
}