﻿using DB_ST.Domain;
using DB_ST.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Administrador)]
    public class TipoEmpleadoController : Controller
    {
        IMasterService<TipoEmpleado> _service;

        public TipoEmpleadoController(IMasterService<TipoEmpleado> service)
        {
            _service = service;
        }

        public ActionResult Index(string SearchCriteria)
        {
            if (string.IsNullOrEmpty(SearchCriteria))
                SearchCriteria = "";

            var lista = _service.Find(
                        m => m.NombreTipoEmpleado.Contains(SearchCriteria)
                        //, x => x.Estudiante, x => x.Seccion, x => x.Grado, x => x.AnioEscolar
                    );

            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Index", lista) : View(lista);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(TipoEmpleado model)
        {
            Validar(model);

            model.UlltimaModificacion = DateTime.Now;
            if (ModelState.IsValid)
            {
                _service.Add(model);
                return RedirectToAction("Index");
            }
            return null;
        }

        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(TipoEmpleado model)
        {
            model.UlltimaModificacion = DateTime.Now;
            Validar(model);
            if (ModelState.IsValid)
            {
                _service.Edit(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(TipoEmpleado model)
        {
            if (ModelState.IsValid)
            {
                _service.Remove(model.TipoEmpleadoId);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var model = _service.GetById(id);

            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }
        //Validaciones///////////////////////////////////////////////////////////////
        private void Validar(TipoEmpleado tipoEmpleado)
        {
            ModelState.Clear();
            
            if (_service.Find(x => x.NombreTipoEmpleado == tipoEmpleado.NombreTipoEmpleado
                                && x.TipoEmpleadoId!=tipoEmpleado.TipoEmpleadoId).Any())
                ModelState.AddModelError("TipoEmpleado", "Ya Existe");
            if (string.IsNullOrEmpty(tipoEmpleado.NombreTipoEmpleado))
                ModelState.AddModelError("NombreTipoEmpleado", "Campo requerido");
        }
    }
}