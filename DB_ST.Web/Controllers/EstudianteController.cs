﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;
using System.Text.RegularExpressions;
using DB_ST.Web.Validators;
using PagedList;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Secretario)]
    public class EstudianteController : Controller
    {
        IMasterService<Estudiante> _service;
        //ubigeo
        IMasterService<Departamento> _serviceDep;
        IMasterService<Provincia> _servicePro;
        IMasterService<Distrito> _serviceDis;

        SelectList deps;
        SelectList pros;
        SelectList diss;

        public EstudianteController(IMasterService<Estudiante> service,
            IMasterService<Departamento> serviceDep,
            IMasterService<Provincia> servicePro,
            IMasterService<Distrito> serviceDis)
        {
            _service = service;

            _serviceDep = serviceDep;
            _servicePro = servicePro;
            _serviceDis = serviceDis;

            deps = new SelectList(_serviceDep.GetAll(), "DepartamentoId", "Nombre");
            pros = new SelectList(_servicePro.GetAll(), "ProvinciaId", "Nombre");
            diss = new SelectList(_serviceDis.GetAll(), "DistritoId", "Nombre");
        }

        // GET: Estudiante
        public ActionResult Index(string SearchCriteria, int? page)
        {
            //var datos = _service.GetEstudiantes(SearchCriteria);
            //return View(datos);

            ViewBag.Titulo = "Estudiantes";


            if (string.IsNullOrEmpty(SearchCriteria))
                SearchCriteria = "";

            var lista = _service.Find(
                        m => m.DNI.Contains(SearchCriteria) && m.Habilitado,m=> m.Distrito
                    );

            ViewBag.SearchCriteria = SearchCriteria;

            var pageIndex = page ?? 1;
            lista = lista.OrderBy(p => p.Apellidos).ToPagedList(pageIndex, Configuraciones.defaultPageSize);

            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Index", lista) : View(lista);
        }

        public ActionResult Create()
        {
            ViewBag.Deps = deps;
            ViewBag.Pros = pros;
            ViewBag.Diss = diss;

            ViewBag.Titulo = "Registrar";
            return View();
        }
        [HttpPost]
        public ActionResult Create(Estudiante estudiante)
        {
            Validar(estudiante);

            if (ModelState.IsValid)
            {
                estudiante.Habilitado = true;
                _service.Add(estudiante);
                return RedirectToAction("Index");
            }
            ViewBag.Deps = deps;
            ViewBag.Pros = pros;
            ViewBag.Diss = diss;

            return View(estudiante);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Deps = deps;
            ViewBag.Pros = pros;
            ViewBag.Diss = diss;

            ViewBag.Titulo = "Editar";
            var model = _service.GetById(id);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Estudiante estudiante)
        {
            Validar(estudiante);
            if (ModelState.IsValid)
            {
                _service.Edit(estudiante);
                return RedirectToAction("Index");
            }
            ViewBag.Deps = deps;
            ViewBag.Pros = pros;
            ViewBag.Diss = diss;
            return View(estudiante);
        }

        public ActionResult Details(int id)
        {
            ViewBag.Titulo = "Detalles";
            var model = _service.GetById(id);
            return View(model);
        }

        public ActionResult Disable(int? id)
        {
            ViewBag.Titulo = "Deshabilitar/Habilitar";
            var model = _service.GetById(id);
            return View(model);
        }
        [HttpPost]
        public ActionResult Disable(int id)
        {
            Estudiante estudiante= _service.GetById(id);
            if (ModelState.IsValid)
            {
                if (estudiante.Habilitado == true)
                    estudiante.Habilitado = false;
                else
                    estudiante.Habilitado = true;
                _service.Edit(estudiante);
                return RedirectToAction("Index");
            }
            return View();
        }
        //[HttpPost]
        //public ActionResult Disable(Estudiante estudiante)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        if(estudiante.Habilitado == true)
        //            estudiante.Habilitado = false;
        //        else
        //            estudiante.Habilitado = false;

        //        _service.Edit(estudiante);
        //        return RedirectToAction("Index");
        //    }
        //    return View();
        //}

        [HttpPost]
        public ActionResult Delete(Estudiante estudiante)
        {
            if (ModelState.IsValid)
            {
                //estudiante.Habilitado = false;
                _service.Remove(estudiante.EstudianteId);
                return RedirectToAction("Index");
            }
            return View();
        }
//----------------------------------------------------------------------------
        [AllowAnonymous]
        public JsonResult GetProvincias(int? id)
        {
            List<Provincia> lista;
            if (id != null)
                lista = _servicePro.Find(x => x.DepartamentoId == id).ToList();
            else
                lista = new List<Provincia>();

            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult GetDistritos(int? id)
        {
            List<Distrito> lista;
            if (id != null)
                lista = _serviceDis.Find(x => x.ProvinciaId == id).ToList();
            else
                lista = new List<Distrito>();

            return Json(lista, JsonRequestBehavior.AllowGet);
        }


        //Validaciones///////////////////////////////////////////////////////////////
        private void Validar(Estudiante estudiante)
        {
            ModelState.Clear();

            if (_service.Find(x => x.DNI == estudiante.DNI && x.EstudianteId!=estudiante.EstudianteId).Any())
                ModelState.AddModelError("DNI", "Campo repetido");
            if (string.IsNullOrEmpty(estudiante.Nombres))
                ModelState.AddModelError("Nombres", "Campo requerido");
            if (string.IsNullOrEmpty(estudiante.Apellidos))
                ModelState.AddModelError("Apellidos", "Campo requerido");
            if (!string.IsNullOrEmpty(estudiante.DNI) && !Validator.IsDNI(estudiante.DNI))
                ModelState.AddModelError("DNI", "Inválido.");
            if (string.IsNullOrEmpty(estudiante.FechaNacimiento.ToString()))
                ModelState.AddModelError("Naciemiento", "Campo requerido");
            if (string.IsNullOrEmpty(estudiante.Direccion))
                ModelState.AddModelError("Direccion", "Campo requerido");
        }
    }
}
