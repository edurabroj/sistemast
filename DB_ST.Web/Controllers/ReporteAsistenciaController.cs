﻿using DB_ST.Domain;
using DB_ST.Domain.Enum;
using DB_ST.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using DB_ST.Web.Models.Extensions;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Apoderado +"," + Rols.Director + "," +
                       Rols.Administrador+","+Rols.Docente+","+ 
                       Rols.Secretario)]
    public class ReporteAsistenciaController : Controller
    {
        IMasterService<AnioEscolar> _serviceAnioEscolar;
        IMasterService<AsistenciaAlumno> _serviceAsistencia;
        IMasterService<Estudiante> _serviceEstudiante;
        IMasterService<Matricula> _serviceMatricula;

        string userId;
        SelectList estudiantes;

        public ReporteAsistenciaController
            (
                IMasterService<AnioEscolar> _serviceAnioEscolar,
                IMasterService<AsistenciaAlumno> _serviceAsistencia,
                IMasterService<Estudiante> _serviceEstudiante,
                IMasterService<Matricula> _serviceMatricula
            )
        {
            this._serviceAnioEscolar = _serviceAnioEscolar;
            this._serviceAsistencia = _serviceAsistencia;
            this._serviceEstudiante=_serviceEstudiante;
            this._serviceMatricula = _serviceMatricula;
        }

        // GET: ReporteAsistencia
        public ActionResult Index()
        {
            List<Estudiante> estudiantesSelect;
            if (User.IsInRole(Rols.Apoderado))
            {
                userId = User.Identity.GetUserEmpleadoId();
                estudiantesSelect = GetEstudiantesByApoderado(int.Parse(userId));
            }
            else
            {
                estudiantesSelect = _serviceEstudiante.GetAll().ToList(); 
            }
            estudiantes = new SelectList(estudiantesSelect,
                                            "EstudianteId", "DniFullName");
            ViewBag.Estudiantes = estudiantes;
            return View();
        }

        public List<Estudiante> GetEstudiantesByApoderado(int apoderadoId)
        {
            var estudiantesIDs = _serviceMatricula.Find(x => x.ApoderadoId == apoderadoId).Select(x => x.EstudianteId);
            var estudiantes = _serviceEstudiante.GetAll().ToList();

            return (from e in estudiantes
                                   from id in estudiantesIDs
                                   where e.EstudianteId == id
                                   select e).ToList();
        }

        public ActionResult _GetAsistencia(int estudianteId, int? anioEscolarId)
        {
            var asistencias = _serviceAsistencia.Find(x => x.EstudianteId == estudianteId);
            if (anioEscolarId != null )
            {
                var anio = _serviceAnioEscolar.GetById(anioEscolarId);
                if(anio!=null)
                    asistencias = asistencias.Where(x => x.Fecha.Year.ToString() == anio.Anio);
            }
            asistencias = asistencias.Take(30);
            ViewBag.Asistio = asistencias.Where(x => x.ValorAsistencia == ValorAsistencia.Asistio).Count();
            ViewBag.Falto = asistencias.Where(x => x.ValorAsistencia == ValorAsistencia.Falto).Count();

            return PartialView(asistencias.OrderByDescending(x=>x.Fecha));
        }
    }
}