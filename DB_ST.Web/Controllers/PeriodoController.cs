﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Director)]
    public class PeriodoController : Controller
    {
        private IMasterService<Periodo> _periodo;
        private IMasterService<AnioEscolar> _anios;
        private IMasterService<Nota> _nota;
        public PeriodoController
        (
            IMasterService<Periodo> _periodo,
            IMasterService<AnioEscolar> _anios,
            IMasterService<Nota> _nota
        )
        {
            this._periodo = _periodo;
            this._anios = _anios;
            this._nota = _nota;
        }
        // GET: Periodo
        public ActionResult Index()
        {
            ViewBag.error = TempData["error"];
            ViewBag.AnioEscolars = new SelectList(_anios.GetAll(), "AnioEscolarId", "Anio");
            return View();
        }

        public ActionResult GetByAnioEscolar(int anioId)
        {
            var periodos = _periodo.Find(x => x.AnioEscolarId == anioId);
            return PartialView(periodos);
        }

        public ActionResult Create(int? anioId)
        {
            ViewBag.AnioEscolar = _anios.GetById(anioId).Anio;
            if (anioId != null)
                return View(
                    new Periodo
                    {
                        AnioEscolarId = anioId.Value,
                        Orden = GetPeriodoOrden(anioId.Value)
                    }
                );
            else
                return RedirectToAction("Index");
        }

        private int GetPeriodoOrden(int anioId)
        {
            var ultPeriodo = _periodo.Find(x => x.AnioEscolarId == anioId)
                                .OrderBy(x => x.Orden).LastOrDefault();
            if (ultPeriodo == null)
                return 0;
            else
                return ultPeriodo.Orden + 1;
        }

        [HttpPost]
        public ActionResult Create(Periodo model)
        {
            Validar(model);
            if (ModelState.IsValid)
            {
                _periodo.Add(model);
                return RedirectToAction("Index");
            }
            ViewBag.AnioEscolar = _anios.GetById(model.AnioEscolarId).Anio;
            return View(model);
        }

        public ActionResult Edit(int id)
        {
            var model = GetPeriodoCompleto(id);
            return View(model);
        }

        private Periodo GetPeriodoCompleto(int id)
        {
            return _periodo.Find(x => x.PeriodoId == id, x => x.AnioEscolar).FirstOrDefault();
        }

        [HttpPost]
        public ActionResult Edit(Periodo model)
        {
            Validar(model);
            if (ModelState.IsValid)
            {
                _periodo.Edit(model);
                return RedirectToAction("Index");
            }
            return View(GetPeriodoCompleto(model.PeriodoId));
        }

        private void Validar(Periodo model)
        {
            ModelState.Clear();
            if (string.IsNullOrEmpty(model.IngresoNotasInicio.ToString()))
                ModelState.AddModelError("IngresoNotasInicio", "Campo Requerido");
            if (string.IsNullOrEmpty(model.IngresoNotasFin.ToString()))
                ModelState.AddModelError("IngresoNotasFin", "Campo Requerido");
            if (string.IsNullOrEmpty(model.Nombre))
                ModelState.AddModelError("Nombre", "Campo Requerido");
            if (string.IsNullOrEmpty(model.Orden.ToString()))
                ModelState.AddModelError("Orden", "Campo Requerido");
            if (string.IsNullOrEmpty(model.PeriodoId.ToString()))
                ModelState.AddModelError("PeriodoId", "Campo Requerido");
            if (string.IsNullOrEmpty(model.AnioEscolarId.ToString()))
                ModelState.AddModelError("AnioEscolarId", "Campo Requerido");
        }

        public ActionResult Delete(int id)
        {
            var model = GetPeriodoCompleto(id);

            ValidarBorrar(model);
            if (!ModelState.IsValid)
            {
                TempData["error"] = ModelState.Values.FirstOrDefault().Errors.FirstOrDefault().ErrorMessage;
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [HttpPost]
        public ActionResult Delete(Periodo model)
        {
            _periodo.Remove(model.PeriodoId);
            return RedirectToAction("Index");
        }

        public void ValidarBorrar(Periodo model)
        {
            ModelState.Clear();
            if (_nota.Find(x => x.PeriodoId == model.PeriodoId).Any())
                ModelState.AddModelError("ContieneNotas", "Imposible eliminar, el periodo contiene notas.");
        }
    }
}