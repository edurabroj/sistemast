﻿using DB_ST.Domain;
using DB_ST.Domain.Enum;
using DB_ST.Service;
using DB_ST.Web.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static DB_ST.Web.Models.AsistenciaAlumnoVM;
using Microsoft.AspNet.Identity;
using DB_ST.Web.Models.Extensions;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Docente)]
    public class AsistenciaAlumnoController : Controller
    {
        private IMasterService<AsistenciaAlumno> _service;
        private IMasterService<Matricula> _matricula;
        private IMasterService<Seccion> _seccion;
        private IMasterService<Grado> _grado;
        private IMasterService<AnioEscolar> _anio;
        private IMasterService<Estudiante> _estudiante;
        private IMasterService<Empleado> _empleado;
        private IMasterService<AsistenciaAlumno> _asistencia;
        private ApplicationDbContext _context = new ApplicationDbContext();

        public AsistenciaAlumnoController
        (
            IMasterService<AsistenciaAlumno> service,
            IMasterService<Matricula> matricula,
            IMasterService<Seccion> seccion,
            IMasterService<Grado> grado,
            IMasterService<AnioEscolar> anio,
            IMasterService<Estudiante> estudiante,
            IMasterService<Empleado> empleado,
            IMasterService<AsistenciaAlumno> asistencia
        )
        {
            _service = service;
            _matricula = matricula;
            _seccion = seccion;
            _grado = grado;
            _anio = anio;            
            _estudiante = estudiante;
            _empleado = empleado;
            _asistencia = asistencia;
        }

        // GET: Asistencia
        public ActionResult Create()
        {
            var hoy = DateTime.Now;
            var anio = _anio.Find(x => x.Anio.Equals(hoy.Year.ToString())).FirstOrDefault();
            
            ViewBag.success = TempData["success"];
            ViewBag.error = TempData["error"];

            if (User.IsInRole("docente"))
            {
                //docente
                var docenteLogueado = _empleado.GetById(int.Parse(User.Identity.GetUserEmpleadoId()));
                if (docenteLogueado != null)
                    ViewBag.DocenteId = docenteLogueado.EmpleadoId;

                //docente sin secciones
                var seccionesDocente = _seccion.Find
                    (
                        x => x.AnioEscolar.Anio.Equals(anio.Anio)
                        && x.EmpleadoId == docenteLogueado.EmpleadoId,
                        x => x.Grado
                    );
                if (!seccionesDocente.Any())
                {
                    ViewBag.Error = 0;
                    return View("Error");
                }

                //destiempo
                if (hoy.TimeOfDay < anio.HoraEntradaClases || hoy.TimeOfDay > anio.HoraSalidaClases)
                {
                    ViewBag.Error = 1;
                    ViewBag.Inicio = anio.HoraEntradaClases;
                    ViewBag.Final = anio.HoraSalidaClases;
                    return View("Error");
                }

                ViewBag.Secciones = new SelectList(seccionesDocente, "SeccionId", "Full");
            }else
            {
                ViewBag.Secciones = new SelectList(new List<Seccion>(), "SeccionId", "Nombre");
            }

            AsistenciaAlumnoVM model = new AsistenciaAlumnoVM();

            model.Fecha = DateTime.Now;
            var grados = _grado.GetAll();

            ViewBag.Grados = new SelectList(grados, "GradoId", "Nombre");

            return View(model);
        }

        [HttpPost]
        public ActionResult Create(AsistenciaAlumnoVM asistencias)
        {            
            var hoy = asistencias.Fecha;
            var anio = _anio.Find(x => x.Anio.Equals(hoy.Year.ToString())).FirstOrDefault();
            if (hoy.TimeOfDay < anio.HoraEntradaClases || hoy.TimeOfDay > anio.HoraSalidaClases)
            {
                ModelState.AddModelError("Fecha","Fuera de plazo");
                ViewBag.Error = 1;
                ViewBag.Inicio = anio.HoraEntradaClases;
                ViewBag.Final = anio.HoraSalidaClases;
                return View("Error");
            }

            var grados = _grado.GetAll();
            ViewBag.Grados = new SelectList(grados, "GradoId", "Nombre");
            var secciones = _seccion.GetAll();
            ViewBag.Secciones = new SelectList(secciones, "SeccionId", "Nombre");

            try
            {
                //if (asistencias.Estudiantes.Count > 0)
                //{
                    //Metodo
                    Validar(asistencias);

                    //if (asistencias.Estudiantes.FindAll(x => x.ValorAsistencia == ValorAsistencia.NoRegistrado).Count() > 0)
                    //{
                    //    //ViewBag.Error = 2;
                    //    //TempData["error"] = "La asistencia no se ha guardado debido a que no ha sido seleccionado el valor de asistencia para todos los alumnos.";
                    //    //return View("Error");
                    //    //ViewBag.error = "No ha sido seleccionado el valor de asistencia para todos los alumnos.";
                    //}
                    //else

                    if (ModelState.IsValid)
                    {
                        for (int i = 0; i < asistencias.Estudiantes.Count; i++)
                        {
                            var asistencia = new AsistenciaAlumno()
                            {
                                ValorAsistencia = asistencias.Estudiantes.ToList()[i].ValorAsistencia,
                                Fecha = asistencias.Fecha,
                                //GradoId = asistencias.GradoId,
                                SeccionId = asistencias.SeccionId,
                                EstudianteId = asistencias.Estudiantes.ToList()[i].EstudianteId,
                            };
                            var registradoLista = _service.Find(x =>
                                    x.EstudianteId == asistencia.EstudianteId &&
                                    x.SeccionId == asistencia.SeccionId &&
                                    x.Fecha.Year == asistencia.Fecha.Year &&
                                    x.Fecha.Month == asistencia.Fecha.Month &&
                                    x.Fecha.Day == asistencia.Fecha.Day
                            );

                            if (registradoLista.Count() > 0)
                            {
                                var registrado = registradoLista.First();
                                try
                                {
                                    registrado.ValorAsistencia = asistencia.ValorAsistencia;
                                    registrado.Fecha = asistencia.Fecha;
                                    _service.Edit(registrado);
                                }
                                catch (DbUpdateConcurrencyException ex)
                                {
                                    // Update the values of the entity that failed to save from the store 
                                    ex.Entries.Single().Reload();
                                }
                            }
                            else
                            {
                                _service.Add(asistencia);
                            }
                        }
                        TempData["success"] = "Se ha guardado correctamente.";
                        //ViewBag.success = "Se ha guardado correctamente.";
                    }

                //}
            }
            catch (Exception e)
            {
                TempData["error"] = "La sección seleccionada no contiene alumno";
                //ViewBag.error = "La sección seleccionada no contiene alumno";
            }
            TempData["gradoId"] = asistencias.GradoId;
            TempData["seccionId"] = asistencias.SeccionId;
            return RedirectToAction("Create");

            //return View();
        }

        // GET: Asistencia/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public ActionResult ListaAsistencias()
        {
            var lista = _service.GetAll(p=>p.Seccion.Grado, p => p.Seccion, p=>p.Estudiante);
            return View(lista);
        }

        //// GET: Asistencia/Create
        //public ActionResult Create()
        //{
        //    ViewBag.Grados = _grado.GetAll();
        //    ViewBag.Secciones = _seccion.GetAll();
        //    return View();
        //}

        //// POST: Asistencia/Create
        //[HttpPost]
        //public ActionResult Create(AsistenciaAlumno asistencia)
        //{
        //    try
        //    {
        //        ViewBag.grados = _grado.GetAll();
        //        ViewBag.seccions = _seccion.GetAll();

        //        if (ModelState.IsValid)
        //        {
        //            _service.Add(asistencia);
        //        }
        //        return RedirectToAction("Index");
        //    }
        //    catch
        //    {
        //        return View();
        //    }
        //}

        // GET: Asistencia/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Asistencia/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Asistencia/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Asistencia/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult GetEstudiantes(int? seccionId, int? anioId)
        {
            if (seccionId == null)
                return null;

            AnioEscolar anio;
            if (anioId == null)
            {
                anio = _anio.Find(x=>x.Anio.Equals(DateTime.Now.Year.ToString())).First();
            }
            else
            {
                anio = _anio.GetById(anioId);
            }

            //var registradosIds = from a in estudiantes select a.EstudianteId;
            //var registrados = from a in _service.GetAll()
            //                  where registradosIds.Contains(a.EstudianteId)
            //                  select a;

            //IEnumerable<>
            //if (registrados.Count() > 0)
            //{

            //}
            //else
            //{

            //}

            var yaRegistrados = _service.Find(x => x.SeccionId == seccionId &&
                                            x.Fecha.Year == DateTime.Now.Year &&
                                            x.Fecha.Month == DateTime.Now.Month &&
                                            x.Fecha.Day == DateTime.Now.Day);
                                              //DateTime.Compare(x.Fecha.Date, DateTime.Now.Date)==0);

            //.Compare(x.price_date.Value.Date, dt.Date) == 0)
            //                                  x.Fecha.Date == DateTime.Now.Date);

            IEnumerable<EstudianteVM> estudiantesVM;

            if (yaRegistrados.Any())
            {
                estudiantesVM = from a in yaRegistrados
                                select new EstudianteVM()
                                {
                                    EstudianteId = a.EstudianteId,
                                    Estudiante = _estudiante.GetById(a.EstudianteId).Apellidos +
                                                ", " +
                                                _estudiante.GetById(a.EstudianteId).Nombres,
                                    ValorAsistencia = a.ValorAsistencia
                                };
            }
            else
            {
                var matriculados = from a in _matricula.GetAll()
                                   where a.SeccionId.Equals(seccionId) &&
                                         a.AnioEscolarId == anio.AnioEscolarId
                                   select a.EstudianteId;

                var estudiantes = from a in _estudiante.GetAll()
                                  where matriculados.Contains(a.EstudianteId)
                                  select a;

                estudiantesVM = from a in estudiantes
                                 select new EstudianteVM()
                                 {
                                     EstudianteId = a.EstudianteId,
                                     Estudiante = a.Apellidos + ", " + a.Nombres,
                                     ValorAsistencia = ValorAsistencia.NoRegistrado
                                 };
            }
            var asistenciaModel = new AsistenciaAlumnoVM();

            asistenciaModel.Estudiantes = estudiantesVM.ToList();

            return PartialView("_GetEstudiantes",asistenciaModel);
        }

        public JsonResult GetSeccionesByGrado(int? id)
        {
            //return Json(_ProvinciaInterface.QueryPorRegionId(id), JsonRequestBehavior.AllowGet);
            List<Seccion> lista;
            if (id != null)
                lista = _seccion.Find(x => x.GradoId == id &&
                    x.AnioEscolar.Anio.Equals(DateTime.Now.Year.ToString())).ToList();
            else
                lista = new List<Seccion>();

            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        //Validaciones///////////////////////////////////////////////////////////////
        //private void Validar(AsistenciaAlumno asistenciaAlumno)
        //{
        //    ModelState.Clear();
        //    if (asistenciaAlumno.AsistenciaAlumnoId < 1)
        //        ModelState.AddModelError("AsistenciaAlumnoId", "Valor no válido");
        //    if (asistenciaAlumno.ValorAsistencia == ValorAsistencia.NoRegistrado)
        //        ModelState.AddModelError("ValorAsistencia", "Alumno no registrado");

        //}
      
        public JsonResult ReporteAsistencia(int estudianteId, int anioEscolarId)
        {
            var anio = _anio.GetById(anioEscolarId);
            
            var asistencias = _asistencia.Find(x => x.EstudianteId == estudianteId && x.Fecha.Year.ToString() == anio.Anio);

            return Json(asistencias,JsonRequestBehavior.AllowGet);

        }

       


        //Validaciones////////////////////////////////////////////////////////////////////////////////////////////
        private void Validar(AsistenciaAlumnoVM asistenciaAlumnoVM)
        {
            ModelState.Clear();

            if (asistenciaAlumnoVM.Estudiantes.FindAll(x => x.ValorAsistencia == ValorAsistencia.NoRegistrado).Any())
            {
                ModelState.AddModelError("ValorAsistencia", "Alumno no registrado");
                ViewBag.Error = 2;
                TempData["error"] = "La asistencia no se ha guardado debido a que no ha sido seleccionado el valor de asistencia para todos los alumnos.";
            }

            if (asistenciaAlumnoVM.Estudiantes.Count <= 0)
            {
                ModelState.AddModelError("Estudiantes", "Asistencia Vacia");
                TempData["error"] = "La sección no contiene alumno";
            }
               
        }


    }
}