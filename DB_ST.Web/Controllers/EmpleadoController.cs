﻿using DB_ST.Domain;
using DB_ST.Service;
using DB_ST.Web.Models;
using DB_ST.Web.Validators;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using PagedList;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DB_ST.Web.Controllers
{
    [Authorize(Roles = Rols.Administrador)]
    public class EmpleadoController : Controller
    {
        private ApplicationDbContext _context = new ApplicationDbContext();
        IMasterService<Empleado> _service;
        IMasterService<TipoEmpleado> _serviceTipoEmpleados;
        IMasterService<Departamento> _serviceDepartamentos;

        public EmpleadoController(IMasterService<Empleado> service,
                                  IMasterService<TipoEmpleado> serviceTipoEmpleados,
                                  IMasterService<Departamento> serviceDepartamentos)
        {
            _service = service;
            _serviceTipoEmpleados = serviceTipoEmpleados;
            _serviceDepartamentos = serviceDepartamentos;
            //InicilizarListas();
        }

        //public void InicilizarListas()
        //{
        //    var genero = new List<dynamic>()
        //    {
        //        new { Text = "Masculino"},
        //        new { Text = "Femenino" }
        //    };
        //    HttpContext.Session["Genero"] = new SelectList(genero, "Text", "Text");

        //    var tipoEmpleados = _serviceTipoEmpleados.GetAll();
        //    HttpContext.Session["TipoEmpleados"] = new SelectList(tipoEmpleados, "TipoEmpleadoId", "NombreTipoEmpleado");
        //}

        // GET: Empleado
        public ActionResult Index(string SearchCriteria, int? page)
        {
            //InicilizarListas();

            ViewBag.Titulo = "Empleados";
            ViewBag.MsjBusqueda = "Ingrese DNI";
            if (string.IsNullOrEmpty(SearchCriteria))
                SearchCriteria = "";

            var lista = _service.Find(
                        m => m.DNI.Contains(SearchCriteria) && m.IsHabilitado==true,
                        m=>m.TipoEmpleado
                    );

            ViewBag.SearchCriteria = SearchCriteria;

            var pageIndex = page ?? 1;
            lista = lista.OrderBy(p => p.Apellidos).ToPagedList(pageIndex, Configuraciones.defaultPageSize);

            return Request.IsAjaxRequest() ? (ActionResult)PartialView("_Index", lista) : View(lista);
        }

        public ActionResult Create()
        {
            ViewBag.Titulo = "Registrar";

            llenarCombos();

            return View();
        }
        [HttpPost]
        public ActionResult Create(Empleado model)
        {
            Validar(model);
            if (ModelState.IsValid)
            {
                model.IsHabilitado = true;
                _service.Add(model);

                try
                {

                    var userStore = new UserStore<ApplicationUser>(_context);
                    var userManager = new UserManager<ApplicationUser>(userStore);

                    //var userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();//primero se migra la base de datos y se copias esta parte de codigo
                    var user = new ApplicationUser
                    {
                        UserName = model.Email,
                        Email = model.Email,
                        EmpleadoId = model.EmpleadoId
                    };
                    var result = userManager.Create(user, "St2017*");
                    var tipoEmp = _serviceTipoEmpleados.GetById(model.TipoEmpleadoId).NombreTipoEmpleado.ToLower();
                    _context.Roles.AddOrUpdate(r => r.Name, new IdentityRole() { Name = tipoEmp });
                    _context.SaveChanges();
                    userManager.AddToRole(user.Id, tipoEmp);

                }
                catch (Exception ex)
                {
                    System.Diagnostics.Debug.WriteLine("ERROR: "+ex.Message);
                }
                return RedirectToAction("Index");
            }
            llenarCombos();
            return View(model);
        }

        public void llenarCombos()
        {
            var tipoEmpleados = _serviceTipoEmpleados.GetAll();
            ViewBag.TipoEmpleados = new SelectList(tipoEmpleados, "TipoEmpleadoId", "NombreTipoEmpleado");
            var deps = _serviceDepartamentos.GetAll();
            ViewBag.Deps = new SelectList(deps, "DepartamentoId", "Nombre");
            ViewBag.Pros = new SelectList(new List<Provincia>(), "ProvinciaId", "Nombre");
            ViewBag.Diss = new SelectList(new List<Distrito>(), "DistritoId", "Nombre");
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Titulo = "Editar";

            llenarCombos();

            var model = _service.GetById(id);
            return View(model);
            //return PartialView("_Edit", model);
        }

        [HttpPost]
        public ActionResult Edit(Empleado model)
        {
            Validar(model);
            if (ModelState.IsValid)
            {
                _service.Edit(model);
                return RedirectToAction("Index");
            }
            llenarCombos();
            return View(model);
        }

        public ActionResult Details(int id)
        {
            ViewBag.Titulo = "Detalles";
            //var model = _service.GetById(id);
            var model = _service.Find(p => p.EmpleadoId == id, p => p.TipoEmpleado, x=>x.Distrito).First();
            //return View(model);
            return View("_Details", model);
        }

        public ActionResult Disable(int? id)
        {
            ViewBag.Titulo = "Deshabilitar/Habilitar";
            var model = _service.Find(p => p.EmpleadoId == id, p => p.TipoEmpleado, x => x.Distrito).First();
            //return View(model);
            return View("_Disable", model);
        }

        [HttpPost]
        public ActionResult Disable(int id)
        {
            Empleado model = _service.GetById(id);
            if (ModelState.IsValid)
            {
                if (model.IsHabilitado == true)
                    model.IsHabilitado = false;
                else
                    model.IsHabilitado = true;
                _service.Edit(model);
                return RedirectToAction("Index");
            }
            return View();
        }
        //public ActionResult Delete(int? id)
        //{
        //    ViewBag.Titulo = "Deshabilitar/Habilitar Empleado";
        //    var model = _service.GetById(id);
        //    return View(model);
        //}
        [HttpPost]
        public ActionResult Delete(Empleado model)
        {
            if (ModelState.IsValid)
            {
                //model.Habilitado = false;
                _service.Remove(model.EmpleadoId);
                return RedirectToAction("Index");
            }
            return View();
        }

        //Validaciones///////////////////////////////////////////////////////////////
        private void Validar(Empleado empleado)
        {
            ModelState.Clear();
            if (_service.Find(x => x.DNI == empleado.DNI && x.EmpleadoId!=empleado.EmpleadoId).Any())
                ModelState.AddModelError("DNI", "Campo repetido");
            if (string.IsNullOrEmpty(empleado.Nombres))
                ModelState.AddModelError("Nombre", "Campo requerido");
            if (string.IsNullOrEmpty(empleado.Apellidos))
                ModelState.AddModelError("Apellido", "Campo requerido");
            if (!string.IsNullOrEmpty(empleado.DNI) && !Validator.IsDNI(empleado.DNI))
                ModelState.AddModelError("DNI", "Inválido.");
            if (String.IsNullOrEmpty(empleado.FechaNacimiento.ToString()))
                ModelState.AddModelError("Naciemiento", "Campo requerido");
            if (string.IsNullOrEmpty(empleado.Direccion))
                ModelState.AddModelError("Direccion", "Campo requerido");
        }
    }
}