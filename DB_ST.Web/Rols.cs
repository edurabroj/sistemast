﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DB_ST.Web
{
    public static class Rols
    {
        public const string Docente = "docente";
        public const string Director = "director";
        public const string Administrador = "administrador";
        public const string Secretario = "secretario";
        public const string Apoderado = "apoderado";
    }
}