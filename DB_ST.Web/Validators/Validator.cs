﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace DB_ST.Web.Validators
{
    public static class Validator
    {
        public static bool IsDNI(string text)
        {
            Regex DNIvalidator = new Regex(@"^\d{8}$");
            return DNIvalidator.IsMatch(text);
        }
    }
}