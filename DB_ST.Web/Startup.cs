﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DB_ST.Web.Startup))]
namespace DB_ST.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
