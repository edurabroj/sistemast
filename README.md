# Sistema de Gestión Y Control Educativo #

## Integrantes ##
* Rabanal Rojas Eduardo
* Marrufo Briceño Yoel
* Zegarra Zavala Javier



#Características de Calidad#


##Funcionalidad##
###Adecuación###
* (E) ¿El Software tiene la capacidad de cumplir las funciones principales requeridas por el usuario?
* (E) ¿El sistema puede realizar con normalidad una Matricula siendo esta un CORE de la institucion?
* (E) ¿Cual es la capacidad del sistema para proveer resultados correctos con el nivel de precisión requerido?
###Exactitud###
* (E) ¿A que consideramos dentro del Software una respuesta precisa?(El resultado se asemeja al valor real)
* (E) ¿Las interfaces muestran información detallada de lo que necesita el usuario?
* (E) ¿Como el usuario puede detectar una respuesta errónea?
###Interoperabilidad###
* No se considera
###Seguridad###
* (E) ¿Los datos pueden ser accedidos por usuarios no autorizados?
* (E) ¿Que bloqueos tiene el sistema frente a un usuario desconocido?
* (E) ¿El sistema permite ingresar a las interfaces mediante el cambio de la url?
* (E) ¿De que tipos de ataques puede protegerse el sistema por si solo? (ddos, sql injection, fuerza bruta, etc)
* (I) ¿El sistema puede detectar a un usuario no registrado/malicioso?


##Fiabilidad##
###Madurez###
* (I) ¿El Software es capaz de manejar los errores que se puedan presentar?
* (I)¿Que tipo de errores puede presentar el sistema?
* (E) ¿El Software muestra al usuario los errores que se puedan presentar?
* (I) ¿De que formas el sistema puede manejar los errores?(Se usan excepciones)
###Tolerancia a Fallos###
* (E) ¿El Software tiene la capacidad de mantener su rendimiento en la presencia de un fallo humano?(Ingreso de datos erroneos)
* (E) ¿Que puede afectar al sistema en un corte de luz?(Perdida de datos no guardados)
* (E) ¿Cuánto tarda el sistema en reponerse tras un fallo?
* (E) ¿Cuantos datos se pierden tras un fallo?
* (E) ¿Que tipo de fallas se pueden presentar en el sistema? Fallas presentadas
* (E) ¿De que forma se pueden corregir las fallas presentadas? Fallas corregidas
###Recuperabilidad###
* (I) ¿El Software es capaz de recuperar datos al producirse un error?
* (E) ¿El Usuario es capaz de recuperar datos al producirse un error?
* (E) ¿El sistema es capaz de ofrecer un backup al usuario?
* (E) ¿Cuantos datos se perdieron tras restauración de backup?
* (E) ¿Que tiempo le toma al sistema realizar un backup?

##Usabilidad##
###Comprensibilidad###
* (E) ¿El usuario comprende la interfaz intuitivamente?
* (E) ¿El usuario necesita ayuda para usar el sistema?
* (E) ¿El sistema cuenta con ayudas o manuales para su fácil uso?
* (E) ¿El sistema puede facilitar otro idioma para que sea usado con facilidad por otros usuarios?
###Facilidad De Aprendizaje###
* (E) ¿Se necesita de una capacitación para usar el sistema?
* (E) ¿El Software Permite un fácil aprendizaje al usuario?
* (E) ¿Que tiempo le toma al usuario aprender a usar el sistema en su totalidad? Tiempo
* (E) ¿El usuario tiene algún respaldo para realizar preguntas sobre el funcionamiento del sistema?
###Operabilidad###
* (E) ¿Como podemos medir la facilidad de uso del sistema?
* (E) ¿Las características que brinda el sistema son fáciles de operar?
* (E) ¿El sistema opera de forma lineal o simultanea?
* (E) ¿El sistema ofrece atajos al usuario?
###Atracción###
* (E) ¿Cumple con los estándares de interacción humano computador?
* (E) ¿El diseño visual es atractivo para el usuario?
* (E) ¿Los colores del software son congruentes con los de la I.E.?
* (E) ¿El usuario puede diferenciar el nivel de autoridad que tiene dentro del sistema por el color del mismo?
* (E) ¿Los colores del sistema pueden ser cambiados para dar comodidad al usuario?(Modo lectura)

##Eficiencia##
###Comportamiento Temporal###
* (I) ¿El software responde y procesa las solicitudes con rapidez?
* (I) ¿Cual es el tiempo de demora del sistema para dar una respuesta al usuario?
* (E) ¿El sistema cumple con los limites de tiempo para dar una respuesta?
* (I) ¿El sistema cuenta con almacenamiento en cache para agilizar la carga de las interfaces?
###Utilización de recursos###
* (I) ¿El software aprovecha eficazmente los recursos establecidos?
* (E) ¿Cuales son los requisitos para que el sistema funcione optimamente?
* (E) ¿El sistema funciona normalmente con requisitos mínimos a los que necesita?
* (E) ¿El sistema es capaz de operar sin acceso a internet?

##Mantenibilidad##
###Analizabilidad###
* (I) ¿El sistema puede diagnosticar sus fallos y deficiencias para ser modificadas?
* (E) ¿Que tipos de fallos puede detectar el sistema?
* (I) ¿Es fácil de ubicar el código donde se encuentra el fallo?
* (I) ¿Cualquier programador puede modificar el código para arreglar un fallo?
###Cambiabilidad###
* (I) ¿El sistema soporta cambios en su código y/o diseño?
* (I) ¿El sistema puede ser migrado a otro lenguaje de programación?
* (E) ¿Las interfaces soportan cualquier navegador de internet?
###Estabilidad###
* (I) ¿El sistema puede evitar efectos inesperados producidos por los cambios?
* (E) ¿El sistema ubica respectivamente la información en una interfaz modificada?
* (E) ¿El sistema se muestra adecuadamente en un navegador de tecnología antigua "I-Explorer"?
* (E) ¿Es sistema necesita un Refresh de pagina en caso de inactividad?
###Facilidad de prueba###
* (I) ¿El Software tiene la capacidad de validar las partes modificadas?
* (I) ¿El sistema tiene control de versiones de las interfaces?
* (E) ¿El sistema cuenta con algún complemento para realizarse pruebas?
* (I) ¿Se pueden agregar datos de prueba a la BD fácilmente?(Es necesario tener otros datos para llenar otros)


##Portabilidad##
###Adaptabilidad###
* (E) ¿El Software puede ser migrado hacia otra I.E., sin realizar cambios?
* (E) ¿El Software soporta uso móvil?
* (E) ¿El software soporta su uso en Chrome, I-Expolorer, Opera, Moxila?
* (I) ¿El Software puede trabajar en otros sistemas operativos?

###Facilidad de Instalación###
* (E) ¿El Software puede ser instalado con facilidad?
* (E) ¿El sistema cuanta con un ayudante de instalación?
* (E) ¿Que tiempo le toma al usuario instalar el sistema?
* (E) ¿Que tiempo le tomo ala sistema cargar por primera vez?
###Coexistencia###
* (I) ¿El sistema puede compartir recursos con algún otro sistema?
* (E) ¿Que información comparte el sistema hacia otros sistemas?
* (E) ¿El sistema acepta-adapta información brindada por otros sistemas a sus registros?
###Reemplazabilidad###
* (E) ¿El software es fácilmente reemplazable por otro?
* (E) ¿En que otras instituciones se puede usar el sistema?
* (E) ¿El sistema tiene mejores características y efectividad para que el cliente prefiera usarlo?