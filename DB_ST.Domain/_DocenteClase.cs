﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class _DocenteClase
    {
        public Int32 DocenteClaseId { get; set; }
        public Empleado Docente { get; set; }
        public Int32 DocenteId { get; set; }
        public Clase Clase { get; set; }
        public Int32 ClaseId { get; set; }
        public AnioEscolar AnioEscolar { get; set; }
        public Int32 AnioEscolarId { get; set; }
    }
}
