﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class TipoEmpleado
    {
        public Int32 TipoEmpleadoId { get; set; }
        public String NombreTipoEmpleado { get; set; }
        public DateTime? UlltimaModificacion { get; set; }
    }
}
