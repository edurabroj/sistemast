﻿using DB_ST.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class AsistenciaAlumno_Bad
    {
        public int AsistenciaAlumnoId { get; set; }
        public DateTime Fecha { get; set; }
        public Estudiante Estudiante { get; set; }
        public Int32 EstudianteId { get; set; }
        public ValorAsistencia ValorAsistencia { get; set; }
    }
}
