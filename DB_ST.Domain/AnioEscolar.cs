﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class AnioEscolar
    {
        public Int32 AnioEscolarId { get; set; }
        [DisplayName("Año")]
        public String Anio { get; set; }
        [DisplayName("Nombre del Año")]
        public String NombreAnio { get; set; }

        //[DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}")]
        [DisplayName("Fecha de inicio de Clases")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaInicio { get; set; }
        [DisplayName("Fecha de fin de Clases")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaFin { get; set; }
        [DisplayName("Fecha de inicio de Matrículas")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaInicioMatricula { get; set; }
        [DisplayName("Fecha de fin de Matrículas")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaFinMatricula { get; set; }
        [DisplayName("Fecha de Matrícula Extemporánea")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaMatriculaContemporanea { get; set; }

        [DisplayName("Hora Entrada Clases")]
        public TimeSpan HoraEntradaClases { get; set; }
        [DisplayName("Hora Salida Clases")]
        public TimeSpan HoraSalidaClases { get; set; }

        [DisplayName("Monto de Matrícula Regular (S/.)")]
        public double MontoMatriculaRegular { get; set; }
        [DisplayName("Monto de Matrícula Extemporanea (S/.)")]
        public double MontoMatriculaExtemporanea { get; set; }

    }
}
