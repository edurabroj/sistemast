﻿using DB_ST.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Sesion
    {
        public Int32 SesionId { get; set; }
        public Dia Dia { get; set; }


        //[DataType(DataType.Time)]
        //[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:HH:mm}")]
        public Horario Horario { get; set; }
        public int HorarioId { get; set; }

        public Clase Clase { get; set; }
        public Int32 ClaseId { get; set; }

        //public int SeccionId { get; set; }
        //public Seccion Seccion { get; set; }


    }
}
