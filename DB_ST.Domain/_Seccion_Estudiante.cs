﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class SeccionEstudiante
    {
        public Int32 SeccionEstudianteId { get; set; }


        public Seccion Seccion { get; set; }
        public Int32 SeccionId { get; set; }
        public Estudiante Estudiante { get; set; }
        public Int32 EstudianteId { get; set; }
    }
}
