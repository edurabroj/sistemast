﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class AsignaturaGrado
    {
        public Int32 AsignaturaGradoId { get; set; }

        public Asignatura Asignatura { get; set; }
        public Int32 AsignaturaId { get; set; }
        public Grado Grado { get; set; }
        public Int32 GradoId { get; set; }
    }
}
