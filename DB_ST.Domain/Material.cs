﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Material
    {
        public int MaterialId { get; set; }
        [Required]
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        [Required]
        public int Disponibles { get; set; }
        public int? Usados { get; set; }

        public int SeccionId { get; set; }
        public virtual Seccion Seccion { get; set; }
    }
}
