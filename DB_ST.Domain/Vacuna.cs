﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Vacuna
    {
        public Int32 VacunaId { get; set; }

        public DateTime UlltimaModificacion { get; set; }
        public String TipoVacuna { get; set; }
        public String Descripcion { get; set; }
    }
}
