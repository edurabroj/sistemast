﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class EstudiantesRetirados
    {
        public Int32 EstudiantesRetiradosId { get; set; }
        public DateTime FechaRetiro { get; set; }
        public String Motivo { get; set; }

        public Estudiante Estudiante { get; set; }
        public Int32 EstudianteId { get; set; }
    }
}
