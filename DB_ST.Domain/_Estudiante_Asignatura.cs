﻿using System;

namespace DB_ST.Domain
{
    public class EstudianteAsignatura
    {
        public Int32 EstudianteAsignaturaId { get; set; }

        public Estudiante Estudiante { get; set; }
        public Int32 EstudianteId { get; set; }
        public Asignatura Asignatura { get; set; }
        public Int32 AsignaturaId { get; set; }
        public AnioEscolar AnioEscolar { get; set; }
        public Int32 AnioEscolarId { get; set; }
    }
}