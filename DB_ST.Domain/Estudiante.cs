﻿using DB_ST.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Estudiante
    {
        public Int32 EstudianteId { get; set; }

        [Required(ErrorMessage ="El campo {0} es requerido")]
        public String Nombres { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        public String Apellidos { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        public String DNI { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Nacimiento")]
        //[DataType(DataType.Date)]
        [DisplayFormat(DataFormatString ="{0:dd/MM/yyyy}",
            ApplyFormatInEditMode =true)]

        //[DisplayFormat(DataFormatString = "{0:hh:mm}",
        //    ApplyFormatInEditMode = true)]
        public DateTime FechaNacimiento { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        public Genero Sexo { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Tipo de Sangre")]
        public TipoSangre RH { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido")]
        [Display(Name = "Dirección")]
        public String Direccion { get; set; }

        //[Required(ErrorMessage = "El campo {0} es requerido")]
        //public Departamento Departamento { get; set; }
        //public int DepartamentoId { get; set; }

        //[Required(ErrorMessage = "El campo {0} es requerido")]
        //public Provincia Provincia { get; set; }
        //public int ProvinciaId { get; set; }

        //[Required(ErrorMessage = "El campo {0} es requerido")]
        public Distrito Distrito { get; set; }
        public int DistritoId { get; set; }

        [Display(Name = "Habilitado")]
        public bool Habilitado { get; set; }

        public EstadoEstudiante EstadoEstudiante { get; set; }

        [Display(Name = "Nombre Completo")]
        public virtual string FullName
        {
            get
            {
                return Nombres + " " + Apellidos;
            }
        }

        public virtual string DniFullName
        {
            get
            {
                return "(" +DNI + ") " + FullName;
            }
        }

        public virtual string Procedencia
        {
            get
            {
               return Distrito.Provincia.DepartamentoId + "" + Distrito.ProvinciaId + "" + DistritoId;
            }
        } 
    }
}
