﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class GradoSeccion
    {
        public Int32 GradoSeccionId { get; set; }

        public virtual Grado Grado { get; set; }
        public Int32 GradoId { get; set; }
        public virtual Seccion Seccion { get; set; }
        public Int32 SeccionId { get; set; }

    }
}
