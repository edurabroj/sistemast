﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Apoderado
    {
        public Int32 ApoderadoId { get; set; }

        public String Nombres { get; set; }
        public String Apellidos { get; set; }
        public String DNI { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public String Profesion { get; set; }
        public String Email { get; set; }
        public String Celular { get; set; }

        [Display(Name = "Nombre Completo")]
        public virtual string FullName
        {
            get
            {
                return Nombres + " " + Apellidos;
            }
        }

        public virtual string DniFullName
        {
            get
            {
                return "(" + DNI + ") " + FullName;
            }
        }
    }
}
