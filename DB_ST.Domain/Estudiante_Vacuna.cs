﻿using System;

namespace DB_ST.Domain
{
    public class EstudianteVacuna
    {
        public Int32 EstudianteVacunaId { get; set; }
        public Estudiante Estudiante { get; set; }
        public Int32 EstudianteId { get; set; }
        public Vacuna Vacuna { get; set; }
        public Int32 VacunaId { get; set; }
    }
}