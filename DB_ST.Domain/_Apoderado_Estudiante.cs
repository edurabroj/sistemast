﻿using System;

namespace DB_ST.Domain
{
    public class ApoderadoEstudiante
    {
        public Int32 ApoderadoEstudianteId { get; set; }
        public String Parentesco { get; set; }

        public Apoderado Apoderado { get; set; }
        public Int32 ApoderadoId { get; set; }

        public Estudiante Estudiante { get; set; }
        public Int32 EstudianteId { get; set; }
    }
}