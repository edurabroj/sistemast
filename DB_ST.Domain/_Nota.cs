﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class _Nota
    {
        public Int32 _NotaId { get; set; }
        public String Cualitativa { get; set; }
        public Int32 Vigecimal { get; set; }
        public Int32 ValorNota1 { get; set; }
        public Int32 ValorNota2 { get; set; }
        public Int32 ValorNota3 { get; set; }
        public Int32 ValorNota4 { get; set; }
        public Int32 Promedio { get; set; }

        public Estudiante Estudiante { get; set; }
        public Int32 EstudianteId { get; set; }

        public Asignatura Asignatura { get; set; }
        public Int32 AsignaturaId { get; set; }
        
        public int GradoId { get; set; }
        public Grado Grado { get; set; }

        public int SeccionId { get; set; }
        public Seccion Seccion { get; set; }
    }
}
