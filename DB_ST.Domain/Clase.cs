﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Clase
    {
        public int ClaseId { get; set; }
        [Required]
        public string Nombre { get; set; }
        //[Required]
        //public int Capacidad { get; set; }
        [Required]
        [Display(Name = "Habilitado")]
        public bool IsHabilitado { get; set; }

        [Required]
        [Display(Name = "Asignatura")]
        public int AsignaturaId { get; set; }
        public virtual Asignatura Asignatura { get; set; }

        [Required]
        [Display(Name = "Docente")]
        public Int32 EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }

        [Required]
        [Display(Name = "Sección")]
        public Int32 SeccionId { get; set; }
        public Seccion Seccion { get; set; }

        public virtual string FullAsignaturaClase
        {
            get
            {
                return Asignatura.Nombre + " (" + Nombre+")";
            }
        }
    }
}
