﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Asignatura
    {
        public Int32 AsignaturaId { get; set; }
        public String Nombre { get; set; }
        public DateTime UlltimaModificacion { get; set; }

        //public virtual Grado Grado { get; set; }
        //public int GradoId { get; set; }
    }
}
