﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Grado
    {
        [Key]
        public Int32 GradoId { get; set; }
        [Required]
        public String Nombre { get; set; }
        [Required]
        public Int32 Orden { get; set; }

        //public Boolean? Promovido { get; set; }
        public DateTime? UlltimaModificacion { get; set; }
    }
}
