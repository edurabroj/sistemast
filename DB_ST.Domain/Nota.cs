﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Nota
    {
        public Int32 NotaId { get; set; }
        public Int32 Vigesimal { get; set; }
        public String Cualitativa { get; set; }
        public EstudianteClase EstudianteClase { get; set; }
        public Int32 EstudianteClaseId { get; set; }

        public Periodo Periodo { get; set; }
        public Int32 PeriodoId { get; set; }
        //public Estudiante Estudiante { get; set; }
        //public Int32 EstudianteId { get; set; }

        //public Asignatura Asignatura { get; set; }
        //public Int32 AsignaturaId { get; set; }

        //public int SeccionId { get; set; }
        //public Seccion Seccion { get; set; }
    }
}
