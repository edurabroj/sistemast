﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain.Enum
{
    public enum TipoSangre
    {
        [Description("O-")] O_Negativo,
        [Description("O+")] O_Positivo,
        [Description("A-")] A_Negativo,
        [Description("A+")] A_Positivo,
        [Description("B-")] B_Negativo,
        [Description("B+")] B_Positivo,
        [Description("AB-")] AB_Negativo,
        [Description("AB+")] AB_Positivo
    }
}
