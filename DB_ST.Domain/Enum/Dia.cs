﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain.Enum
{
    public enum Dia
    {
        L,
        M,
        X,
        J,
        V,
        S,
        D
    }
}
