﻿using System;

namespace DB_ST.Domain
{
    public class AsignaturaDocente
    {
        public Int32 AsignaturaDocenteId { get; set; }


        public Empleado Empleado { get; set; }
        public Int32 EmpleadoId { get; set; }
        public Asignatura Asignatura { get; set; }
        public Int32 AsignaturaId { get; set; }
        public AnioEscolar AnioEscolar { get; set; }
        public Int32 AnioEscolarId { get; set; }
    }
}