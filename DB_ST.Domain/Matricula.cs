﻿using DB_ST.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Matricula
    {
        public Int32 MatriculaId { get; set; }

        [Required]
        [Display(Name = "Numero de Voucher")]
        public String NumeroVoucher { get; set; }

        [Display(Name = "Fecha de Matrícula")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime FechaMatricula { get; set; }

        [Display(Name = "Tipo de Matrícula")]
        //public String TipoDeMatricula { get; set; }
        public TipoMatricula TipoMatricula { get; set; }

        [Display(Name = "Monto")]
        public double MontoMatricula { get; set; }

        public Grado Grado { get; set; }

        [Display(Name = "Grado")]
        public Int32 GradoId { get; set; }

        [Display(Name = "Sección")]
        public Seccion Seccion{ get; set; }

        [Display(Name = "Sección")]
        public Int32 SeccionId { get; set; }

        public Estudiante Estudiante { get; set; }

        [Display(Name = "Estudiante")]
        public Int32 EstudianteId { get; set; }

        [Display(Name = "Año Escolar")]
        public AnioEscolar AnioEscolar { get; set; }

        [Display(Name = "Año Escolar")]
        public Int32 AnioEscolarId { get; set; }

        public Apoderado Apoderado { get; set; }

        [Display(Name = "Apoderado")]
        public int ApoderadoId { get; set; }

        [Display(Name = "Estado")]
        public EstadoEstudiante EstadoEstudiante { get; set; }

        [Display(Name = "I.E. Origen (Cod)")]
        public string CodigoInsitucionOrigen { get; set; }

        [Display(Name = "Razón")]
        public string RazonTraslado { get; set; }

        //public int EstudianteEstadoAnioId { get; set; }
        //public EstudianteEstadoAnio EstudianteEstadoAnio { get; set; }
    }
}
