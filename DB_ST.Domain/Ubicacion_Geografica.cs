﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DB_ST.Domain
{
    public class Departamento
    {
        [System.ComponentModel.DataAnnotations.KeyAttribute()]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DepartamentoId { get; set; }
        public string Nombre { get; set; }
    }
    public class Provincia
    {
        [System.ComponentModel.DataAnnotations.KeyAttribute()]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProvinciaId { get; set; }
        public string Nombre { get; set; }

        public Departamento Departamento { get; set; }
        public int DepartamentoId { get; set; }
    }
    public class Distrito
    {
        [System.ComponentModel.DataAnnotations.KeyAttribute()]
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int DistritoId { get; set; }
        public string Nombre { get; set; }

        public Provincia Provincia { get; set; }
        public int ProvinciaId { get; set; }
    }
}