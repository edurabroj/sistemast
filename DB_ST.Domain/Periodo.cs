﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Periodo
    {
        public int PeriodoId { get; set; }

        public string Nombre { get; set; }
        public int Orden { get; set; }

        [Display(Name = "Año Escolar")]
        public Int32 AnioEscolarId { get; set; }
        public AnioEscolar AnioEscolar { get; set; }

        [Display(Name = "Inicio Ingreso Notas")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime IngresoNotasInicio { get; set; }

        [Display(Name = "Fin Ingreso Notas")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime IngresoNotasFin { get; set; }
    }
}
