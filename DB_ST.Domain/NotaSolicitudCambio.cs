﻿using DB_ST.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class NotaSolicitudCambio
    {
        public int NotaSolicitudCambioId { get; set; }
        public int NotaId { get; set; }
        public Nota Nota { get; set; }
        public int NotaNueva { get; set; }
        public string Motivo { get; set; }
        public Empleado Destinatario { get; set; }
        public int DestinatarioId { get; set; }
        public EstadoSolicitud EstadoSolicitud { get; set; }
    }
}
