﻿using DB_ST.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class EstudianteEstadoAnio
    {
        public int EstudianteEstadoAnioId{ get; set; }

        public int EstudianteId { get; set; }
        public Estudiante Estudiante { get; set; }

        public int AnioEscolarId { get; set; }
        public AnioEscolar AnioEscolar { get; set; }

        public EstadoEstudiante EstadoEstudiante { get; set; }

        public string TrasladoCodigoIntitucion { get; set; }
        public string TrasladoConstancia { get; set; }
    }
}
