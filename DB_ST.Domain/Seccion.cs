﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Seccion
    {
        [Key]
        public Int32 SeccionId { get; set; }
        public String Nombre { get; set; }
        public Int32 Capacidad { get; set; }
        //public Int32 Matriculados { get; set; }
        //public Int32 VacantesDisp { get; set; }

        [DisplayName("Año Escolar")]
        public Int32 AnioEscolarId { get; set; }
        public AnioEscolar AnioEscolar { get; set; }
        [DisplayName("Grado")]
        public Int32 GradoId { get; set; }
        public virtual Grado Grado { get; set; }

        [DisplayName("Docente")]
        public Int32 EmpleadoId { get; set; }
        public Empleado Empleado { get; set; }

        //public virtual int Cupos
        //{
        //    get
        //    {
        //        return Capacidad - Matriculados;
        //    }
        //}

        public virtual string Full
        {
            get
            {
                return Nombre + " (" + Grado.Nombre + ")";
            }
        }
    }
}
