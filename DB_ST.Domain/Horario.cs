﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Horario
    {
        public int HorarioId { get; set; }
        public TimeSpan HoraInicio { get; set; }

        public TimeSpan HoraFin { get; set; }

        public virtual string Full
        {
            get
            {
                return HoraInicio.ToString() + "-" + HoraFin.ToString();
            }
        }
    }
}
