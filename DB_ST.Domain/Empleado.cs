﻿using DB_ST.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class Empleado
    {
        public Int32 EmpleadoId { get; set; }

        [Required]
        public String Nombres { get; set; }

        [Required]
        public String Apellidos { get; set; }

        [Required]
        [StringLength(8,MinimumLength =8)]
        public String DNI { get; set; }

        [Required]
        [Display(Name ="Nacimiento")]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}",
            ApplyFormatInEditMode = true)]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}",
        //    ApplyFormatInEditMode = true)]
        public DateTime FechaNacimiento { get; set; }

        [Required]
        public Genero Sexo { get; set; }

        [Required]
        public String Direccion { get; set; }

        ////[Required]
        //public String Departamento { get; set; }

        ////[Required]
        //public String Provincia { get; set; }

        ////[Required]
        //public String Distrito { get; set; }
        public Distrito Distrito { get; set; }
        public int DistritoId { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        public String Email { get; set; }

        [Required]
        public String Celular { get; set; }

        [Required]
        [Display(Name = "Profesión")]
        public String Profesion { get; set; }

        [Required]
        public Int32 Salario { get; set; }

        //[Required]
        [Display(Name = "Activo")]
        public bool IsHabilitado { get; set; }
        //public Int32 HorasTrabajo { get; set; }

        [Required]
        [Display(Name = "Tipo")]
        public Int32 TipoEmpleadoId { get; set; }

        public TipoEmpleado TipoEmpleado { get; set; }

        [Display(Name = "Nombre Completo")]
        public virtual string FullName {
            get
            {
                return Nombres + " " + Apellidos;
            }
        }
        //public virtual string Procedencia
        //{
        //    get
        //    {
        //        return Departamento + ", " + Provincia + ", " + Distrito;
        //    }
        //}
    }
}
