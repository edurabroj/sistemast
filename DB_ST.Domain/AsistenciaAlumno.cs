﻿using DB_ST.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class AsistenciaAlumno
    {
        [Key]
        public int AsistenciaAlumnoId { get; set; }

        [Required]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }

        [Required]
        public int SeccionId { get; set; }
        public Seccion Seccion { get; set; }

        [Required]
        public int EstudianteId { get; set; }
        public Estudiante Estudiante { get; set; }

        [Required]
        [DisplayName("Asistencia")]
        public ValorAsistencia ValorAsistencia { get; set; }
    }
}
