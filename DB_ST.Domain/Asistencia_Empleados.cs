﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class AsistenciaEmpleados
    {
        public Int32 AsistenciaEmpleadosId { get; set; }
        public DateTime Fecha { get; set; }
        public DateTime Entrada { get; set; }
        public DateTime Salida { get; set; }
        public Boolean Falta { get; set; }

        public Empleado Empleado { get; set; }
        public Int32 EmpleadoId { get; set; }
    }
}
