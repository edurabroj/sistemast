﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DB_ST.Domain
{
    public class EstudianteClase
    {
        public Int32 EstudianteClaseId { get; set; }

        public Estudiante Estudiante { get; set; }
        public Int32 EstudianteId { get; set; }
        public Clase Clase { get; set; }
        public Int32 ClaseId { get; set; }
    }
}
