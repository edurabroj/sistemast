﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using DB_ST.Domain;
using System.Data.Entity.Validation;

namespace DB_ST.Repository
{
    public class DB_STContext : DbContext
    {
        public DbSet<AnioEscolar> AnioEscolars { get; set; }
        public DbSet<Apoderado> Apoderados { get; set; }
        public DbSet<Asignatura> Asignaturas { get; set; }
        public DbSet<AsistenciaAlumno> AsistenciaAlumnos { get; set; }
        //public DbSet<AsistenciaEmpleados> AsistenciaEmpleadoses { get; set; }
        public DbSet<Clase> Clases { get; set; }
        public DbSet<Empleado> Empleados { get; set; }
        public DbSet<Estudiante> Estudiantes { get; set; }
        //public DbSet<EstudianteEstadoAnio> EstudianteEstadoAnios { get; set; }
        public DbSet<EstudianteVacuna> EstudianteVacunas { get; set; }
        //public DbSet<EstudiantesRetirados> EstudiantesRetiradoses { get; set; }
        public DbSet<Grado> Grados { get; set; }
        public DbSet<EstudianteClase> EstudianteClases { get; set; }
        public DbSet<Matricula> Matriculas { get; set; }
        //public DbSet<_Nota> _Notas { get; set; }
        public DbSet<Nota> Notas { get; set; }
        public DbSet<NotaSolicitudCambio> NotaSolicitudCambios { get; set; }
        public DbSet<Periodo> Periodos { get; set; }
        public DbSet<Seccion> Seccions { get; set; }
        public DbSet<Horario> Horarios { get; set; }
        public DbSet<Sesion> Sesions { get; set; }
        public DbSet<Material> Materiales { get; set; }
        public DbSet<TipoEmpleado> TipoEmpleados { get; set; }
        public DbSet<Vacuna> Vacunas { get; set; }
        //ubigeo
        public DbSet<Departamento> Departamentos { get; set; }
        public DbSet<Provincia> Provincias { get; set; }
        public DbSet<Distrito> Distritos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public override int SaveChanges()
        {
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException ex)
            {
                // Retrieve the error messages as a list of strings.
                var errorMessages = ex.EntityValidationErrors
                        .SelectMany(x => x.ValidationErrors)
                        .Select(x => x.ErrorMessage);

                // Join the list to a single string.
                var fullErrorMessage = string.Join("; ", errorMessages);

                // Combine the original exception message with the new one.
                var exceptionMessage = string.Concat(ex.Message, " The validation errors are: ", fullErrorMessage);

                // Throw a new DbEntityValidationException with the improved exception message.
                throw new DbEntityValidationException(exceptionMessage, ex.EntityValidationErrors);
            }
        }
    }
}
