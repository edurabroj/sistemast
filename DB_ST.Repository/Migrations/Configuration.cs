namespace DB_ST.Repository.Migrations
{
    using Domain;
    using Domain.Enum;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Validation;
    using System.Linq;
    using System.Text;

    internal sealed class Configuration : DbMigrationsConfiguration<DB_ST.Repository.DB_STContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }
        
        protected override void Seed(DB_ST.Repository.DB_STContext context)
        {
            context.Apoderados.AddOrUpdate(
              p => p.DNI,
              new Apoderado
              {
                  ApoderadoId = 1,
                  DNI = "12345678",
                  Nombres = "El",
                  Apellidos = "Herauld",
                  Profesion = "Vago",
                  Celular = "123",
                  Email = "elvereauld@mail.com",
                  FechaNacimiento = DateTime.Now
              },
              new Apoderado
              {
                  ApoderadoId = 2,
                  DNI = "87654321",
                  Nombres = "El",
                  Apellidos = "Jhonson",
                  Profesion = "Vago",
                  Celular = "321",
                  Email = "elruben@mail.com",
                  FechaNacimiento = DateTime.Now
              }
            );

            context.AnioEscolars.AddOrUpdate(
              p => p.Anio,
              new AnioEscolar
              {
                  AnioEscolarId = 1,
                  Anio = "2016",
                  FechaInicio = DateTime.Now,
                  FechaFin = DateTime.Now,
                  FechaInicioMatricula = DateTime.Parse("2016/01/25"),
                  FechaFinMatricula = DateTime.Parse("2016/12/24"),
                  FechaMatriculaContemporanea = DateTime.Parse("2016/12/30"),

                  HoraEntradaClases = new TimeSpan(8, 45, 0),
                  HoraSalidaClases = new TimeSpan(13, 0, 0),

                  NombreAnio = "El a�o m�s xvre",
                  MontoMatriculaRegular = 100,
                  MontoMatriculaExtemporanea = 150
              },
              new AnioEscolar
              {
                  AnioEscolarId = 2,
                  Anio = "2017",
                  FechaInicio = DateTime.Now,
                  FechaFin = DateTime.Now,
                  FechaInicioMatricula = DateTime.Parse("2017/01/25"),
                  FechaFinMatricula = DateTime.Parse("2017/12/24"),
                  FechaMatriculaContemporanea = DateTime.Parse("2016/12/30"),

                  HoraEntradaClases = new TimeSpan(8, 45, 0),
                  HoraSalidaClases = new TimeSpan(13, 0, 0),

                  NombreAnio = "El a�o m�s bac�n",
                  MontoMatriculaRegular = 100,
                  MontoMatriculaExtemporanea = 150
              }
            );

            context.Horarios.AddOrUpdate(
                p => new { p.HoraInicio, p.HoraFin },
                new Horario
                {
                    HoraInicio = new TimeSpan(8, 45, 0),
                    HoraFin = new TimeSpan(9, 30, 0)
                },
                new Horario
                {
                    HoraInicio = new TimeSpan(9, 30, 0),
                    HoraFin = new TimeSpan(10, 15, 0)
                },
                new Horario
                {
                    HoraInicio = new TimeSpan(11, 30, 0),
                    HoraFin = new TimeSpan(12, 15, 0)
                },
                new Horario
                {
                    HoraInicio = new TimeSpan(12, 15, 0),
                    HoraFin = new TimeSpan(13, 0, 0)
                }
            );

            context.Periodos.AddOrUpdate(
                p=>p.IngresoNotasInicio,
                new Periodo
                {
                    PeriodoId=1,
                    Nombre="P1",
                    Orden = 1,
                    AnioEscolarId =2,
                    IngresoNotasInicio= DateTime.Parse("2017/03/20"),
                    IngresoNotasFin=DateTime.Parse("2017/03/25"),
                },
                new Periodo
                {
                    PeriodoId = 2,
                    Nombre = "P2",
                    Orden = 2,
                    AnioEscolarId = 2,
                    IngresoNotasInicio = DateTime.Parse("2017/05/20"),
                    IngresoNotasFin = DateTime.Parse("2017/06/30"),
                }
            );

            context.Asignaturas.AddOrUpdate(
                p => p.Nombre,
                new Asignatura
                {
                    AsignaturaId = 1,
                    Nombre = "Matem�tica",
                    //GradoId = 1,
                    UlltimaModificacion = DateTime.Now
                },
                new Asignatura
                {
                    AsignaturaId = 2,
                    Nombre = "Comunicaci�n",
                    //GradoId = 1,
                    UlltimaModificacion = DateTime.Now
                },
                new Asignatura
                {
                    AsignaturaId = 3,
                    Nombre = "Religi�n",
                    //GradoId = 1,
                    UlltimaModificacion = DateTime.Now
                },
                new Asignatura
                {
                    AsignaturaId = 4,
                    Nombre = "Educacion Fisica",
                    //GradoId = 1,
                    UlltimaModificacion = DateTime.Now
                },
                new Asignatura
                {
                    AsignaturaId = 5,
                    Nombre = "Arte",
                    //GradoId = 1,
                    UlltimaModificacion = DateTime.Now
                },
                new Asignatura
                {
                    AsignaturaId = 6,
                    Nombre = "Ingl�s",
                    //GradoId = 1,
                    UlltimaModificacion = DateTime.Now
                }
            );

           context.TipoEmpleados.AddOrUpdate(
                p => p.NombreTipoEmpleado,
                new TipoEmpleado
                {
                    TipoEmpleadoId = 1,
                    NombreTipoEmpleado = "Docente"
                },
                new TipoEmpleado
                {
                    TipoEmpleadoId = 2,
                    NombreTipoEmpleado = "Director"
                },
                new TipoEmpleado
                {
                    TipoEmpleadoId = 3,
                    NombreTipoEmpleado = "Administrador"
                },
                new TipoEmpleado
                {
                    TipoEmpleadoId = 4,
                    NombreTipoEmpleado = "Secretario"
                }
            );


            context.Departamentos.AddOrUpdate(
              p => p.Nombre,
              new Departamento
              {
                  DepartamentoId = 1,
                  Nombre = "Cax",
              }
            );

            context.Provincias.AddOrUpdate(
              p => p.Nombre,
              new Provincia
              {
                  ProvinciaId = 1,
                  Nombre = "Cax",
                  DepartamentoId = 1
              }
            );

            context.Distritos.AddOrUpdate(
                p => p.Nombre,
                new Distrito
                {
                    DistritoId = 1,
                    Nombre = "Cax",
                    ProvinciaId = 1
                }
            );

            context.Empleados.AddOrUpdate(
                p => p.DNI,
                new Empleado
                {
                    EmpleadoId = 1,
                    DNI = "12345678",
                    Nombres = "Un",
                    Apellidos = "Perro",
                    Celular = "159456753",
                    Direccion = "Jr. Su Casa 10",
                    Email = "un@perro.com",
                    Profesion = "Vagabundo",
                    FechaNacimiento = DateTime.Now,
                    Salario = 300,
                    Sexo = Genero.Masculino,
                    TipoEmpleadoId = 1,
                    IsHabilitado = true,
                    DistritoId=1
                },
                new Empleado
                {
                    EmpleadoId = 2,
                    DNI = "12345678",
                    Nombres = "El",
                    Apellidos = "Supremo",
                    Celular = "159456753",
                    Direccion = "Jr. Su Casa 10",
                    Email = "un@supremo.com",
                    Profesion = "Vagabundo",
                    FechaNacimiento = DateTime.Now,
                    Salario = 300,
                    Sexo = Genero.Masculino,
                    TipoEmpleadoId = 2,
                    IsHabilitado = true,
                    DistritoId=1
                }
            );

            context.Estudiantes.AddOrUpdate(
              p => p.DNI,
              new Estudiante
              {
                  EstudianteId = 1,
                  DNI = "12345678",
                  Nombres = "La",
                  Apellidos = "Ramira",
                  Direccion = "Jr. Su Casa 123",
                  FechaNacimiento = DateTime.Now,
                  RH = TipoSangre.AB_Negativo,
                  Sexo = Genero.Femenino,
                  Habilitado = true,
                  DistritoId = 1
              },
              new Estudiante
              {
                  EstudianteId = 2,
                  DNI = "87654321",
                  Nombres = "El",
                  Apellidos = "Mimo",
                  Direccion = "Jr. Su Jaus 123",
                  FechaNacimiento = DateTime.Now,
                  RH = TipoSangre.A_Negativo,
                  Sexo = Genero.Femenino,
                  Habilitado = true,
                  DistritoId = 1
              }
            );

            context.Grados.AddOrUpdate(
              p => p.Nombre,
              new Grado
              {
                  GradoId = 1,
                  Nombre = "3 a�os"
              },
                new Grado
                {
                    GradoId = 2,
                    Nombre = "4 a�os"
                },
                new Grado
                {
                    GradoId = 3,
                    Nombre = "5 a�os"
                }
            );

            context.Seccions.AddOrUpdate(
              p => p.Nombre,
              new Seccion
              {
                  SeccionId = 1,
                  Nombre = "Perritos 16",
                  AnioEscolarId = 1,
                  GradoId = 1,
                  EmpleadoId=1,
                  Capacidad=20
              },
                new Seccion
                {
                    SeccionId = 2,
                    Nombre = "Gatitos 16",
                    AnioEscolarId = 1,
                    GradoId = 2,
                    EmpleadoId = 1,
                    Capacidad = 20
                },
                new Seccion
                {
                    SeccionId = 3,
                    Nombre = "Perritos 17",
                    AnioEscolarId = 2,
                    GradoId = 1,
                    EmpleadoId = 1,
                    Capacidad = 20
                },
                new Seccion
                {
                    SeccionId = 5,
                    Nombre = "Hormiguitas 17",
                    AnioEscolarId = 2,
                    GradoId = 1,
                    EmpleadoId = 1,
                    Capacidad = 20
                },
                new Seccion
                {
                    SeccionId = 4,
                    Nombre = "Gatitos 17",
                    AnioEscolarId = 2,
                    GradoId = 2,
                    EmpleadoId = 1,
                    Capacidad = 20
                },
                new Seccion
                {
                    SeccionId = 6,
                    Nombre = "Lobitos 17",
                    AnioEscolarId = 2,
                    GradoId = 2,
                    EmpleadoId = 1,
                    Capacidad = 20
                }
            );

            context.Matriculas.AddOrUpdate(
              p => p.NumeroVoucher,
              new Matricula
              {
                  MatriculaId = 1,
                  AnioEscolarId = 2,
                  FechaMatricula = DateTime.Parse("2017/12/10"),
                  EstudianteId = 1,
                  ApoderadoId = 1,
                  GradoId = 1,
                  SeccionId = 3,
                  NumeroVoucher = "451278",
                  EstadoEstudiante = EstadoEstudiante.Regular,
                  MontoMatricula = 100,
                  TipoMatricula = TipoMatricula.Regular
              },
              new Matricula
              {
                  MatriculaId = 2,
                  AnioEscolarId = 2,
                  FechaMatricula = DateTime.Parse("2017/12/10"),
                  EstudianteId = 2,
                  ApoderadoId = 2,
                  GradoId = 1,
                  SeccionId = 3,
                  NumeroVoucher = "784512",
                  EstadoEstudiante = EstadoEstudiante.Regular,
                  MontoMatricula = 100,
                  TipoMatricula = TipoMatricula.Regular
              }
            );

            context.Clases.AddOrUpdate(
                p => p.Nombre,
                new Clase
                {
                    ClaseId = 1,
                    AsignaturaId = 1,
                    Nombre = "G1",
                    IsHabilitado = true,
                    EmpleadoId = 1,
                    SeccionId = 3
                },
                new Clase
                {
                    ClaseId = 2,
                    AsignaturaId = 1,
                    Nombre = "G2",
                    IsHabilitado = true,
                    EmpleadoId = 1,
                    SeccionId = 3
                }
            );

            context.EstudianteClases.AddOrUpdate(
                p => new { p.EstudianteId, p.ClaseId },
                new EstudianteClase
                {
                    EstudianteClaseId = 1,
                    EstudianteId = 1,
                    ClaseId = 1
                },
                new EstudianteClase
                {
                    EstudianteClaseId = 2,
                    EstudianteId = 1,
                    ClaseId = 2
                },
                new EstudianteClase
                {
                    EstudianteClaseId = 3,
                    EstudianteId = 2,
                    ClaseId = 1
                },
                new EstudianteClase
                {
                    EstudianteClaseId = 4,
                    EstudianteId = 2,
                    ClaseId = 2
                }
            );

            context.Notas.AddOrUpdate
            (
                p => new { p.EstudianteClaseId, p.PeriodoId },
                new Nota
                {
                    Vigesimal = 20,
                    EstudianteClaseId = 1,
                    PeriodoId = 2
                },
                new Nota
                {
                    Vigesimal = 20,
                    EstudianteClaseId = 2,
                    PeriodoId = 1
                }
            );
        }
    }
}
