﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB_ST.Domain;

namespace DB_ST.Repository
{
    public interface IEstudianteRepository
    {
        IEnumerable<Estudiante> GetEstudiantes(string SearchCriteria);
        void Create(Estudiante estudiante);
        void Update(Estudiante estudiante);
        void Delete(int id);
        Estudiante FindById(int id);
    }
}
