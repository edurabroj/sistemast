﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB_ST.Domain;

namespace DB_ST.Repository
{
    public class EstudianteRepository : IEstudianteRepository
    {
        DB_STContext _context;

        public EstudianteRepository()
        {
            if (_context == null) _context = new DB_STContext();
        }

        public void Create(Estudiante estudiante)
        {
            _context.Estudiantes.Add(estudiante);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var estudiante = _context.Estudiantes.Find(id);
            _context.Estudiantes.Remove(estudiante);
            _context.SaveChanges();
        }

        public Estudiante FindById(int id)
        {
            return _context.Estudiantes.Find(id);
        }

        public IEnumerable<Estudiante> GetEstudiantes(string SearchCriteria)
        {
            var query = from c in _context.Estudiantes select c;
            if (!String.IsNullOrEmpty(SearchCriteria)) query = query.Where(x => x.Nombres.ToLower().Contains(SearchCriteria.ToLower()));
            return query;
        }


        public void Update(Estudiante estudiante)
        {
            _context.Estudiantes.Attach(estudiante);
            _context.Entry(estudiante).State = EntityState.Modified;
            _context.SaveChanges();
        }
    }
}
