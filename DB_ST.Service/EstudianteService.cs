﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DB_ST.Domain;
using DB_ST.Repository;

namespace DB_ST.Service
{
    public class EstudianteService : IEstudianteService
    {

        private IEstudianteRepository _repository;

        public EstudianteService()
        {
            if (_repository == null) _repository = new EstudianteRepository();
        }

        public void Create(Estudiante estudiante)
        {
            _repository.Create(estudiante);
        }

        public void Delete(int id)
        {
            _repository.Delete(id);
        }

        public Estudiante FindById(int id)
        {
            return _repository.FindById(id);
        }

        public IEnumerable<Estudiante> GetEstudiantes(string SearchCriteria)
        {
            return _repository.GetEstudiantes(SearchCriteria);
        }

        public void Update(Estudiante estudiante)
        {
            _repository.Update(estudiante);
        }
    }
}
