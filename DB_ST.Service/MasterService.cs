﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DB_ST.Repository;

namespace DB_ST.Service
{
    public class MasterService<TEntity> : IMasterService<TEntity> where TEntity : class
    {
        private IMasterRepository<TEntity> _masterRepository;

        public MasterService()
        {
            if (_masterRepository== null)
            {
                _masterRepository= new MasterRepository<TEntity>();
            }
        }

        public IQueryable<TEntity> PerformInclusions(IEnumerable<Expression<Func<TEntity, object>>> includeProperties,
                                                      IQueryable<TEntity> query)
        {
            return _masterRepository.PerformInclusions(includeProperties, query);
        }

        public IQueryable<TEntity> AsQueryable()
        {
            return _masterRepository.AsQueryable();
        }
        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> where, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return _masterRepository.Find(where,includeProperties);
        }

        public void Add(TEntity entidad)
        {
            _masterRepository.Add(entidad);
        }

        public void Commit()
        {
            _masterRepository.Commit();
        }

        public TEntity GetById(int? id)
        {
            return _masterRepository.GetById(id);
        }

        public void Remove(int id)
        {
            _masterRepository.Remove(id);
        }

        public IEnumerable<TEntity> SearchByCriteria(Expression<Func<TEntity, bool>> criterio)
        {
            return _masterRepository.SearchByCriteria(criterio);
        }

        public void Edit(TEntity entity)
        {
            _masterRepository.Edit(entity);
        }

        public IEnumerable<TEntity> GetAll(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return _masterRepository.GetAll(includeProperties);
        }
    }
}
