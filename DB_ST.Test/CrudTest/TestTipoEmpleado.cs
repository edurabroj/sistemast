﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework;
using System.Web;
using System.Web.Routing;

namespace DB_ST.Test
{
    [TestFixture]
    class TestTipoEmpleado
    {
        Mock<IMasterService<TipoEmpleado>> _serviceFalse;
        HttpContextBase context = _Helpers.GetFakeContext();
        //controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

        [SetUp]
        public void SetUp()
        {
            _serviceFalse = new Mock<IMasterService<TipoEmpleado>>();
        }

        [Test]
        public void ListaTipoEmpleado()
        {
            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<TipoEmpleado, bool>>>())).Returns(
                new List<TipoEmpleado>()
                {
                    new TipoEmpleado {
                        TipoEmpleadoId = 1,
                        NombreTipoEmpleado = "Test"
                        

                    },
                    new TipoEmpleado {
                        TipoEmpleadoId = 2,
                        NombreTipoEmpleado = "ll"
                    }
                }
            );

            var controller = new TipoEmpleadoController(_serviceFalse.Object);
            controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

            var resultado = controller.Index(SearchCriteria: "123") as PartialViewResult;
            Assert.AreEqual(2, (resultado.Model as List<TipoEmpleado>).Count());

        }

        [Test]
        public void GuardarExitosoTipoEmpleado()
        {
            var controller = new TipoEmpleadoController(_serviceFalse.Object);
            controller.Create(new TipoEmpleado
            {
                TipoEmpleadoId = 1,
                NombreTipoEmpleado = "Test"
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarFallidoTipoEmpleado()
        {
            var controller = new TipoEmpleadoController(_serviceFalse.Object);
            controller.Create(new TipoEmpleado { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarRepetidoTipoEmpleado()
        {
            var controller = new TipoEmpleadoController(_serviceFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<TipoEmpleado, bool>>>())).Returns(
                new List<TipoEmpleado>()
                {
                    new TipoEmpleado {
                       TipoEmpleadoId = 1,
                        NombreTipoEmpleado = "Test"
                }});


            controller.Create(new TipoEmpleado()
            {
                TipoEmpleadoId = 1,
                NombreTipoEmpleado = "Test"
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        //**********************************************************************************************

        [Test]
        public void EditarExitosoTipoEmpleado()
        {
            var controller = new TipoEmpleadoController(_serviceFalse.Object);
            controller.Edit(new TipoEmpleado
            {
                TipoEmpleadoId = 1,
                NombreTipoEmpleado = "Test"
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarFallidoTipoEmpleado()
        {
            var controller = new TipoEmpleadoController(_serviceFalse.Object);
            controller.Edit(new TipoEmpleado { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarRepetidoTipoEmpleado()
        {
            var controller = new TipoEmpleadoController(_serviceFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<TipoEmpleado, bool>>>())).Returns(
                new List<TipoEmpleado>()
                {
                    new TipoEmpleado {
                       TipoEmpleadoId = 1,
                        NombreTipoEmpleado = "Test"
                }});


            controller.Edit(new TipoEmpleado()
            {
                TipoEmpleadoId = 2,
                NombreTipoEmpleado = "Test"
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
    }
}
