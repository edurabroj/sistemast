﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework.Internal;
using NUnit.Framework;

namespace DB_ST.Test
{
    [TestFixture]
    class TestAnioEscolar
    {
        Mock<IMasterService<AnioEscolar>> _serviceFalse;
        

        [SetUp]
        public void SetUp()
        {
            _serviceFalse = new Mock<IMasterService<AnioEscolar>>();
            
        }

        [Test]
        public void ListaAnioEscolar()
        {


            _serviceFalse.Setup(o => o.GetAll()).Returns(
                new List<AnioEscolar>()
                {
                    new AnioEscolar {
                        AnioEscolarId = 1,
                        Anio = "2016",
                        FechaInicio = DateTime.Now,
                        FechaFin = DateTime.Now,
                        FechaInicioMatricula = DateTime.Now,
                        FechaFinMatricula = DateTime.Now,
                        FechaMatriculaContemporanea = DateTime.Now
                    },
                    new AnioEscolar {
                         AnioEscolarId = 2,
                         Anio = "2017",
                        FechaInicio = DateTime.Now,
                        FechaFin = DateTime.Now,
                        FechaInicioMatricula = DateTime.Now,
                        FechaFinMatricula = DateTime.Now,
                        FechaMatriculaContemporanea = DateTime.Now
                    }
                }
            );

            var controller = new AnioEscolarController(_serviceFalse.Object);

            var resultado = controller.Index("") as ViewResult;

            Assert.AreEqual(2, (resultado.Model as List<AnioEscolar>).Count());

        }

        [Test]
        public void GuardarExitosoAnioEscolar()
        {
            var controller = new AnioEscolarController(_serviceFalse.Object);
            controller.Create(new AnioEscolar
            {
                AnioEscolarId = 1,
                Anio = "2016",
                FechaInicio = DateTime.Now,
                FechaFin = DateTime.Now,
                FechaInicioMatricula = DateTime.Now,
                FechaFinMatricula = DateTime.Now,
                FechaMatriculaContemporanea = DateTime.Now
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarFallidoAnioEscolar()
        {
            var controller = new AnioEscolarController(_serviceFalse.Object);
            controller.Create(new AnioEscolar { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarRepetidoAnioEscolar()
        {
            var controller = new AnioEscolarController(_serviceFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<AnioEscolar, bool>>>())).Returns(
                new List<AnioEscolar>()
                {
                    new AnioEscolar {
                        AnioEscolarId = 1,
                        Anio = "2016",
                        FechaInicio = DateTime.Now,
                        FechaFin = DateTime.Now,
                        FechaInicioMatricula = DateTime.Now,
                        FechaFinMatricula = DateTime.Now,
                        FechaMatriculaContemporanea = DateTime.Now
                }});


            controller.Create(new AnioEscolar()
            {
                AnioEscolarId = 2,
                Anio = "2016",
                FechaInicio = DateTime.Now,
                FechaFin = DateTime.Now,
                FechaInicioMatricula = DateTime.Now,
                FechaFinMatricula = DateTime.Now,
                FechaMatriculaContemporanea = DateTime.Now
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        //--------------------------------------------------------------------------------------------
        [Test]
        public void EditarExitosoAnioEscolar()
        {
            var controller = new AnioEscolarController(_serviceFalse.Object);
            controller.Edit(new AnioEscolar
            {
                AnioEscolarId = 1,
                Anio = "2016",
                FechaInicio = DateTime.Now,
                FechaFin = DateTime.Now,
                FechaInicioMatricula = DateTime.Now,
                FechaFinMatricula = DateTime.Now,
                FechaMatriculaContemporanea = DateTime.Now
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarFallidoAnioEscolar()
        {
            var controller = new AnioEscolarController(_serviceFalse.Object);
            controller.Edit(new AnioEscolar { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarRepetidoAnioEscolar()
        {
            var controller = new AnioEscolarController(_serviceFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<AnioEscolar, bool>>>())).Returns(
                new List<AnioEscolar>()
                {
                    new AnioEscolar {
                        AnioEscolarId = 1,
                        Anio = "2016",
                        FechaInicio = DateTime.Now,
                        FechaFin = DateTime.Now,
                        FechaInicioMatricula = DateTime.Now,
                        FechaFinMatricula = DateTime.Now,
                        FechaMatriculaContemporanea = DateTime.Now
                }});


            controller.Edit(new AnioEscolar()
            {
                AnioEscolarId = 2,
                Anio = "2016",
                FechaInicio = DateTime.Now,
                FechaFin = DateTime.Now,
                FechaInicioMatricula = DateTime.Now,
                FechaFinMatricula = DateTime.Now,
                FechaMatriculaContemporanea = DateTime.Now
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
    }
}

