﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DB_ST.Domain;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace DB_ST.Test
{
    //[TestFixture]
    //class TestVacuna
    //{
    //    Mock<IMasterService<Vacuna>> _serviceFalse;

    //    HttpContextBase context = _Helpers.GetFakeContext();
    //    //controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

    //    [SetUp]
    //    public void SetUp()
    //    {
    //        _serviceFalse = new Mock<IMasterService<Vacuna>>();
            
    //    }

    //    [Test]
    //    public void ListaVacuna()
    //    {


    //        _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Vacuna, bool>>>())).Returns(
    //            new List<Vacuna>()
    //            {
    //                new Vacuna {
    //                    VacunaId = 1,
    //                    TipoVacuna = "Test",
    //                    Descripcion= "Tests"
    //                },
    //                new Vacuna {
    //                     VacunaId = 2,
    //                    TipoVacuna = "Test",
    //                    Descripcion= "Tests"
    //                }
    //            }
    //        );

    //        var controller = new VacunaController(_serviceFalse.Object);

    //        controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

    //        var resultado = controller.Index("Test") as PartialViewResult;

    //        Assert.AreEqual(2, (resultado.Model as List<Vacuna>).Count());

    //    }

    //    [Test]
    //    public void GuardarExitosoVacuna()
    //    {
    //        var controller = new VacunaController(_serviceFalse.Object);
    //        controller.Create(new Vacuna
    //        {
    //            VacunaId = 1,
    //            TipoVacuna = "Test",
    //            Descripcion = "Tests"
    //        });
    //        Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
    //    }

    //    [Test]
    //    public void GuardarFallidoVacuna()
    //    {
    //        var controller = new VacunaController(_serviceFalse.Object);
    //        controller.Create(new Vacuna { });
    //        Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
    //    }

    //    [Test]
    //    public void GuardarRepetidoVacuna()
    //    {
    //        var controller = new VacunaController(_serviceFalse.Object);

    //        _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Vacuna, bool>>>())).Returns(
    //            new List<Vacuna>()
    //            {
    //                new Vacuna {
    //                   VacunaId = 1,
    //                    TipoVacuna = "Test",
    //                    Descripcion= "Tests"
    //            }});


    //        controller.Create(new Vacuna()
    //        {
    //            VacunaId = 2,
    //            TipoVacuna = "Test",
    //            Descripcion = "Tests"
    //        });


    //        Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
    //    }

    //    //***********************************************************************

    //    [Test]
    //    public void EditarExitosoVacuna()
    //    {
    //        var controller = new VacunaController(_serviceFalse.Object);
    //        controller.Edit(new Vacuna
    //        {
    //            VacunaId = 1,
    //            TipoVacuna = "Test",
    //            Descripcion = "Tests"
    //        });
    //        Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
    //    }

    //    [Test]
    //    public void EditarFallidoVacuna()
    //    {
    //        var controller = new VacunaController(_serviceFalse.Object);
    //        controller.Edit(new Vacuna { });
    //        Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
    //    }

    //    [Test]
    //    public void EditarRepetidoVacuna()
    //    {
    //        var controller = new VacunaController(_serviceFalse.Object);

    //        _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Vacuna, bool>>>())).Returns(
    //            new List<Vacuna>()
    //            {
    //                new Vacuna {
    //                   VacunaId = 1,
    //                    TipoVacuna = "Test",
    //                    Descripcion= "Tests"
    //            }});


    //        controller.Edit(new Vacuna()
    //        {
    //            VacunaId = 2,
    //            TipoVacuna = "Test",
    //            Descripcion = "Tests"
    //        });


    //        Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
    //    }
    //}
}
