﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Domain.Enum;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework;
using System.Web.Routing;
using System.Web;
using PagedList;

namespace DB_ST.Test
{
    [TestFixture]
    public class TestsEstudiante
    {
        Mock <IMasterService<Estudiante>> _serviceFalse;
        //ubigeo
        Mock<IMasterService<Departamento>> _serviceDepFalse;
        Mock<IMasterService<Provincia>> _serviceProFalse;
        Mock<IMasterService<Distrito>> _serviceDisFalse;
        HttpContextBase context = _Helpers.GetFakeContext();
        //controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

        [SetUp]
        public void SetUp()
        {
            _serviceFalse = new Mock<IMasterService<Estudiante>>();
            _serviceDepFalse = new Mock<IMasterService<Departamento>>();
            _serviceProFalse = new Mock<IMasterService<Provincia>>();
            _serviceDisFalse = new Mock<IMasterService<Distrito>>();
        }


        [Test]
        public void ListaEstudiante()
        {
           _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Estudiante, bool>>>(),
                It.IsAny<Expression<Func<Estudiante, object>>>())).Returns( 
                new List<Estudiante>()
                {
                    new Estudiante {
                        EstudianteId= 1,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Femenino,
                        RH = TipoSangre.AB_Negativo,
                        Direccion = "jr.cul",
                        DistritoId = 1,
                        Habilitado = true,
                        EstadoEstudiante = EstadoEstudiante.Regular

                    },
                    new Estudiante {
                        EstudianteId= 2,
                        Nombres = "Test2",
                        Apellidos="Tests2",
                        DNI="12345679",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Masculino,
                        RH = TipoSangre.AB_Positivo,
                        Direccion = "jr.culiao",
                        DistritoId = 2,
                        Habilitado = true,
                        EstadoEstudiante = EstadoEstudiante.Regular
                    }
                }
            );
            var controller = new EstudianteController(_serviceFalse.Object, _serviceDepFalse.Object, _serviceProFalse.Object, _serviceDisFalse.Object);

            controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

            var resultado = controller.Index(SearchCriteria:"", page:null) as PartialViewResult;
            Assert.AreEqual(2, (resultado.Model as IPagedList<Estudiante>).Count());

        }

        [Test]
        public void GuardarExitosoEstudiante()
        {
            var controller = new EstudianteController(_serviceFalse.Object, _serviceDepFalse.Object, _serviceProFalse.Object, _serviceDisFalse.Object);
            controller.Create(new Estudiante
            {
                EstudianteId = 2,
                Nombres = "Test2",
                Apellidos = "Tests2",
                DNI = "12345679",
                FechaNacimiento = DateTime.Now,
                Sexo = Genero.Masculino,
                RH = TipoSangre.AB_Positivo,
                Direccion = "jr.culiao",
                DistritoId = 2,
                Habilitado = true,
                EstadoEstudiante = EstadoEstudiante.Regular
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarFallidoEstudiante()
        {

            var controller = new EstudianteController(_serviceFalse.Object, _serviceDepFalse.Object, _serviceProFalse.Object, _serviceDisFalse.Object);
            controller.Create(new Estudiante { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarRepetidoEstudiante()
        {
            var controller = new EstudianteController(_serviceFalse.Object, _serviceDepFalse.Object, _serviceProFalse.Object, _serviceDisFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Estudiante, bool>>>())).Returns(
                new List<Estudiante>()
                {
                    new Estudiante {
                        EstudianteId= 1,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Femenino,
                        RH = TipoSangre.AB_Negativo,
                        Direccion = "jr.cul",
                        DistritoId = 1,
                        Habilitado = true,
                        EstadoEstudiante = EstadoEstudiante.Regular
                }});


            controller.Create(new Estudiante
            {
                EstudianteId = 2,
                Nombres = "Test2",
                Apellidos = "Tests2",
                DNI = "12345678",
                FechaNacimiento = DateTime.Now,
                Sexo = Genero.Masculino,
                RH = TipoSangre.AB_Positivo,
                Direccion = "jr.culiao",
                DistritoId = 2,
                Habilitado = true,
                EstadoEstudiante = EstadoEstudiante.Regular
            });

            
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        

        [Test]
        public void EditarExitosoEstudiante()
        {
            var controller = new EstudianteController(_serviceFalse.Object, _serviceDepFalse.Object, _serviceProFalse.Object, _serviceDisFalse.Object);
            controller.Edit(new Estudiante
            {
                EstudianteId = 2,
                Nombres = "Test2",
                Apellidos = "Tests2",
                DNI = "12345679",
                FechaNacimiento = DateTime.Now,
                Sexo = Genero.Masculino,
                RH = TipoSangre.AB_Positivo,
                Direccion = "jr.culiao",
                DistritoId = 2,
                Habilitado = true,
                EstadoEstudiante = EstadoEstudiante.Regular
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }
        [Test]
        public void EditarFallidoEstudiante()
        {

            var controller = new EstudianteController(_serviceFalse.Object, _serviceDepFalse.Object, _serviceProFalse.Object, _serviceDisFalse.Object);
            controller.Edit(new Estudiante { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
        [Test]
        public void EditarRepetidoEstudiante()
        {
            var controller = new EstudianteController(_serviceFalse.Object, _serviceDepFalse.Object, _serviceProFalse.Object, _serviceDisFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Estudiante, bool>>>())).Returns(
                new List<Estudiante>()
                {
                    new Estudiante {
                        EstudianteId= 1,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Femenino,
                        RH = TipoSangre.AB_Negativo,
                        Direccion = "jr.cul",
                        DistritoId = 1,
                        Habilitado = true,
                        EstadoEstudiante = EstadoEstudiante.Regular
                }});


            controller.Edit(new Estudiante
            {
                EstudianteId = 1,
                Nombres = "Test",
                Apellidos = "Tests",
                DNI = "12345678",
                FechaNacimiento = DateTime.Now,
                Sexo = Genero.Masculino,
                RH = TipoSangre.AB_Positivo,
                Direccion = "jr.culiao",
                DistritoId = 2,
                Habilitado = true,
                EstadoEstudiante = EstadoEstudiante.Regular
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

    }
}
