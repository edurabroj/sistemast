﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Domain.Enum;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework;
using System.Web;
using System.Web.Routing;

namespace DB_ST.Test
{
    [TestFixture]
    class TestMaterial
    {
            Mock<IMasterService<Material>> _serviceFalse;
            Mock<IMasterService<Seccion>> _serviceSeccionFalse;
        HttpContextBase context = _Helpers.GetFakeContext();
        //controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

        [SetUp]
            public void SetUp()
            {
                _serviceFalse = new Mock<IMasterService<Material>>();
                _serviceSeccionFalse = new Mock<IMasterService<Seccion>>();
            }

        [Test]
        public void ListaMaterial()
        {
            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Material, bool>>>(),
                It.IsAny<Expression<Func<Material, object>>>())).Returns(
                new List<Material>()
                {
                    new Material {
                        MaterialId = 1,
                        Nombre = "Test",
                        Descripcion= "Tests",
                        Disponibles=5,
                        Usados = 3,
                        SeccionId = 1
                    },
                    new Material {
                         MaterialId = 2,
                        Nombre = "Tests",
                        Descripcion= "Tests",
                        Disponibles=5,
                        Usados = 3,
                        SeccionId = 1
                    }
                }
            );

            var controller = new MaterialController(_serviceFalse.Object, _serviceSeccionFalse.Object);
            controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

            var resultado = controller.Index("Test") as PartialViewResult;
            
            Assert.AreEqual(2, (resultado.Model as List<Material>).Count());

        }

        [Test]
        public void GuardarExitosoMaterial()
        {
            var controller = new MaterialController(_serviceFalse.Object, _serviceSeccionFalse.Object);
            controller.Create(new Material
            {
                MaterialId = 1,
                Nombre = "Test",
                Descripcion = "Tests",
                Disponibles = 5,
                Usados = 3,
                SeccionId = 1
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarFallidoMaterial()
        {
            var controller = new MaterialController(_serviceFalse.Object, _serviceSeccionFalse.Object);
            controller.Create(new Material { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarRepetidoMaterial()
        {
            var controller = new MaterialController(_serviceFalse.Object, _serviceSeccionFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Material, bool>>>())).Returns(
                new List<Material>()
                {
                    new Material {
                         MaterialId = 1,
                        Nombre = "Test",
                        Descripcion= "Tests",
                        Disponibles=5,
                        Usados = 3,
                        SeccionId = 1
                }});


            controller.Create(new Material()
            {
                MaterialId = 2,
                Nombre = "Test",
                Descripcion = "Tests",
                Disponibles = 5,
                Usados = 3,
                SeccionId = 1
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        //*************************************************************************************

        [Test]
        public void EditarExitosoMaterial()
        {
            var controller = new MaterialController(_serviceFalse.Object, _serviceSeccionFalse.Object);
            controller.Edit(new Material
            {
                MaterialId = 1,
                Nombre = "Test",
                Descripcion = "Tests",
                Disponibles = 5,
                Usados = 3,
                SeccionId = 1
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarFallidoMaterial()
        {
            var controller = new MaterialController(_serviceFalse.Object, _serviceSeccionFalse.Object);
            controller.Edit(new Material { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarRepetidoMaterial()
        {
            var controller = new MaterialController(_serviceFalse.Object, _serviceSeccionFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Material, bool>>>())).Returns(
                new List<Material>()
                {
                    new Material {
                         MaterialId = 1,
                        Nombre = "Test",
                        Descripcion= "Tests",
                        Disponibles=5,
                        Usados = 3,
                        SeccionId = 1
                }});


            controller.Edit(new Material()
            {
                MaterialId = 2,
                Nombre = "Test",
                Descripcion = "Tests",
                Disponibles = 5,
                Usados = 3,
                SeccionId = 1
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

    }
}
