﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Domain.Enum;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework;
using System.Web;
using System.Web.Routing;
using PagedList;

namespace DB_ST.Test
{
    [TestFixture]
    class TestApoderado
    {
        Mock<IMasterService<Apoderado>> _serviceFalse;
        HttpContextBase context = _Helpers.GetFakeContext();
        //controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

        [SetUp]
        public void SetUp()
        {

            _serviceFalse = new Mock<IMasterService<Apoderado>>();
        }

        [Test]
        public void ListaApoderado()
        {
            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Apoderado, bool>>>())).Returns(
                new List<Apoderado>()
                {
                    new Apoderado {
                        ApoderadoId = 1,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Profesion = "Abogado",
                        Email = "a@hot",
                        Celular = "123456789"

                    },
                    new Apoderado {
                        ApoderadoId = 2,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Profesion = "Abogado",
                        Email = "a@hot",
                        Celular = "123456789"
                    }
                }
            );
                        
            var controller = new ApoderadoController(_serviceFalse.Object);
            controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

            var resultado = controller.Index(SearchCriteria: "", page:null) as PartialViewResult;
            Assert.AreEqual(2, (resultado.Model as IPagedList<Apoderado>).Count());

        }

        [Test]
        public void GuardarExitosoApoderado()
        {
            var controller = new ApoderadoController(_serviceFalse.Object);
            controller.Create(new Apoderado
            {
                ApoderadoId = 1,
                Nombres = "Test",
                Apellidos = "Tests",
                DNI = "12345678",
                FechaNacimiento = DateTime.Now,
                Profesion = "Abogado",
                Email = "a@hot",
                Celular = "123456789"
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarFallidoApoderado()
        {
            var controller = new ApoderadoController(_serviceFalse.Object);
            controller.Create(new Apoderado { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarRepetidoApoderado()
        {
            var controller = new ApoderadoController(_serviceFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Apoderado, bool>>>())).Returns(
                new List<Apoderado>()
                {
                    new Apoderado {
                         ApoderadoId = 1,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Profesion = "Abogado",
                        Email = "a@hot",
                        Celular = "123456789"
                }});


            controller.Create(new Apoderado()
            {
                ApoderadoId = 2,
                Nombres = "Test",
                Apellidos = "Tests",
                DNI = "12345678",
                FechaNacimiento = DateTime.Now,
                Profesion = "Abogado",
                Email = "a@hot",
                Celular = "123456789"
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        //-----------------------------------------------------------------------------------------------
        [Test]
        public void EditarExitosoApoderado()
        {
            var controller = new ApoderadoController(_serviceFalse.Object);
            controller.Edit(new Apoderado
            {
                ApoderadoId = 1,
                Nombres = "Test",
                Apellidos = "Tests",
                DNI = "12345678",
                FechaNacimiento = DateTime.Now,
                Profesion = "Abogado",
                Email = "a@hot",
                Celular = "123456789"
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarFallidoApoderado()
        {
            var controller = new ApoderadoController(_serviceFalse.Object);
            controller.Edit(new Apoderado { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarRepetidoApoderado()
        {
            var controller = new ApoderadoController(_serviceFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Apoderado, bool>>>())).Returns(
                new List<Apoderado>()
                {
                    new Apoderado {
                         ApoderadoId = 1,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Profesion = "Abogado",
                        Email = "a@hot",
                        Celular = "123456789"
                }});


            controller.Edit(new Apoderado()
            {
                ApoderadoId = 2,
                Nombres = "Test",
                Apellidos = "Tests",
                DNI = "12345678",
                FechaNacimiento = DateTime.Now,
                Profesion = "Abogado",
                Email = "a@hot",
                Celular = "123456789"
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
    }
}
