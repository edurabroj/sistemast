﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework.Internal;
using NUnit.Framework;
using System.Web.Routing;
using System.Web;

namespace DB_ST.Test
{
    [TestFixture]
    class TestSeccion
    {
        Mock<IMasterService<Seccion>> _serviceFalse;
        Mock<IMasterService<Grado>> _serviceGradoFalse;
        Mock<IMasterService<AnioEscolar>> _serviceAnioEscolarFalse;
        Mock<IMasterService<Empleado>> _serviceEmpleadoFalse;
        HttpContextBase context = _Helpers.GetFakeContext();
        //controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

        [SetUp]
        public void SetUp()
        {
            _serviceFalse = new Mock<IMasterService<Seccion>>();
            _serviceGradoFalse = new Mock<IMasterService<Grado>>();
            _serviceAnioEscolarFalse = new Mock<IMasterService<AnioEscolar>>();
            _serviceEmpleadoFalse = new Mock<IMasterService<Empleado>>();

            _serviceEmpleadoFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Empleado, bool>>>())).Returns(
                new List<Empleado>()
                {
                    new Empleado {
                }});
        }

        [Test]
        public void ListaSeccion()
        {
            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Seccion, bool>>>(),
                It.IsAny<Expression<Func<Seccion, object>>>(),
                It.IsAny<Expression<Func<Seccion, object>>>()
                )).Returns(
                new List<Seccion>()
                {
                    new Seccion {
                        SeccionId = 1,
                        Nombre = "Test",
                        Capacidad = 20,
                        //VacantesDisp= 15,
                        AnioEscolar= new AnioEscolar(),
                        Grado = new Grado()
                       
                    },
                    new Seccion {
                        SeccionId = 2,
                        Nombre = "Tests",
                        Capacidad = 20,
                        //VacantesDisp= 15,
                        AnioEscolar= new AnioEscolar(),
                        Grado = new Grado()
                    }
                }
            );

            var controller = new SeccionController(_serviceFalse.Object, _serviceGradoFalse.Object,_serviceAnioEscolarFalse.Object,_serviceEmpleadoFalse.Object);

            controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);
            var resultado = controller.Index("Test") as PartialViewResult;

            Assert.AreEqual(2, (resultado.Model as List<Seccion>).Count());

        }

        [Test]
        public void GuardarExitosoSeccion()
        {
            var controller = new SeccionController
                (_serviceFalse.Object, 
                _serviceGradoFalse.Object,
                _serviceAnioEscolarFalse.Object,
                _serviceEmpleadoFalse.Object);
            controller.Create(new Seccion
            {
                SeccionId = 1,
                Nombre = "Test",
                Capacidad = 20,
                //VacantesDisp = 15,
                AnioEscolar = new AnioEscolar(),
                Grado = new Grado()
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarFallidoSeccion()
        {
            var controller = new SeccionController(_serviceFalse.Object, _serviceGradoFalse.Object, _serviceAnioEscolarFalse.Object, _serviceEmpleadoFalse.Object);
            controller.Create(new Seccion { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarRepetidoSeccion()
        {
            var controller = new SeccionController(_serviceFalse.Object, _serviceGradoFalse.Object, _serviceAnioEscolarFalse.Object, _serviceEmpleadoFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Seccion, bool>>>())).Returns(
                new List<Seccion>()
                {
                    new Seccion {
                        SeccionId = 1,
                        Nombre = "Test",
                        Capacidad = 20,
                        //VacantesDisp= 15,
                        AnioEscolar= new AnioEscolar(),
                        Grado = new Grado()
                }});


            controller.Create(new Seccion()
            {
                SeccionId = 2,
                Nombre = "Test",
                Capacidad = 20,
                //VacantesDisp = 15,
                AnioEscolar = new AnioEscolar(),
                Grado = new Grado()
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        //****************************************************************************************************

        [Test]
        public void EditarExitosoSeccion()
        {
            var controller = new SeccionController
                (_serviceFalse.Object,
                _serviceGradoFalse.Object,
                _serviceAnioEscolarFalse.Object,
                _serviceEmpleadoFalse.Object);
            controller.Edit(new Seccion
            {
                SeccionId = 1,
                Nombre = "Test",
                Capacidad = 20,
                //VacantesDisp = 15,
                AnioEscolar = new AnioEscolar(),
                Grado = new Grado()
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarFallidoSeccion()
        {
            var controller = new SeccionController(_serviceFalse.Object, _serviceGradoFalse.Object, _serviceAnioEscolarFalse.Object, _serviceEmpleadoFalse.Object);
            controller.Edit(new Seccion { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarRepetidoSeccion()
        {
            var controller = new SeccionController(_serviceFalse.Object, _serviceGradoFalse.Object, _serviceAnioEscolarFalse.Object, _serviceEmpleadoFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Seccion, bool>>>())).Returns(
                new List<Seccion>()
                {
                    new Seccion {
                        SeccionId = 1,
                        Nombre = "Test",
                        Capacidad = 20,
                        //VacantesDisp= 15,
                        AnioEscolar= new AnioEscolar(),
                        Grado = new Grado()
                }});


            controller.Edit(new Seccion()
            {
                SeccionId = 2,
                Nombre = "Test",
                Capacidad = 20,
                //VacantesDisp = 15,
                AnioEscolar = new AnioEscolar(),
                Grado = new Grado()
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
    }
}

