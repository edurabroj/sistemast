﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework;
using System.Web;
using System.Web.Routing;

namespace DB_ST.Test
{
    [TestFixture]
    class TestGrado
    {
        Mock<IMasterService<Grado>> _serviceFalse;
        HttpContextBase context = _Helpers.GetFakeContext();
        //controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

        [SetUp]
        public void SetUp()
        {
            _serviceFalse = new Mock<IMasterService<Grado>>();
        }

        [Test]
        public void ListaGrado()
        {


            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Grado, bool>>>())).Returns(
                new List<Grado>()
                {
                    new Grado {
                        GradoId = 1,
                        Nombre = "Test",
                        UlltimaModificacion = DateTime.Now
                    },
                    new Grado {
                         GradoId = 2,
                        Nombre = "Tests",
                        UlltimaModificacion = DateTime.Now
                    }
                }
            );

            var controller = new GradoController(_serviceFalse.Object);
            controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

            var resultado = controller.Index("Test") as PartialViewResult;

            Assert.AreEqual(2, (resultado.Model as List<Grado>).Count());

        }

        [Test]
        public void GuardarExitosoGrado()
        {
            var controller = new GradoController(_serviceFalse.Object);
            controller.Create(new Grado
            {
                GradoId = 1,
                Nombre = "Test",
                UlltimaModificacion = DateTime.Now
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarFallidoGrado()
        {
            var controller = new GradoController(_serviceFalse.Object);
            controller.Create(new Grado { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarRepetidoGrado()
        {
            var controller = new GradoController(_serviceFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Grado, bool>>>())).Returns(
                new List<Grado>()
                {
                    new Grado {
                         GradoId = 1,
                        Nombre = "Test",
                        UlltimaModificacion = DateTime.Now
                }});


            controller.Create(new Grado()
            {
                GradoId = 2,
                Nombre = "Test",
                UlltimaModificacion = DateTime.Now
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        //*************************************************************************************

        [Test]
        public void EditarExitosoGrado()
        {
            var controller = new GradoController(_serviceFalse.Object);
            controller.Edit(new Grado
            {
                GradoId = 1,
                Nombre = "Test",
                UlltimaModificacion = DateTime.Now
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarFallidoGrado()
        {
            var controller = new GradoController(_serviceFalse.Object);
            controller.Edit(new Grado { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarRepetidoGrado()
        {
            var controller = new GradoController(_serviceFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Grado, bool>>>())).Returns(
                new List<Grado>()
                {
                    new Grado {
                         GradoId = 1,
                        Nombre = "Test",
                        UlltimaModificacion = DateTime.Now
                }});


            controller.Edit(new Grado()
            {
                GradoId = 2,
                Nombre = "Test",
                UlltimaModificacion = DateTime.Now
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
    }
}
