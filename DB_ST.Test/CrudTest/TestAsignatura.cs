﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework.Internal;
using NUnit.Framework;
using System.Web;
using System.Web.Routing;

namespace DB_ST.Test
{
    [TestFixture]
    class TestAsignatura
    {
        Mock<IMasterService<Asignatura>> _serviceFalse;

        HttpContextBase context = _Helpers.GetFakeContext();
        //controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

        [SetUp]
        public void SetUp()
        {
            _serviceFalse = new Mock<IMasterService<Asignatura>>();
            
        }

        [Test]
        public void ListaAsignatura()
        {


            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Asignatura, bool>>>())).Returns(
                new List<Asignatura>()
                {
                    new Asignatura {
                        AsignaturaId = 1,
                        Nombre = "Test"
                    },
                    new Asignatura {
                         AsignaturaId = 2,
                        Nombre = "Tests"
                    }
                }
            );

            var controller = new AsignaturaController(_serviceFalse.Object);
            controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);
            var resultado = controller.Index("Test") as PartialViewResult;

            Assert.AreEqual(2, (resultado.Model as List<Asignatura>).Count());

        }

        [Test]
        public void GuardarExitosoAsignatura()
        {
            var controller = new AsignaturaController(_serviceFalse.Object);
            controller.Create(new Asignatura
            {
                AsignaturaId = 1,
                Nombre = "Test"
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarFallidoAsignatura()
        {
            var controller = new AsignaturaController(_serviceFalse.Object);
            controller.Create(new Asignatura { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarRepetidoAsignatura()
        {
            var controller = new AsignaturaController(_serviceFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Asignatura, bool>>>())).Returns(
                new List<Asignatura>()
                {
                    new Asignatura {
                         AsignaturaId = 1,
                        Nombre = "Test"
                }});


            controller.Create(new Asignatura()
            {
                AsignaturaId = 2,
                Nombre = "Test"
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        //-----------------------------------------------------------------------------------------
        [Test]
        public void EditarExitosoAsignatura()
        {
            var controller = new AsignaturaController(_serviceFalse.Object);
            controller.Edit(new Asignatura
            {
                AsignaturaId = 1,
                Nombre = "Test"
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarFallidoAsignatura()
        {
            var controller = new AsignaturaController(_serviceFalse.Object);
            controller.Edit(new Asignatura { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarRepetidoAsignatura()
        {
            var controller = new AsignaturaController(_serviceFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Asignatura, bool>>>())).Returns(
                new List<Asignatura>()
                {
                    new Asignatura {
                         AsignaturaId = 1,
                        Nombre = "Test"
                }});


            controller.Edit(new Asignatura()
            {
                AsignaturaId = 2,
                Nombre = "Test"
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
    }
}

