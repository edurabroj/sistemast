﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using DB_ST.Domain;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace DB_ST.Test
{
    [TestFixture]
    class TestClase
    {
        Mock<IMasterService<Clase>> _serviceFalse;
        Mock<IMasterService<Asignatura>> _serviceAsignaturaFalse;
        Mock<IMasterService<AnioEscolar>> _serviceAnioFalse;
        Mock<IMasterService<Seccion>> _serviceSeccionFalse;
        Mock<IMasterService<Grado>> _serviceGradoFalse;
        Mock<IMasterService<Empleado>> _sEmpleadoFalse;

        HttpContextBase context = _Helpers.GetFakeContext();
        //controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

        [SetUp]
        public void SetUp()
        {
            _serviceFalse = new Mock<IMasterService<Clase>>();
            _serviceAsignaturaFalse= new Mock<IMasterService<Asignatura>>();
            _serviceAnioFalse = new Mock<IMasterService<AnioEscolar>>();
            _serviceSeccionFalse = new Mock<IMasterService<Seccion>>();
            _serviceGradoFalse = new Mock<IMasterService<Grado>>();
            _sEmpleadoFalse = new Mock<IMasterService<Empleado>>();
        }


        [Test]
        public void ListaClase()
        {
            //mock año falso
            _serviceAnioFalse.Setup(o => o.Find(It.IsAny<Expression<Func<AnioEscolar, bool>>>())).Returns(
                new List<AnioEscolar>()
                {
                    new AnioEscolar {
                        AnioEscolarId = 1,
                        NombreAnio = "Test"
                    },
                    new AnioEscolar {
                         AnioEscolarId = 2,
                        NombreAnio = "Tests"
                    }
                }
            );

            //mock seccion falsa
            _serviceSeccionFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Seccion, bool>>>())).Returns(
                new List<Seccion>()
                {
                    new Seccion {
                        SeccionId = 1,
                        Nombre = "Test"
                    },
                    new Seccion {
                         SeccionId = 2,
                        Nombre = "Tests"
                    }
                }
            );

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Clase, bool>>>(),
                                            It.IsAny<Expression<Func<Clase, object>>>())).Returns(
                new List<Clase>()
                {
                    new Clase {
                        ClaseId = 1,
                        Nombre = "Test"
                        
                    },
                    new Clase {
                         ClaseId = 2,
                        Nombre = "Tests"
                        
                    }
                }
            );

            var controller = new ClaseController(_serviceFalse.Object,_serviceAsignaturaFalse.Object,_serviceAnioFalse.Object,_serviceSeccionFalse.Object,_serviceGradoFalse.Object,_sEmpleadoFalse.Object);

            controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

            var resultado = controller.Index("Test",seccionId:null,verDisabled:false) as PartialViewResult;

            Assert.AreEqual(2, (resultado.Model as List<Clase>).Count());

        }

        [Test]
        public void GuardarExitosoClase()
        {
            var controller = new ClaseController(_serviceFalse.Object, _serviceAsignaturaFalse.Object, _serviceAnioFalse.Object, _serviceSeccionFalse.Object, _serviceGradoFalse.Object, _sEmpleadoFalse.Object);
            controller.Create(new Clase
            {
                ClaseId = 1,
                Nombre = "Test"
                
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarFallidoClase()
        {
            var controller = new ClaseController(_serviceFalse.Object, _serviceAsignaturaFalse.Object, _serviceAnioFalse.Object, _serviceSeccionFalse.Object, _serviceGradoFalse.Object, _sEmpleadoFalse.Object);
            controller.Create(new Clase { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarRepetidoClase()
        {
            var controller = new ClaseController(_serviceFalse.Object, _serviceAsignaturaFalse.Object, _serviceAnioFalse.Object, _serviceSeccionFalse.Object, _serviceGradoFalse.Object, _sEmpleadoFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Clase, bool>>>())).Returns(
                new List<Clase>()
                {
                    new Clase {
                         ClaseId = 1,
                        Nombre = "Test"
                }});


            controller.Create(new Clase()
            {
                ClaseId = 2,
                Nombre = "Test"
            });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
        //---------------------------------------------------------------------------------------
        [Test]
        public void EditarExitosoClase()
        {
            var controller = new ClaseController(_serviceFalse.Object, _serviceAsignaturaFalse.Object, _serviceAnioFalse.Object, _serviceSeccionFalse.Object, _serviceGradoFalse.Object, _sEmpleadoFalse.Object);
            controller.Edit(new Clase
            {
                ClaseId = 1,
                Nombre = "Test"

            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarFallidoClase()
        {
            var controller = new ClaseController(_serviceFalse.Object, _serviceAsignaturaFalse.Object, _serviceAnioFalse.Object, _serviceSeccionFalse.Object, _serviceGradoFalse.Object, _sEmpleadoFalse.Object);
            controller.Edit(new Clase { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarRepetidoClase()
        {
            var controller = new ClaseController(_serviceFalse.Object, _serviceAsignaturaFalse.Object, _serviceAnioFalse.Object, _serviceSeccionFalse.Object, _serviceGradoFalse.Object, _sEmpleadoFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Clase, bool>>>())).Returns(
                new List<Clase>()
                {
                    new Clase {
                         ClaseId = 1,
                        Nombre = "Test"
                }});


            controller.Edit(new Clase()
            {
                ClaseId = 2,
                Nombre = "Test"
            });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
    }

}

