﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Domain.Enum;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework;
using System.Web.Routing;
using System.Web;
using PagedList;

namespace DB_ST.Test
{
    [TestFixture]
    class TestEmpleado
    {
        Mock<IMasterService<Empleado>> _serviceFalse;
        Mock<IMasterService<TipoEmpleado>> _serviceTipoEmpleadosFalse;
        Mock<IMasterService<Departamento>> _serviceDepartamentosFalse;
        HttpContextBase context = _Helpers.GetFakeContext();
        //controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

        [SetUp]
        public void SetUp()
        {
            _serviceFalse = new Mock<IMasterService<Empleado>>();
            _serviceTipoEmpleadosFalse = new Mock<IMasterService<TipoEmpleado>>();
            _serviceDepartamentosFalse = new Mock<IMasterService<Departamento>>();
        }



        [Test]
        public void ListaEmpleado()
        {
            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Empleado, bool>>>(),
                It.IsAny<Expression<Func<Empleado, object>>>())).Returns(
                new List<Empleado>()
                {
                    new Empleado {
                        EmpleadoId = 1,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Femenino,
                        Direccion = "jr.cul",
                        Email = "b@hot.com",
                        Celular = "123456",
                        Profesion = "Abogado",
                        Salario = 1000,
                        IsHabilitado = true,
                        TipoEmpleadoId = 1

                    },
                    new Empleado {
                         EmpleadoId = 2,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Femenino,
                        Direccion = "jr.cul",
                        Email = "b@hot.com",
                        Celular = "123456",
                        Profesion = "Abogado",
                        Salario = 1000,
                        IsHabilitado = true,
                        TipoEmpleadoId = 1
                    }
                }
            );

            var controller = new EmpleadoController(_serviceFalse.Object, _serviceTipoEmpleadosFalse.Object, _serviceDepartamentosFalse.Object);
            controller.ControllerContext = new ControllerContext(context, new RouteData(), controller);

            var resultado = controller.Index("",null) as PartialViewResult;
            Assert.AreEqual(2, (resultado.Model as IPagedList<Empleado>).Count());

        }

        [Test]
        public void GuardarExitosoEmpleado()
        {
            var controller = new EmpleadoController(_serviceFalse.Object, _serviceTipoEmpleadosFalse.Object, _serviceDepartamentosFalse.Object);
            controller.Create(new Empleado
            {
                EmpleadoId = 1,
                Nombres = "Test",
                Apellidos = "Tests",
                DNI = "12345678",
                FechaNacimiento = DateTime.Now,
                Sexo = Genero.Femenino,
                Direccion = "jr.cul",
                Email = "b@hot.com",
                Celular = "123456",
                Profesion = "Abogado",
                Salario = 1000,
                IsHabilitado = true,
                TipoEmpleadoId = 1
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarFallidoEmpleado()
        {
            var controller = new EmpleadoController(_serviceFalse.Object, _serviceTipoEmpleadosFalse.Object, _serviceDepartamentosFalse.Object);
            controller.Create(new Empleado { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarRepetidoEmpleado()
        {
            var controller = new EmpleadoController(_serviceFalse.Object, _serviceTipoEmpleadosFalse.Object, _serviceDepartamentosFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Empleado, bool>>>())).Returns(
                new List<Empleado>()
                {
                    new Empleado {
                         EmpleadoId = 1,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Femenino,
                        Direccion = "jr.cul",
                        Email = "b@hot.com",
                        Celular = "123456",
                        Profesion = "Abogado",
                        Salario = 1000,
                        IsHabilitado = true,
                        TipoEmpleadoId = 1
                }});


            controller.Create(new Empleado()
            {
                EmpleadoId = 2,
                Nombres = "Test",
                Apellidos = "Tests",
                DNI = "12345678",
                FechaNacimiento = DateTime.Now,
                Sexo = Genero.Femenino,
                Direccion = "jr.cul",
                Email = "b@hot.com",
                Celular = "123456",
                Profesion = "Abogado",
                Salario = 1000,
                IsHabilitado = true,
                TipoEmpleadoId = 1
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
        //********************************************************************************
        [Test]
        public void EditarExitosoEmpleado()
        {
            var controller = new EmpleadoController(_serviceFalse.Object, _serviceTipoEmpleadosFalse.Object, _serviceDepartamentosFalse.Object);
            controller.Edit(new Empleado
            {
                EmpleadoId = 1,
                Nombres = "Test",
                Apellidos = "Tests",
                DNI = "12345678",
                FechaNacimiento = DateTime.Now,
                Sexo = Genero.Femenino,
                Direccion = "jr.cul",
                Email = "b@hot.com",
                Celular = "123456",
                Profesion = "Abogado",
                Salario = 1000,
                IsHabilitado = true,
                TipoEmpleadoId = 1
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarFallidoEmpleado()
        {
            var controller = new EmpleadoController(_serviceFalse.Object, _serviceTipoEmpleadosFalse.Object, _serviceDepartamentosFalse.Object);
            controller.Edit(new Empleado { });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void EditarRepetidoEmpleado()
        {
            var controller = new EmpleadoController(_serviceFalse.Object, _serviceTipoEmpleadosFalse.Object, _serviceDepartamentosFalse.Object);

            _serviceFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Empleado, bool>>>())).Returns(
                new List<Empleado>()
                {
                    new Empleado {
                         EmpleadoId = 1,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Femenino,
                        Direccion = "jr.cul",
                        Email = "b@hot.com",
                        Celular = "123456",
                        Profesion = "Abogado",
                        Salario = 1000,
                        IsHabilitado = true,
                        TipoEmpleadoId = 1
                }});


            controller.Edit(new Empleado()
            {
                EmpleadoId = 2,
                Nombres = "Test",
                Apellidos = "Tests",
                DNI = "12345678",
                FechaNacimiento = DateTime.Now,
                Sexo = Genero.Femenino,
                Direccion = "jr.cul",
                Email = "b@hot.com",
                Celular = "123456",
                Profesion = "Abogado",
                Salario = 1000,
                IsHabilitado = true,
                TipoEmpleadoId = 1
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
    }
}
