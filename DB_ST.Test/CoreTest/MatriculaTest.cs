﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Domain.Enum;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace DB_ST.Test.CoreTest
{
    [TestFixture]
    class MatriculaTest
    {
         Mock<IMasterService<Matricula>> _serviceFalso;
         Mock<IMasterService<Seccion>> _serviceSeccionFalso;
         Mock<IMasterService<Estudiante>> _serviceEstudianteFalso;
         Mock<IMasterService<Grado>> _serviceGradoFalso;
         Mock<IMasterService<AnioEscolar>> _serviceAnioEscolarFalso;
         Mock<IMasterService<Apoderado>> _serviceApoderadoFalso;
         Mock<IMasterService<EstudianteEstadoAnio>> _serviceEstudianteEstadoAnioFalso;
         Mock<IMasterService<Clase>> _serviceClaseFalso;
         Mock<IMasterService<EstudianteClase>> _serviceEstudianteClaseFalso;
         Mock<IMasterService<Nota>> _serviceNotaFalso;
         Mock<IMasterService<Periodo>> _servicePeriodoFalso;

        

        [SetUp]
        public void SetUp()
        {
            _serviceFalso = new Mock<IMasterService<Matricula>>();
            _serviceSeccionFalso = new Mock<IMasterService<Seccion>>();
            _serviceEstudianteFalso = new Mock<IMasterService<Estudiante>>();
            _serviceGradoFalso = new Mock<IMasterService<Grado>>();
            _serviceAnioEscolarFalso = new Mock<IMasterService<AnioEscolar>>();
            _serviceApoderadoFalso =  new Mock<IMasterService<Apoderado>>();
            _serviceEstudianteEstadoAnioFalso = new Mock<IMasterService<EstudianteEstadoAnio>>();
            _serviceClaseFalso = new Mock<IMasterService<Clase>>();
            _serviceEstudianteClaseFalso = new Mock<IMasterService<EstudianteClase>>();
            _serviceNotaFalso = new Mock<IMasterService<Nota>>();
            _servicePeriodoFalso = new Mock<IMasterService<Periodo>>();

            _serviceFalso.Setup(o => o.Find(It.IsAny<Expression<Func<Matricula, bool>>>())).Returns(
                new List<Matricula>()
            );
            _serviceEstudianteFalso.Setup(o => o.GetById(It.IsAny<int>())).Returns(
                new Estudiante
                {
                    EstadoEstudiante=EstadoEstudiante.Regular,
                    EstudianteId=1
                }
            );
            _serviceApoderadoFalso.Setup(o => o.GetById(It.IsAny<int>())).Returns(
                new Apoderado
                {
                    ApoderadoId=1
                }
            );

            _serviceAnioEscolarFalso.Setup(o => o.GetAll()).Returns(
                new List<AnioEscolar>()
            );
            _serviceGradoFalso.Setup(o => o.GetAll()).Returns(
                new List<Grado>()
            );
            _serviceSeccionFalso.Setup(o => o.GetAll()).Returns(
                new List<Seccion>()
            );

            _serviceEstudianteFalso.Setup(o => o.Find(It.IsAny<Expression<Func<Estudiante, bool>>>())).Returns(

                    new List<Estudiante>()
                {
                    new Estudiante {
                        EstudianteId= 1,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Femenino,
                        RH = TipoSangre.AB_Negativo,
                        Direccion = "jr.cul",
                        DistritoId = 1,
                        Habilitado = true,
                        EstadoEstudiante = EstadoEstudiante.Regular

                    }
                }

                );
            _serviceApoderadoFalso.Setup(o => o.Find(It.IsAny<Expression<Func<Apoderado, bool>>>())).Returns(
                new List<Apoderado>()
                {
                    new Apoderado
                    {
                        ApoderadoId = 1,
                        Nombres = "Test",
                        Apellidos = "Tests",
                        DNI = "12345678",
                        FechaNacimiento = DateTime.Now,
                        Profesion = "Abogado",
                        Email = "a@hot",
                        Celular = "123456789"

                    }
                }
                );

            _serviceGradoFalso.Setup(o => o.Find(It.IsAny<Expression<Func<Grado, bool>>>())).Returns(
                new List<Grado>()
                {
                    new Grado
                    {
                        GradoId = 1,
                        Nombre = "Test",
                        UlltimaModificacion = DateTime.Now
                    }
                }
            );

            _serviceSeccionFalso.Setup(o => o.Find(It.IsAny<Expression<Func<Seccion, bool>>>(),
                It.IsAny<Expression<Func<Seccion, object>>>(),
                It.IsAny<Expression<Func<Seccion, object>>>()
            )).Returns(
                new List<Seccion>()
                {
                    new Seccion
                    {
                        SeccionId = 1,
                        Nombre = "Test",
                        Capacidad = 20,
                        //VacantesDisp= 15,
                        AnioEscolar = new AnioEscolar(),
                        Grado = new Grado()

                    }
                }
            );

            _serviceSeccionFalso.Setup(o => o.GetById(It.IsAny<int>())).Returns(
                new Seccion
                {
                    SeccionId = 1,
                    Nombre = "Test",
                    Capacidad = 20,
                    //VacantesDisp= 15,
                    AnioEscolar = new AnioEscolar(),
                    Grado = new Grado()

                }
           );

            _serviceAnioEscolarFalso.Setup(o => o.Find(It.IsAny<Expression<Func<AnioEscolar, bool>>>())).Returns(
                new List<AnioEscolar>()
                {
                    new AnioEscolar {
                        AnioEscolarId = 1,
                        Anio = "2016",
                        FechaInicio = DateTime.Parse("2017/07/04 10:00:00.000"),
                        FechaFin = DateTime.Parse("2017/07/05 10:00:00.000"),
                        FechaInicioMatricula = DateTime.Parse("2017/07/05 10:00:00.000"),
                        FechaFinMatricula = DateTime.Parse("2017/07/07 10:00:00.000"),
                        FechaMatriculaContemporanea = DateTime.Parse("2017/07/08 10:00:00.000")
                    }
                }
                );
        }

        [Test]
        public void MatriculaExitosa()
        {
            var controller = new MatriculaController(_serviceFalso.Object, _serviceSeccionFalso.Object, _serviceEstudianteFalso.Object,
                _serviceGradoFalso.Object, _serviceAnioEscolarFalso.Object, _serviceApoderadoFalso.Object, _serviceEstudianteEstadoAnioFalso.Object,
                _serviceClaseFalso.Object, _serviceEstudianteClaseFalso.Object, _serviceNotaFalso.Object, _servicePeriodoFalso.Object);

            controller.Create(new Matricula
            {
                TipoMatricula = TipoMatricula.Regular,
                FechaMatricula = DateTime.Parse("2017/07/06 11:00:00.000"),
                MontoMatricula = 100,
                EstudianteId = 1,
                ApoderadoId = 1,
                GradoId = 1,
                SeccionId = 1,
                NumeroVoucher = "01021415"
            }
            );

            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }
        [Test]
        public void MatriculaFallida()
        {
            var controller = new MatriculaController(_serviceFalso.Object, _serviceSeccionFalso.Object, _serviceEstudianteFalso.Object,
                _serviceGradoFalso.Object, _serviceAnioEscolarFalso.Object, _serviceApoderadoFalso.Object, _serviceEstudianteEstadoAnioFalso.Object,
                _serviceClaseFalso.Object, _serviceEstudianteClaseFalso.Object, _serviceNotaFalso.Object, _servicePeriodoFalso.Object);

            controller.Create(new Matricula{});

            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);

        }
        [Test]
        public void MatriculaFechaValidaRegular()
        {
            var controller = new MatriculaController(_serviceFalso.Object, _serviceSeccionFalso.Object, _serviceEstudianteFalso.Object,
                _serviceGradoFalso.Object, _serviceAnioEscolarFalso.Object, _serviceApoderadoFalso.Object, _serviceEstudianteEstadoAnioFalso.Object,
                _serviceClaseFalso.Object, _serviceEstudianteClaseFalso.Object, _serviceNotaFalso.Object, _servicePeriodoFalso.Object);

            controller.Create(new Matricula
            {
                TipoMatricula = TipoMatricula.Regular,
                FechaMatricula = DateTime.Parse("2017/07/06 11:00:00.000"),
                MontoMatricula = 100,
                EstudianteId = 1,
                ApoderadoId = 1,
                GradoId = 1,
                SeccionId = 1,
                NumeroVoucher = "01021415"
            }
            );

            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);

        }
        [Test]
        public void MatriculaFechaValidaContemporanea()
        {
            var controller = new MatriculaController(_serviceFalso.Object, _serviceSeccionFalso.Object, _serviceEstudianteFalso.Object,
                _serviceGradoFalso.Object, _serviceAnioEscolarFalso.Object, _serviceApoderadoFalso.Object, _serviceEstudianteEstadoAnioFalso.Object,
                _serviceClaseFalso.Object, _serviceEstudianteClaseFalso.Object, _serviceNotaFalso.Object, _servicePeriodoFalso.Object);

            controller.Create(new Matricula
            {
                TipoMatricula = TipoMatricula.Extemporanea,
                FechaMatricula = DateTime.Parse("2017/07/08 11:00:00.000"),
                MontoMatricula = 100,
                EstudianteId = 1,
                ApoderadoId = 1,
                GradoId = 1,
                SeccionId = 1,
                NumeroVoucher = "01021415"
            }
            );

            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);

        }
        [Test]
        public void MatriculaFechaNoValida()
        {
            var controller = new MatriculaController(_serviceFalso.Object, _serviceSeccionFalso.Object, _serviceEstudianteFalso.Object,
                _serviceGradoFalso.Object, _serviceAnioEscolarFalso.Object, _serviceApoderadoFalso.Object, _serviceEstudianteEstadoAnioFalso.Object,
                _serviceClaseFalso.Object, _serviceEstudianteClaseFalso.Object, _serviceNotaFalso.Object, _servicePeriodoFalso.Object);

            controller.Create(new Matricula
            {
                TipoMatricula = TipoMatricula.Extemporanea,
                FechaMatricula = DateTime.Parse("2017/08/08 11:00:00.000"),
                MontoMatricula = 100,
                EstudianteId = 1,
                ApoderadoId = 1,
                GradoId = 1,
                SeccionId = 1,
                NumeroVoucher = "01021415"
            }
            );

            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);

        }
        [Test]
        public void EstudianteYaMatriculado()
        {
            var controller = new MatriculaController(_serviceFalso.Object, _serviceSeccionFalso.Object, _serviceEstudianteFalso.Object,
                _serviceGradoFalso.Object, _serviceAnioEscolarFalso.Object, _serviceApoderadoFalso.Object, _serviceEstudianteEstadoAnioFalso.Object,
                _serviceClaseFalso.Object, _serviceEstudianteClaseFalso.Object, _serviceNotaFalso.Object, _servicePeriodoFalso.Object);

            _serviceFalso.Setup(o => o.Find(It.IsAny<Expression<Func<Matricula, bool>>>())).Returns(
                new List<Matricula>()
                {
                    new Matricula
            {
                TipoMatricula = TipoMatricula.Extemporanea,
                FechaMatricula = DateTime.Parse("2017/08/08 11:00:00.000"),
                MontoMatricula = 100,
                EstudianteId = 1,
                ApoderadoId = 1,
                GradoId = 1,
                SeccionId = 1,
                NumeroVoucher = "01021415"
            }});

            controller.Create(new Matricula
            {
                TipoMatricula = TipoMatricula.Extemporanea,
                FechaMatricula = DateTime.Parse("2017/08/08 11:00:00.000"),
                MontoMatricula = 100,
                EstudianteId = 1,
                ApoderadoId = 1,
                GradoId = 1,
                SeccionId = 1,
                NumeroVoucher = "01021415"
            }
            );

            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);

        }
        [Test]
        public void NoHayCupos()
        {

        }
        [Test]
        public void MatriculaSinEstudiante()
        {
            var controller = new MatriculaController(_serviceFalso.Object, _serviceSeccionFalso.Object, _serviceEstudianteFalso.Object,
                _serviceGradoFalso.Object, _serviceAnioEscolarFalso.Object, _serviceApoderadoFalso.Object, _serviceEstudianteEstadoAnioFalso.Object,
                _serviceClaseFalso.Object, _serviceEstudianteClaseFalso.Object, _serviceNotaFalso.Object, _servicePeriodoFalso.Object);

            controller.Create(new Matricula
            {
                TipoMatricula = TipoMatricula.Regular,
                FechaMatricula = DateTime.Parse("2017/07/06 11:00:00.000"),
                MontoMatricula = 100,
                ApoderadoId = 1,
                GradoId = 1,
                SeccionId = 1,
                NumeroVoucher = "01021415"
            }
            );

            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
        [Test]
        public void MatriculaSinApoderado()
        {
            var controller = new MatriculaController(_serviceFalso.Object, _serviceSeccionFalso.Object, _serviceEstudianteFalso.Object,
                _serviceGradoFalso.Object, _serviceAnioEscolarFalso.Object, _serviceApoderadoFalso.Object, _serviceEstudianteEstadoAnioFalso.Object,
                _serviceClaseFalso.Object, _serviceEstudianteClaseFalso.Object, _serviceNotaFalso.Object, _servicePeriodoFalso.Object);

            controller.Create(new Matricula
            {
                TipoMatricula = TipoMatricula.Regular,
                FechaMatricula = DateTime.Parse("2017/07/06 11:00:00.000"),
                MontoMatricula = 100,
                EstudianteId = 1,
                GradoId = 1,
                SeccionId = 1,
                NumeroVoucher = "01021415"
            }
            );

            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);

        }
    }
}
