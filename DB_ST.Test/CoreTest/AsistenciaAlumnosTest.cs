﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DB_ST.Domain;
using DB_ST.Domain.Enum;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using DB_ST.Web.Models;
using Moq;
using NUnit.Framework;

namespace DB_ST.Test.CoreTest
{
    [TestFixture]
    class AsistenciaAlumnosTest
    {
        Mock<IMasterService<AsistenciaAlumno>> _serviceFalse;
        Mock<IMasterService<Matricula>> _matriculaFalse;
        Mock<IMasterService<Seccion>> _seccionFalse;
        Mock<IMasterService<Grado>> _gradoFalse;
        Mock<IMasterService<AnioEscolar>> _anioFalse;
        Mock<IMasterService<Estudiante>> _estudianteFalse;
        Mock<IMasterService<Empleado>> _empleadoFalse;
        Mock<IMasterService<AsistenciaAlumno>> _asistenciaFalse;

        [SetUp]
        public void SetUp()
        {
            _serviceFalse = new Mock<IMasterService<AsistenciaAlumno>>();
            _matriculaFalse = new Mock<IMasterService<Matricula>>();
            _seccionFalse = new Mock<IMasterService<Seccion>>();
            _gradoFalse = new Mock<IMasterService<Grado>>();
            _anioFalse = new Mock<IMasterService<AnioEscolar>>();
            _estudianteFalse = new Mock<IMasterService<Estudiante>>();
            _empleadoFalse = new Mock<IMasterService<Empleado>>();
            _asistenciaFalse = new Mock<IMasterService<AsistenciaAlumno>>();


            //////////////////////////////////////////////
            _anioFalse.Setup(o => o.Find(It.IsAny<Expression<Func<AnioEscolar, bool>>>())).Returns(
                new List<AnioEscolar>()
                {
                new AnioEscolar()
                    {
                    AnioEscolarId = 1,
                    Anio = "2017",
                    FechaInicio = DateTime.Now,
                    FechaFin = DateTime.Now,
                    FechaInicioMatricula = DateTime.Now,
                    FechaFinMatricula = DateTime.Now,
                    FechaMatriculaContemporanea = DateTime.Now,
                    HoraEntradaClases = new TimeSpan(8, 45, 0),
                    HoraSalidaClases = new TimeSpan(13, 0, 0),

                    }
                }
            );

            _empleadoFalse.Setup(o => o.GetById(It.IsAny<int>())).Returns(
                new Empleado()
                {
                    EmpleadoId = 1,
                    Nombres = "Test",
                    Apellidos = "Tess",
                    Celular = "123456789",
                    Direccion = "adfadf",
                    DistritoId = 1,
                    DNI = "123456",
                    Email = "asdasd",
                    FechaNacimiento = DateTime.Now
                }
            );

            _seccionFalse.Setup(o => o.Find(It.IsAny<Expression<Func<Seccion, bool>>>())).Returns(
                new List<Seccion>()
                {
                    new Seccion {
                        SeccionId = 1,
                        Nombre = "Test",
                        Capacidad = 20,
                        //VacantesDisp= 15,
                        AnioEscolar= new AnioEscolar(),
                        Grado = new Grado()

                    }
                }
            );

            _gradoFalse.Setup(o => o.GetAll()).Returns(
                new List<Grado>()
                {
                    new Grado {
                        GradoId = 1,
                        Nombre = "Test",
                        UlltimaModificacion = DateTime.Now
                    }
                }
                );
        }


        [Test]
        public void GuardarExitosoAsistenciaAlumno()
        {
            var controller = new AsistenciaAlumnoController(_serviceFalse.Object,_matriculaFalse.Object,_seccionFalse.Object,_gradoFalse.Object,_anioFalse.Object,_estudianteFalse.Object,_empleadoFalse.Object,_asistenciaFalse.Object);

            controller.Create(new AsistenciaAlumnoVM
            {
                Fecha = DateTime.Parse("2017/01/25 10:00:00.000"),
                GradoId = 1,
                SeccionId = 1,
                Estudiantes = new List<EstudianteVM>
                { 
                        new EstudianteVM
                        {
                            EstudianteId = 1,
                            Estudiante = "Test",
                            ValorAsistencia = ValorAsistencia.Asistio
                        }

                }
            });


            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarVacioAsistenciaAlumno()
        {
            var controller = new AsistenciaAlumnoController(_serviceFalse.Object, _matriculaFalse.Object, _seccionFalse.Object, _gradoFalse.Object, _anioFalse.Object, _estudianteFalse.Object, _empleadoFalse.Object, _asistenciaFalse.Object);
            controller.Create(new AsistenciaAlumnoVM
            {
                Fecha = DateTime.Parse("2017/01/25 10:00:00.000"),
                GradoId = 1,
                SeccionId = 1,
                Estudiantes = new List<EstudianteVM>()
            });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarNoRegistradoAsistenciaAlumno()
        {
            var controller = new AsistenciaAlumnoController(_serviceFalse.Object, _matriculaFalse.Object, _seccionFalse.Object, _gradoFalse.Object, _anioFalse.Object, _estudianteFalse.Object, _empleadoFalse.Object, _asistenciaFalse.Object);
            controller.Create(new AsistenciaAlumnoVM
            {
                Fecha = DateTime.Parse("2017/01/25 10:00:00.000"),
                GradoId = 1,
                SeccionId = 1,
                Estudiantes = new List<EstudianteVM>
                {
                        new EstudianteVM
                        {
                            EstudianteId = 1,
                            Estudiante = "Test",
                            ValorAsistencia = ValorAsistencia.NoRegistrado
                        }

                }
            });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
        [Test]
        public void GuardarFueraDeHoraAsistenciaAlumno()
        {

            var controller = new AsistenciaAlumnoController(_serviceFalse.Object, _matriculaFalse.Object, _seccionFalse.Object, _gradoFalse.Object, _anioFalse.Object, _estudianteFalse.Object, _empleadoFalse.Object, _asistenciaFalse.Object);
            controller.Create(new AsistenciaAlumnoVM
            {
                Fecha = DateTime.Parse("2017/01/25 15:00:00.000"),
                GradoId = 1,
                SeccionId = 1,
                Estudiantes = new List<EstudianteVM>
                {
                        new EstudianteVM
                        {
                            EstudianteId = 1,
                            Estudiante = "Test",
                            ValorAsistencia = ValorAsistencia.Asistio
                        }

                }
            });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }


    }
}
