﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DB_ST.Domain;
using DB_ST.Domain.Enum;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace DB_ST.Test.CoreTest
{
    [TestFixture]
    class SolicitudCambioNotasTest
    {
        Mock<IMasterService<NotaSolicitudCambio>> _serviceNotaSolicitudCambioFalso;
        Mock<IMasterService<Nota>> _serviceNotaFalso;
        Mock<IMasterService<Empleado>> _serviceEmpleadoFalso;

        [SetUp]
        public void SetUp()
        {
            _serviceNotaSolicitudCambioFalso = new Mock<IMasterService<NotaSolicitudCambio>>();
            _serviceNotaFalso= new Mock<IMasterService<Nota>>();
            _serviceEmpleadoFalso = new Mock<IMasterService<Empleado>>();

            _serviceNotaSolicitudCambioFalso.Setup(o => o.Find(It.IsAny<Expression<Func<NotaSolicitudCambio, bool>>>())).Returns(
                new List<NotaSolicitudCambio>
                {
                    new NotaSolicitudCambio
                    {
                        NotaSolicitudCambioId = 1,
                        NotaId = 1,
                        NotaNueva = 10,
                        Motivo = "kakakaka",
                        DestinatarioId = 1,
                        EstadoSolicitud = EstadoSolicitud.Aprobada
                    }
                }
                );
            _serviceNotaFalso.Setup(o => o.GetById(It.IsAny<int>())).Returns(
                new Nota
                {
                    NotaId = 1,
                    EstudianteClaseId = 1,
                    PeriodoId = 1,
                    Vigesimal = 15
                }
                );
            _serviceEmpleadoFalso.Setup(o => o.Find(It.IsAny<Expression<Func<Empleado, bool>>>())).Returns(
                new List<Empleado>()
                {
                        new Empleado
                    {
                        EmpleadoId = 1,
                        Nombres = "Test",
                        Apellidos = "Tests",
                        DNI = "12345678",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Femenino,
                        Direccion = "jr.cul",
                        Email = "b@hot.com",
                        Celular = "123456",
                        Profesion = "Abogado",
                        Salario = 1000,
                        IsHabilitado = true,
                        TipoEmpleadoId = 1
                    }
                }
                
                );
        }


        [Test]
        public void GuardarExitosoSolicitudNota()
        {
            var controller = new NotaSolicitudCambioController(_serviceNotaSolicitudCambioFalso.Object, _serviceNotaFalso.Object, _serviceEmpleadoFalso.Object);

            controller.Create(new NotaSolicitudCambio
            {
                Motivo = "asdas",
                NotaId = 1,
                NotaNueva = 19,
                EstadoSolicitud = EstadoSolicitud.Pendiente
            });
            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }
        [Test]
        public void GuardarFallidoSolicitudNota()
        {
            var controller = new NotaSolicitudCambioController(_serviceNotaSolicitudCambioFalso.Object, _serviceNotaFalso.Object, _serviceEmpleadoFalso.Object);

            controller.Create(new NotaSolicitudCambio
            {
                
            });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
        [Test]
        public void GuardarNotaFueraRango()
        {
            var controller = new NotaSolicitudCambioController(_serviceNotaSolicitudCambioFalso.Object, _serviceNotaFalso.Object, _serviceEmpleadoFalso.Object);

            controller.Create(new NotaSolicitudCambio
            {
                Motivo = "asdas",
                NotaId = 1,
                NotaNueva = 28,
                EstadoSolicitud = EstadoSolicitud.Pendiente
            });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
        [Test]
        public void GuardarSinMotivoSolicitudNota()
        {
            var controller = new NotaSolicitudCambioController(_serviceNotaSolicitudCambioFalso.Object, _serviceNotaFalso.Object, _serviceEmpleadoFalso.Object);

            controller.Create(new NotaSolicitudCambio
            {
                NotaId = 1,
                NotaNueva = 19,
                EstadoSolicitud = EstadoSolicitud.Pendiente
            });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

        [Test]
        public void GuardarSinNuevaNota()
        {
            var controller = new NotaSolicitudCambioController(_serviceNotaSolicitudCambioFalso.Object, _serviceNotaFalso.Object, _serviceEmpleadoFalso.Object);

            controller.Create(new NotaSolicitudCambio
            {
                Motivo = "asdas",
                NotaId = 1,
                EstadoSolicitud = EstadoSolicitud.Pendiente
            });
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }

    }
}
