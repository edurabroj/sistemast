﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using DB_ST.Domain;
using DB_ST.Domain.Enum;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using DB_ST.Web.Models;
using DB_ST.Web.Models.EntitiesVM;
using Moq;
using NUnit.Framework;

namespace DB_ST.Test.CoreTest
{
    [TestFixture]
    class NotaAlumnosTest
    {
        Mock<IMasterService<AnioEscolar>> _serviceAnioEscolar;
        Mock<IMasterService<Asignatura>> _serviceAsignaturaFalse;
        Mock<IMasterService<Clase>> _serviceClaseFalse;
        Mock<IMasterService<Estudiante>> _serviceEstudianteFalse;
        Mock<IMasterService<EstudianteClase>> _serviceEstudianteClaseFalse;
        Mock<IMasterService<Nota>> _serviceNotaFalse;
        Mock<IMasterService<NotaSolicitudCambio>> _serviceNotaSolicitudCambioFalse;
        Mock<IMasterService<Periodo>> _servicePeriodoFalse;
        Mock<IMasterService<Empleado>> _serviceEmpleadoFalse;

        [SetUp]
        public void SetUp()
        {
            _serviceAnioEscolar = new Mock<IMasterService<AnioEscolar>>();
            _serviceAsignaturaFalse = new Mock<IMasterService<Asignatura>>();
            _serviceClaseFalse =  new Mock<IMasterService<Clase>>();
            _serviceEstudianteFalse = new Mock<IMasterService<Estudiante>>();
            _serviceEstudianteClaseFalse = new Mock<IMasterService<EstudianteClase>>();
            _serviceNotaFalse = new Mock<IMasterService<Nota>>();
            _serviceNotaSolicitudCambioFalse = new Mock<IMasterService<NotaSolicitudCambio>>();
            _servicePeriodoFalse = new Mock<IMasterService<Periodo>>();
            _serviceEmpleadoFalse = new Mock<IMasterService<Empleado>>();

            //clase
            _serviceClaseFalse.Setup(o => o.GetById(It.IsAny<int>())).Returns(
                    new Clase {
                        ClaseId = 1,
                        Nombre = "Test"
                    }
            );

            //estudiante
            _serviceEstudianteFalse.Setup(o => o.GetAll()).Returns(
                new List<Estudiante>()
                {
                    new Estudiante {
                        EstudianteId= 1,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Femenino,
                        RH = TipoSangre.AB_Negativo,
                        Direccion = "jr.cul",
                        DistritoId = 1,
                        Habilitado = true,
                        EstadoEstudiante = EstadoEstudiante.Regular
                 },
                    new Estudiante {
                        EstudianteId= 2,
                        Nombres = "Test2",
                        Apellidos="Tests2",
                        DNI="12345679",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Masculino,
                        RH = TipoSangre.AB_Positivo,
                        Direccion = "jr.culiao",
                        DistritoId = 2,
                        Habilitado = true,
                        EstadoEstudiante = EstadoEstudiante.Regular
}
                }
            );

            //Estudiante clase

            _serviceEstudianteClaseFalse.Setup(o => o.Find(It.IsAny<Expression<Func<EstudianteClase, bool>>>(),
                                            It.IsAny<Expression<Func<EstudianteClase, object>>>(),
                                            It.IsAny<Expression<Func<EstudianteClase, object>>>(),
                                            It.IsAny<Expression<Func<EstudianteClase, object>>>())).Returns(
                new List<EstudianteClase>()
                {
                    new EstudianteClase
                    {
                       EstudianteClaseId = 1,
                       Estudiante = new Estudiante{
                        EstudianteId= 1,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Femenino,
                        RH = TipoSangre.AB_Negativo,
                        Direccion = "jr.cul",
                        DistritoId = 1,
                        Habilitado = true,
                        EstadoEstudiante = EstadoEstudiante.Regular
                 },
                       Clase = new Clase
                       {
                           Nombre = "Test",
                           Asignatura = new Asignatura
                           {
                               Nombre = "asds"
                           }
                       }
                       

                    },
                    new EstudianteClase
                    {
                        EstudianteClaseId = 2,
                        Estudiante = new Estudiante{
                        EstudianteId= 2,
                        Nombres = "Test",
                        Apellidos="Tests",
                        DNI="12345678",
                        FechaNacimiento = DateTime.Now,
                        Sexo = Genero.Femenino,
                        RH = TipoSangre.AB_Negativo,
                        Direccion = "jr.cul",
                        DistritoId = 1,
                        Habilitado = true,
                        EstadoEstudiante = EstadoEstudiante.Regular
                 }
                    }
                }
            );

            //periodos
            _servicePeriodoFalse.Setup(o => o.GetById(It.IsAny<int>())).Returns(
                    new Periodo
                    {
                        PeriodoId = 1,
                        Nombre = "Test",
                        Orden = 1
                     }
            );
        }
        
        
        [Test]
        public void GuardarExitosoNota()
        {
            var controller = new NotaController(_serviceAnioEscolar.Object, _serviceAsignaturaFalse.Object, _serviceClaseFalse.Object, _serviceEstudianteFalse.Object, _serviceEstudianteClaseFalse.Object, _serviceNotaFalse.Object, _serviceNotaSolicitudCambioFalse.Object,_servicePeriodoFalse.Object,_serviceEmpleadoFalse.Object);

            controller.Create(new Nota
            {
                EstudianteClaseId = 1,
                PeriodoId = 1,
                Vigesimal = 15
            });


            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }
        [Test]
        public void GuardarFallidoNota()
        {
            var controller = new NotaController(_serviceAnioEscolar.Object, _serviceAsignaturaFalse.Object, _serviceClaseFalse.Object, _serviceEstudianteFalse.Object, _serviceEstudianteClaseFalse.Object, _serviceNotaFalse.Object, _serviceNotaSolicitudCambioFalse.Object, _servicePeriodoFalse.Object, _serviceEmpleadoFalse.Object);

            controller.Create(new Nota{});

            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
        [Test]
        public void GuardarNotaVacia()
        {
            var controller = new NotaController(_serviceAnioEscolar.Object, _serviceAsignaturaFalse.Object, _serviceClaseFalse.Object, _serviceEstudianteFalse.Object, _serviceEstudianteClaseFalse.Object, _serviceNotaFalse.Object, _serviceNotaSolicitudCambioFalse.Object, _servicePeriodoFalse.Object, _serviceEmpleadoFalse.Object);

            controller.Create(new Nota
            {
                EstudianteClaseId = 1,
                PeriodoId = 1
                
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
        [Test]
        public void GuardarNotaNegativa()
        {
            var controller = new NotaController(_serviceAnioEscolar.Object, _serviceAsignaturaFalse.Object, _serviceClaseFalse.Object, _serviceEstudianteFalse.Object, _serviceEstudianteClaseFalse.Object, _serviceNotaFalse.Object, _serviceNotaSolicitudCambioFalse.Object, _servicePeriodoFalse.Object, _serviceEmpleadoFalse.Object);

            controller.Create(new Nota
            {
                EstudianteClaseId = 1,
                PeriodoId = 1,
                Vigesimal = -1
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
        [Test]
        public void GuardarNotaMayorVeinte()
        {
            var controller = new NotaController(_serviceAnioEscolar.Object, _serviceAsignaturaFalse.Object, _serviceClaseFalse.Object, _serviceEstudianteFalse.Object, _serviceEstudianteClaseFalse.Object, _serviceNotaFalse.Object, _serviceNotaSolicitudCambioFalse.Object, _servicePeriodoFalse.Object, _serviceEmpleadoFalse.Object);

            controller.Create(new Nota
            {
                EstudianteClaseId = 1,
                PeriodoId = 1,
                Vigesimal = 25
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
    }
}
