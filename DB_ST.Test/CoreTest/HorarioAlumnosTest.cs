﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using DB_ST.Domain;
using DB_ST.Domain.Enum;
using DB_ST.Service;
using DB_ST.Web.Controllers;
using Moq;
using NUnit.Framework;

namespace DB_ST.Test.CoreTest
{
    [TestFixture]
    class HorarioAlumnosTest
    {
        Mock<IMasterService<Sesion>> _serviceSesionFalso;
        Mock<IMasterService<Clase>> _serviceClaseFalso;
        Mock<IMasterService<Grado>> _serviceGradoFalso;
        Mock<IMasterService<Seccion>> _serviceSeccionFalso;
        Mock<IMasterService<Horario>> _serviceHorarionFalso;

        [SetUp]
        public void SetUp()
        {
            _serviceSesionFalso = new Mock<IMasterService<Sesion>>();
            _serviceClaseFalso = new Mock<IMasterService<Clase>>();
            _serviceSeccionFalso = new Mock<IMasterService<Seccion>>();
            _serviceGradoFalso = new Mock<IMasterService<Grado>>();
            _serviceHorarionFalso = new Mock<IMasterService<Horario>>();


            _serviceSesionFalso.Setup(o => o.Find(It.IsAny<Expression<Func<Sesion, bool>>>(),
                                                  It.IsAny<Expression<Func<Sesion, Object>>>(),
                                                  It.IsAny<Expression<Func<Sesion, Object>>>())).Returns(
                new List<Sesion>()
                {
                    new Sesion
                    {
                        SesionId = 1
                    }
                }
                );

            _serviceClaseFalso.Setup(o => o.GetById(It.IsAny<int>())).Returns(
                    new Clase
                    {
                        ClaseId = 1,
                        Nombre = "Test"
                    }
            );

            _serviceSeccionFalso.Setup(o => o.Find(It.IsAny<Expression<Func<Seccion, bool>>>())).Returns(
                new List<Seccion>()
                {
                    new Seccion
                    {
                        SeccionId = 1,
                        Nombre = "asdasd"

                    }
                }
                );
            _serviceGradoFalso.Setup(o => o.GetById(It.IsAny<int>())).Returns(
                    new Grado {
                        GradoId = 1,
                        Nombre = "Test",
                        UlltimaModificacion = DateTime.Now
                    }
            );
        }

       [Test]
        public void TestGuardarSesiones()
        {
            var controller = new SesionController(_serviceSesionFalso.Object, _serviceClaseFalso.Object, _serviceGradoFalso.Object, _serviceSeccionFalso.Object,_serviceHorarionFalso.Object);

            controller.Create(new Sesion
            {
               SesionId = 1,
               ClaseId = 1,
               Dia = Dia.L,
               HorarioId=1
            });


            Assert.AreEqual(true, controller.ViewData.ModelState.IsValid);
        }
        //[Test]
        //public void TestGuardarVacioDiaSesiones()
        //{
        //    var controller = new SesionController(_serviceSesionFalso.Object, _serviceClaseFalso.Object, _serviceGradoFalso.Object, _serviceSeccionFalso.Object, _serviceHorarionFalso.Object);

        //    controller.Create(new Sesion
        //    {
        //        SesionId = 1,
        //        ClaseId = 1,
        //        HorarioId=1
        //    });


        //    Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        //}
        [Test]
        public void TestGuardarVacioHorarioSesiones()
        {
            var controller = new SesionController(_serviceSesionFalso.Object, _serviceClaseFalso.Object, _serviceGradoFalso.Object, _serviceSeccionFalso.Object, _serviceHorarionFalso.Object);

            controller.Create(new Sesion
            {
                SesionId = 1,
                ClaseId = 1,
                Dia = Dia.L
            });


            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        }
        //[Test]
        //public void TestGuardarIgualesIniFinSesiones()
        //{
        //    var controller = new SesionController(_serviceSesionFalso.Object, _serviceClaseFalso.Object, _serviceGradoFalso.Object, _serviceSeccionFalso.Object, _serviceHorarionFalso.Object);

        //    controller.Create(new Sesion
        //    {
        //        SesionId = 1,
        //        ClaseId = 1,
        //        Dia = Dia.L,
        //        HorarioId=1
        //    });


        //    Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
        //}
    }
}
